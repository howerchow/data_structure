## 严蔚敏《数据结构》课本源码与习题解析

## 源码编译
```cmake
	cmake -S . -B build
	cmake --build build
```

## 源码目录

| 章              | 节                          | 内容                                  | 包含算法                          | 备注                           |
|:----------------|:----------------------------|:--------------------------------------|:----------------------------------|:-------------------------------|
| 01 绪论         | Status                      |                                       |                                   | 定义一些共享常量和函数         |
| 02 线性表       | SqList                      | 顺序表                                | 2.3、2.4、2.5、2.6                | 线性表的顺序存储结构           |
|                 | Union                       | A=A∪B                                 | 2.1                               |                                |
|                 | MergeSqList                 | C=A+B                                 | 2.2、2.7                          | 归并顺序表                     |
|                 | LinkList                    | 单链表                                | 2.8、2.9、2.10、2.11              | 线性表的链式存储结构           |
|                 | MergeList                   | C=A+B                                 | 2.12                              | 归并链表                       |
|                 | SLinkList                   | 静态链表                              | 2.13、2.14、2.15、2.16            |                                |
|                 | Difference                  | (A-B)∪(B-A)                           | 2.17                              |                                |
|                 | DuLinkList                  | 双向循环链表                          | 2.18、2.19                        |                                |
|                 | ELinkList                   | 扩展的线性链表                        | 2.20                              |                                |
|                 | MergeEList                  | C=A+B                                 | 2.21                              | 归并扩展的线性链表             |
|                 | Polynomial                  | 一元多项式                            | 2.22、2.23                        |                                |
|                 | NaturalJoin                 | 自然连接                              |                                   | 顺序表和链表的应用             |
| 03 栈和队列     | SqStack                     | 栈                                    |                                   | 顺序存储结构                   |
|                 | Conversion                  | 进制转换                              | 3.1                               | 栈的应用                       |
|                 | LineEdit                    | 行编辑程序                            | 3.2                               | 栈的应用                       |
|                 | Maze                        | 迷宫寻路                              | 3.3                               | 栈的应用                       |
|                 | Expression                  | 表达式求值                            | 3.4                               | 栈的应用                       |
|                 | Hanoi                       | 汉诺塔                                | 3.5                               | 递归                           |
|                 | LinkQueue                   | 单链表队列                            |                                   | 链式存储结构(单链表)           |
|                 | SqQueue                     | 循环队列                              |                                   | 顺序存储结构                   |
|                 | BankQueuing                 | 模拟银行排队                          | 3.6、3.7                          | 队列的应用                     |
|                 | DeQueue                     | 双端队列                              |                                   | 双向链表                       |
|                 | Queue                       | 队列                                  |                                   | 两个栈模拟队列                 |
| 04 串           | SString                     | 顺序串                                | 4.1、4.2、4.3、4.5                | 顺序存储                       |
|                 | HString                     | 堆串                                  | 4.4                               | 顺序存储，动态分配内存         |
|                 | LString                     | 块链串                                |                                   | 顺序存储+链式存储              |
|                 | KMP                         | KMP算法                               | 4.6、4.7、4.8                     | 字符串匹配算法                 |
|                 | WordList                    | 关键词索引                            | 4.9、4.10、4.11、4.12、4.13、4.14 | 堆串和线性表的应用             |
| 05 数组和广义表 | Array                       | 多维数组                              |                                   |                                |
|                 | TSMatrix                    | 稀疏矩阵                              | 5.1、5.2                          | 三元组顺序表存储方式           |
|                 | RLSMatrix                   | 稀疏矩阵                              | 5.3                               | 行逻辑链接的顺序表存储方式     |
|                 | CrossList                   | 稀疏矩阵                              | 5.4                               | 十字链表存储方式               |
|                 | GList-HT                    | 广义表                                | 5.5、5.6、5.7、5.8                | 头尾链表存储表示               |
|                 | GList-E                     | 广义表                                |                                   | 扩展线性链表存储表示           |
|                 | MPList                      | m元多项式                             |                                   | 链式存储                       |
| 06 树和二叉树   | SqBiTree                    | 二叉树顺序存储结构                    |                                   |                                |
|                 | BiTree                      | 二叉树的二叉链表存储结构              | 6.1、6.2、6.3、6.4                |                                |
|                 | BiTriTree                   | 二叉树的三叉链表存储结构              |                                   |                                |
|                 | BiThrTree                   | 线索二叉树                            | 6.5、6.6、6.7                     |                                |
|                 | PTree                       | 树的双亲表存储表示                    |                                   |                                |
|                 | CTree                       | 树的孩子链表(带双亲)的存储表示        |                                   |                                |
|                 | CSTree                      | 树的二叉链表（孩子-兄弟）结构存储表示 |                                   |                                |
|                 | MFSet                       | 集合                                  | 6.8、6.9、6.10、6.11              |                                |
|                 | HuffmanTree                 | 赫夫曼树                              | 6.12、6.13                        | 又称"哈夫曼树"                 |
|                 | PowerSet                    | 冪集                                  | 6.14/6.15                         |                                |
|                 | NQueens                     | N皇后问题                             | 6.16                              |                                |
| 07 图           | MGraph                      | 图的邻接矩阵存储                      | 7.1、7.2、7.4、7.5、7.6           | 有向图、有向网、无向图、无向网 |
|                 | ALGraph                     | 图的邻接表存储                        |                                   | 有向图、有向网、无向图、无向网 |
|                 | OLGraph                     | 图的十字链表存储                      | 7.3                               | 有向图、有向网、无向图、无向网 |
|                 | AMLGraph                    | 图的邻接多重表存储                    |                                   | 无向图、无向网                 |
|                 | SpanningTree                | 无向图的生成树                        | 7.7、7.8                          | 深度优先生成树                 |
|                 | StronglyConnectedComponents | 有向图强连通分量                      |                                   | Kosaraju算法和Tarjan算法       |
|                 | MinimumSpanningTree         | 无向网的最小生成树                    | 7.9                               | Prim算法和Kruskal算法          |
|                 | ArticulationPoints          | 无向图的关节点                        | 7.10、7.11                        |                                |
|                 | TopologicalSorting          | AOV-网的拓扑排序                      | 7.12                              | 有向图                         |
|                 | CriticalPathMethod          | AOE-网的关键路径                      | 7.13、7.14                        | 有向网                         |
|                 | ShortestPaths               | 最短路径算法                          | 7.15、7.16                        | Dijkstra算法和Floyd算法        |
| 08 动态存储管理 | BoundaryTagMethod           | 边界标识法                            | 8.1                               |                                |
|                 | BuddySystem                 | 伙伴系统                              | 8.2                               |                                |
|                 | GarbageCollection           | 无用单元收集                          | 8.3                               | 无栈遍历广义表                 |
| 09 查找         | Base                        | 通用查找表存储结构                    |                                   |                                |
|                 | SequnceSearch               | 顺序查找(无序表)                      | 9.1                               |                                |
|                 | Binsearch                   | 折半查找(二分查找)                    | 9.2                               |                                |
|                 | FibonacciSearch             | 斐波那契查找                          |                                   |                                |
|                 | InterpolationSearch         | 插值查找                              |                                   |                                |
|                 | NearlyOptimalSearchTree     | 次优查找树                            | 9.3、9.4                          |                                |
|                 | BinarySortTree              | 二叉排序/查找/搜索树                  | 9.5 9.6 9.7 9.8                   |                                |
|                 | BalancedBinarySortTree      | 平衡二叉排序/查找/搜索树              | 9.9 9.10 9.11 9.12                |                                |
|                 | B-Tree                      | B-树                                  | 9.13 9.14                         |                                |
|                 | B+Tree                      | B+树                                  |                                   |                                |
|                 | DLTree                      | 双链树                                | 9.15                              |                                |
|                 | TrieTree                    | 键树(字典树)                          | 9.16                              |                                |
|                 | HashTable                   | 哈希查找表                            | 9.17 9.18                         |                                |
|                 | RBTree                      | 红黑树                                |                                   |                                |
|                 | BitMap                      | 位图                                  |                                   |                                |
| 10 排序         | SequenceListType            | 排序表顺序表存储结构                  |                                   |                                |
|                 | StaticLinkedListType        | 排序表静态表存储结构                  |                                   |                                |
|                 | InsertSort                  | 直接插入排序                          | 10.1                              |                                |
|                 | BInsertSort                 | 折半插入排序                          | 10.2                              |                                |
|                 | 2-InsertSort                | 2-路插入排序                          |                                   |                                |
|                 | TInsertSort                 | 表插入排序                            | 10.3                              |                                |
|                 | ShellSort                   | 希尔插入排序                          | 10.4 10.5                         |                                |
|                 | BubbleSort                  | 冒泡排序                              |                                   |                                |
|                 | QuickSort                   | 快速排序                              | 10.6 10.7 10.8                    |                                |
|                 | SelectSort                  | 简单选择排序                          | 10.9                              |                                |
|                 | HeapSort                    | 堆排序                                | 10.10 10.11                       |                                |
|                 | MergingSort                 | 归并排序                              | 10.12 10.13 10.14                 |                                |
|                 | RadixSort                   | 基数排序                              | 10.15 10.16 10.17                 |                                |
|                 | AddressSort                 | 顺序表地址排序                        | 10.18                             |                                |
|                 | CocktailShakerSort          | 鸡尾酒排序                            |                                   | 冒泡排序优化(几乎有序表现较好) |
|                 | BidirectSelectionSort       | 双向选择排序                          |                                   | 简单选择排序优化               |
| 11 外部排序     | LoserTree                   | 败者树                                | 11.1 11.2 11.3                    |                                |
|                 | ReSeSort                    | 置换选择排序                          | 11.4 11.5 11.6 11.7               |                                |
| 12 文件         | SequentialFile              | 文件归并                              | 12.1                              |                                |

## 排序算法

| 排序算法 | 平均时间复杂度 | 最好情况   | 最坏情况   | 空间复杂度 | 排序方式 | 稳定性 |
|----------|----------------|------------|------------|------------|----------|--------|
| 冒泡排序 | O(n^2)         | O(n)       | O(n^2)     | O(1)       | 内排序   | 稳定   |
| 选择排序 | O(n^2)         | O(n^2)     | O(n^2)     | O(1)       | 内排序   | 不稳定 |
| 插入排序 | O(n^2)         | O(n)       | O(n^2)     | O(1)       | 内排序   | 稳定   |
| 希尔排序 | O(n^1.3)       | O(n)       | O(n^2)     | O(1)       | 内排序   | 不稳定 |
| 归并排序 | O(n log n)     | O(n log n) | O(n log n) | O(n)       | 内排序   | 稳定   |
| 快速排序 | O(n log n)     | O(n log n) | O(n^2)     | O(log n)   | 内排序   | 不稳定 |
| 堆排序   | O(n log n)     | O(n log n) | O(n log n) | O(1)       | 内排序   | 不稳定 |
| 计数排序 | O(n+k)         | O(n+k)     | O(n+k)     | O(k)       | 外排序   | 稳定   |
| 桶排序   | O(n+k)         | O(n+k)     | O(n^2)     | O(n+k)     | 外排序   | 稳定   |
| 基数排序 | O(n+k)         | O(n+k)     | O(n*k)     | O(n+k)     | 外排序   | 稳定   |



## Big-O
[Big-O Cheat Sheet](https://www.bigocheatsheet.com/)
