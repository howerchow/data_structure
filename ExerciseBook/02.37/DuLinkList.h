/*=====================
 * 双向循环链表
 *
 * 包含算法: 2.18、2.19
 ======================*/

#pragma once

#include "Status.h" //**▲01 绪论**//
#include <stdio.h>
#include <stdlib.h> //提供malloc、realloc、free、exit原型

/* 双向循环链表元素类型定义 */
typedef int ElemType;

/*
 * 双向循环链表结构
 *
 * 注：这里的双向循环链表存在头结点
 */
typedef struct DuLNode {
        ElemType data;
        struct DuLNode *prior; // 前驱
        struct DuLNode *next;  // 后继
} DuLNode;

// 指向双向循环链表结点的指针
typedef DuLNode *DuLinkList;

/*
 * 初始化
 *
 * 初始化成功则返回OK，否则返回ERROR。
 */
Status InitList( DuLinkList *L );

/*
 * ████████ 算法2.18 ████████
 *
 * 插入
 *
 * 向双向循环链表第i个位置上插入e，插入成功则返回OK，否则返回ERROR。
 *
 *【备注】
 * 教材中i的含义是元素位置，从1开始计数
 */
Status ListInsert( DuLinkList L, int i, ElemType e );

/*
 * 遍历
 *
 * 用visit函数访问双向循环链表L
 */
void ListTraverse( DuLinkList L, void( Visit )( ElemType ) );
