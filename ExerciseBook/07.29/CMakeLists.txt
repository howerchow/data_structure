
add_executable(07.29 MGraph.h MGraph.c 07.29.c)
target_include_directories(07.29 PUBLIC ${CMAKE_SOURCE_DIR}/Status)
target_link_libraries(07.29 PUBLIC Scanf_lib)

# 记录要拷贝到*.exe目录下的资源文件
file(GLOB TestData TestData*.txt)
# 将资源文件拷贝到*.exe目录下，不然无法加载
file(COPY ${TestData} DESTINATION ${CMAKE_CURRENT_BINARY_DIR})
