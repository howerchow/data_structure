
add_executable(06.50 LinkQueue.h LinkQueue.c BiTree.h BiTree.c 06.50.c)
target_include_directories(06.50 PUBLIC ${CMAKE_SOURCE_DIR}/Status)
target_link_libraries(06.50 PUBLIC Scanf_lib)

# 记录要拷贝到*.exe目录下的资源文件
file(GLOB TestData TestData*.txt)
# 将资源文件拷贝到*.exe目录下，不然无法加载
file(COPY ${TestData} DESTINATION ${CMAKE_CURRENT_BINARY_DIR})
