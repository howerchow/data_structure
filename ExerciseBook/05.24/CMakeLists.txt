
add_executable(05.24 SMatrix.h SMatrix.c 05.24.c)
target_include_directories(05.24 PUBLIC ${CMAKE_SOURCE_DIR}/Status)
target_link_libraries(05.24 PUBLIC Scanf_lib)


# 记录要拷贝到*.exe目录下的资源文件
file(GLOB TestData TestData*.txt)
# 将资源文件拷贝到*.exe目录下，不然无法加载
file(COPY ${TestData} DESTINATION ${CMAKE_CURRENT_BINARY_DIR})
