
add_executable(05.38.1 SString.h SString.c GList-HT.h GList-HT.c 05.38.1.c)
target_include_directories(05.38.1 PUBLIC ${CMAKE_SOURCE_DIR}/Status)
target_link_libraries(05.38.1 PUBLIC Scanf_lib)
