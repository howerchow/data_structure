/*************************
 *                       *
 * 文件夹: ▲10 内部排序 *
 *                       *
 * 文件名: BInsertSort.c *
 *                       *
 * 算  法: 10.2          *
 *                       *
 *************************/

#include "BInsertSort.h" //**▲10 内部排序**//

/*════╗
║算法10.2║
╚════*/
void BInsertSort( SqList_sort *L ) {

        for ( int i = 1; i < L->length; ++i ) {
                RcdType temp = L->r[ i ];
                int low = 0;
                int high = i - 1;

                while ( low <= high ) // 在r[low..high]中折半查找有序插入的位置
                {
                        int m = ( low + high ) / 2; // 折半

                        if ( LT( temp.key,
                                 L->r[ m ].key ) ) // 插入点在低半区
                        {
                                high = m - 1;
                        } else { // 插入点在高半区
                                low = m + 1;
                        }
                }

                for ( int j = i - 1; j >= high + 1; --j ) { // 记录后移
                        L->r[ j + 1 ] = L->r[ j ];
                }

                L->r[ high + 1 ] = temp; // 插入
        }
}
