/************************************
 *                                  *
 * 文件夹: ▲10 内部排序            *
 *                                  *
 * 文件名: BInsertSort.h            *
 *                                  *
 * 内  容: 折半插入排序相关操作列表 *
 *                                  *
 ************************************/

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include "SequenceListType.h" //**▲10 内部排序**//

/* 折半插入排序函数列表 */
void BInsertSort( SqList_sort *L );
/*━━━━━━━━━━━━━━━━━━━━┓
┃(01)算法10.2：对顺序表L作折半插入排序。 ┃
┗━━━━━━━━━━━━━━━━━━━━*/

#ifdef __cplusplus
}
#endif
