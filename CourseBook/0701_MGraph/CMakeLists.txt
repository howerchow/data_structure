# 生成可执行文件
add_executable(MGraph LinkQueue.h LinkQueue.c MGraph.h MGraph.c MGraph-main.c)
target_include_directories(MGraph PUBLIC ${CMAKE_SOURCE_DIR}/Status)
# 链接公共库
target_link_libraries(MGraph Scanf_lib)

# 记录要拷贝到*.exe目录下的资源文件
file(GLOB TestData TestData*.txt)
# 将资源文件拷贝到*.exe目录下，不然无法加载
file(COPY ${TestData} DESTINATION ${CMAKE_CURRENT_BINARY_DIR})
