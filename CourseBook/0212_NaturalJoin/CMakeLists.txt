
add_executable(NaturalJoin NaturalJoin.h NaturalJoin.c NaturalJoin-main.c)
target_include_directories(NaturalJoin PUBLIC ${CMAKE_SOURCE_DIR}/Status)
target_link_libraries(NaturalJoin Scanf_lib)

file(GLOB TestData TestData*.txt)
file(COPY ${TestData} DESTINATION ${CMAKE_CURRENT_BINARY_DIR})
