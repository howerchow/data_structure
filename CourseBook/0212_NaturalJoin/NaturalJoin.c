#include "NaturalJoin.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

Status CreateTable( HList *H, FILE *fp ) {
        // 创建头结点
        H->head = calloc( 1, sizeof( DList ) );
        DList *r = H->head;

        ReadData( fp, "%d", &( H->row ) );
        ReadData( fp, "%d", &( H->col ) );

        for ( int i = 0; i < H->row; ++i ) {
                DList *dl = calloc( 1, sizeof( DList ) );
                for ( int j = 0; j < H->col; ++j ) {
                        ReadData( fp, "%d", &( dl->data[ j ] ) );
                }

                // 尾插法 插入结点
                r->next = dl;
                r = dl;
        }

        return OK;
}

Status DestoryTable( HList *H ) {
        if ( H == nullptr || H->head == nullptr ) {
                return ERROR;
        }

        DList **p = &H->head;
        while ( ( *p ) != nullptr ) {
                DList *entry = *p;
                p = &entry->next;
                free( entry );
        }
        free( H->head );

        return OK;
}

Status PrintTable( HList *H ) {
        if ( H == nullptr || H->head == nullptr ) {
                return ERROR;
        }

        DList *p = H->head->next;

        while ( p != nullptr ) {
                for ( int j = 0; j < H->col; ++j ) {
                        printf( "%4d", p->data[ j ] );
                }
                p = p->next;
                printf( "\n" );
        }
        printf( "\n" );

        return OK;
}

Status NaturalJoin( const HList *H1, const HList *H2, HList *H3, int c1,
                    int c2 ) {

        if ( H1 == nullptr || H1->head == nullptr || H2 == nullptr ||
             H2->head == nullptr ) {
                return ERROR;
        }

        // 创建头结点
        H3->head = calloc( 1, sizeof( DList ) );
        H3->row = 0;
        H3->col = H1->col + H2->col;

        DList *r = H3->head;

        DList *p1 = H1->head->next;
        while ( p1 != nullptr ) {

                DList *p2 = H2->head->next;
                while ( p2 != nullptr ) {
                        if ( p1->data[ c1 ] != p2->data[ c2 ] ) {
                                p2 = p2->next;
                                continue;
                        }

                        // 尾插法 插入结点
                        DList *dl = calloc( 1, sizeof( DList ) );
                        for ( int i = 0; i < H3->col; ++i ) {
                                if ( i < H1->col ) {
                                        dl->data[ i ] = p1->data[ i ];
                                } else {
                                        dl->data[ i ] = p2->data[ i - H1->col ];
                                }
                        }
                        r->next = dl;
                        r = dl;
                        ++H3->row; // 更新列数

                        p2 = p2->next;
                }

                p1 = p1->next;
        }

        return OK;
}
