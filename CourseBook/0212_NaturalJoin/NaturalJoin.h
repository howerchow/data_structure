/*=========================
 * 自然连接
 * (顺序表和链表的混合使用)
 *
 =========================*/

#include "Status.h"

#define MaxCol 10

typedef int ElemType;

typedef struct DList {
        ElemType data[ MaxCol ];
        struct DList *next;
} DList;

typedef struct HList {
        int row, col;
        DList *head;
} HList, *HListPtr;

Status CreateTable( HList *H, FILE *fp );
Status DestoryTable( HList *H );
Status PrintTable( HList *H );

Status NaturalJoin( const HList *H1, const HList *H2, HList *H3, int c1,
                    int c2 );
