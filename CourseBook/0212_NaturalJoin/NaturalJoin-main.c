
#include "NaturalJoin.h"
#include <stdio.h>

int main( int argc, char *argv[] ) {
        HList h1, h2;

        printf( "创建并输出2个Table..\n" );
        {
                FILE *fp1 = fopen( "TestData1.txt", "r" );
                FILE *fp2 = fopen( "TestData2.txt", "r" );

                CreateTable( &h1, fp1 );
                CreateTable( &h2, fp2 );

                PrintTable( &h1 );
                PrintTable( &h2 );

                fclose( fp1 );
                fclose( fp2 );
        }
        PressEnterToContinue( debug );

        printf( "自然连接...\n" );
        {
                HList h3;

                NaturalJoin( &h1, &h2, &h3, 2, 0 );
                PrintTable( &h3 );

                DestoryTable( &h1 );
                DestoryTable( &h2 );
                DestoryTable( &h3 );
        }
        PressEnterToContinue( debug );

        return 0;
}
