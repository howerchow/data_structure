/*=======================
 * 扩展的单链表（线性链表）
 *
 * 包含算法: 2.20
 ========================*/

#include "ELinkList.h" //**▲02 线性表**//
#include <limits.h>    // 提供一些极限常量
#include <stdio.h>
#include <stdlib.h> // 提供malloc、realloc、free、exit原型

/*━━━━━━━━━━━━━━━━━━━━━━ 内存操作 ━━━━━━━━━━━━━━━━━━━━━━*/

/*
 * 内存分配
 *
 * 为线性链表申请一个结点，并存入指定的数据e。
 */
Status MakeNode( Link *p, ElemType e ) {
        if ( p == nullptr ) {
                return ERROR;
        }

        // 申请空间
        *p = malloc( sizeof( LNode ) );
        if ( *p == nullptr ) {
                // 这里没有退出程序，而是返回错误提示
                return ERROR;
        }

        ( *p )->data = e;
        ( *p )->next = nullptr;

        return OK;
}

/*
 * 内存回收
 *
 * 释放线性链表中指定的结点。
 */
void FreeNode( Link *p ) {
        if ( p == nullptr || *p == nullptr ) {
                return;
        }

        free( *p );
        *p = nullptr;
}

/*━━━━━━━━━━━━━━━━━━━━━━ 链表常规操作 ━━━━━━━━━━━━━━━━━━━━━━*/

/*
 * 初始化
 *
 * 初始化成功则返回OK，否则返回ERROR。
 */
Status InitList( ELinkList *L ) {

        if ( L == nullptr ) {
                return ERROR;
        }

        // 创建头结点
        Link p = malloc( sizeof( LNode ) );
        if ( p == nullptr ) {
                exit( OVERFLOW );
        }
        p->next = nullptr;

        // 只有头结点时，首位游标指向自身
        L->head = L->tail = p;
        L->len = 0;

        return OK;
}

/*
 * 销毁(结构)
 *
 * 释放链表所占内存。
 */
Status DestroyList( ELinkList *L ) {

        // 链表不存在时没必要销毁
        if ( L == nullptr || L->head == nullptr ) {
                return ERROR;
        }

        ClearList( L );

        free( L->head );

        L->head = L->tail = nullptr;

        return OK;
}

/*
 * 置空(内容)
 *
 * 这里需要释放链表中非头结点处的空间。
 */
Status ClearList( ELinkList *L ) {
        Link p, q;

        // 没有有效元素时不需要清理
        if ( L == nullptr || L->head == nullptr || L->len <= 0 ) {
                return ERROR;
        }

        // 指向第1个元素
        p = L->head->next;

        // 释放所有元素所占内存
        while ( p != nullptr ) {
                q = p->next;
                free( p );
                p = q;
        }

        L->head->next = nullptr;
        L->tail = L->head;
        L->len = 0;

        return OK;
}

/*
 * 判空
 *
 * 判断链表中是否包含有效数据。
 *
 * 返回值：
 * true : 链表为空
 * false: 链表不为空
 */
Status ListEmpty( ELinkList L ) {
        if ( L.len <= 0 ) {
                return true;
        } else {
                return false;
        }
}

/*
 * 计数
 *
 * 返回链表包含的有效元素的数量。
 */
int ListLength( ELinkList L ) {
        return L.len;
}

/*
 * 查找
 *
 * 返回链表中首个与e满足Compare关系的元素引用。
 * 如果不存在这样的元素，则返回nullptr。
 *
 *【备注】
 * 1.元素e是Compare函数第二个形参
 * 2.这里的返回值是目标元素的引用，而不是其位序
 */
Position LocateElem( ELinkList L, ElemType e,
                     bool ( *Compare )( ElemType, ElemType ) ) {
        if ( L.len <= 0 ) {
                return nullptr;
        }

        // 指向第1个元素
        Position p = L.head->next;

        while ( p != nullptr && !Compare( p->data, e ) ) {
                p = p->next;
        }

        return p;
}

/*
 * ████████ 算法2.20 ████████
 *
 * 插入
 *
 * 向链表第i个位置上插入e，插入成功则返回OK，否则返回ERROR。
 *
 *【备注】
 * 教材中i的含义是元素位置，从1开始计数
 * 可以看做是算法2.9的改写
 */
Status ListInsert( ELinkList *L, int i, ElemType e ) {
        if ( L == nullptr || L->head == nullptr ) {
                return ERROR;
        }
        // 确保i值合规[1, len+1]
        if ( i < 1 || i > L->len + 1 ) {
                return ERROR;
        }

        Link h, s;
        // 查找第i-1个元素的引用，存储在h中
        if ( LocatePos( *L, i - 1, &h ) == ERROR ) {
                return ERROR;
        }

        // 分配新结点s
        if ( MakeNode( &s, e ) == ERROR ) {
                return ERROR;
        }

        // 将s结点插入到h结点后面，成为h后面的第一个结点
        if ( InsFirst( L, h, s ) == ERROR ) {
                return ERROR;
        }

        return OK;
}

/*
 * 删除
 *
 * 删除链表第i个位置上的元素，并将被删除元素存储到e中。
 * 删除成功则返回OK，否则返回ERROR。
 *
 *【备注】
 * 教材中i的含义是元素位置，从1开始计数
 * 可以看做是算法2.10的改写
 */
Status ListDelete( ELinkList *L, int i, ElemType *e ) {
        if ( L == nullptr || L->head == nullptr ) {
                return ERROR;
        }

        // 确保i值合规[1, len]
        if ( i < 1 || i > L->len ) {
                return ERROR;
        }

        Link h, q;
        // 查找第i-1个元素的引用，存储在h中
        if ( LocatePos( *L, i - 1, &h ) == ERROR ) {
                return ERROR;
        }

        // 删除h结点后的第一个结点，并用q存储被删除结点的引用
        if ( DelFirst( L, h, &q ) == ERROR ) {
                return ERROR;
        }

        // 记下被删除元素的值
        *e = q->data;

        // 释放被删除结点的空间
        FreeNode( &q );

        return OK;
}

/*
 * 遍历
 *
 * 用visit函数访问链表L
 */
void ListTraverse( ELinkList L, void ( *Visit )( ElemType ) ) {

        if ( L.len <= 0 ) {
                return;
        }

        // 指向第1个元素
        Link p = L.head->next;

        while ( p != nullptr ) {
                Visit( p->data );
                p = p->next;
        }

        printf( "\n" );
}

/*━━━━━━━━━━━━━━━━━━━━━━ 链表扩展操作 ━━━━━━━━━━━━━━━━━━━━━━*/

/*
 * 取值
 *
 * 获取结点p的元素值。
 */
ElemType GetCurElem( Link p ) {
        if ( p == nullptr ) {
                return INT_MIN;
        }

        return p->data;
}

/*
 * 设值
 *
 * 为结点p设置元素值。
 */
Status SetCurElem( Link p, ElemType e ) {
        if ( p == nullptr ) {
                return ERROR;
        }

        p->data = e;

        return OK;
}

/*
 * 头结点
 *
 * 获取头结点引用。
 */
Position GetHead( ELinkList L ) {
        return L.head;
}

/*
 * 尾结点
 *
 * 获取尾结点引用。
 */
Position GetLast( ELinkList L ) {
        return L.tail;
}

/*
 * 前驱
 *
 * 获取结点p的前驱，如果不存在，则返回nullptr。
 */
Position PriorPos( ELinkList L, Link p ) {

        // 确保链表(头结点)存在
        if ( L.head == nullptr ) {
                return nullptr;
        }

        if ( p == nullptr ) {
                return nullptr;
        }
        // 指向头结点
        Link pre = L.head;

        // 第一个结点无前驱
        if ( pre->next == p ) {
                return nullptr;
        }

        // 查找P的前驱
        while ( pre != nullptr && pre->next != p ) {
                pre = pre->next;
        }

        return pre;
}

/*
 * 后继
 *
 * 获取结点p的后继，如果不存在，则返nullptr。
 */
Position NextPos( ELinkList L, Link p ) {

        // 确保链表(头结点)存在
        if ( L.head == nullptr ) {
                return nullptr;
        }

        if ( p == nullptr ) {
                return nullptr;
        }

        return p->next;
}

/*
 * 查找
 *
 * 查找链表L中第i(允许为0)个结点，并将其引用存入p，且返回OK
 * 如果i值不合规，则返回ERROR
 * 特别注意，当i为0时，p存储的是头结点的引用
 */
Status LocatePos( ELinkList L, int i, Link *p ) {
        // 注：i允许为0
        if ( i < 0 || i > L.len ) {
                return ERROR;
        }

        // 保证链表(头结点)存在
        if ( L.head == nullptr ) {
                return ERROR;
        }

        // i为0时，取头结点
        if ( i == 0 ) {
                *p = L.head;
                return OK;
        }

        int j = 0;       // 计数
        Link r = L.head; // 指向头结点

        while ( r != nullptr && j < i ) {
                ++j;
                r = r->next;
        }

        if ( r == nullptr ) {
                return ERROR;
        }

        *p = r;

        return OK;
}

/*
 * 插入
 *
 * 将s结点插入到h结点后面，成为h后面的第一个结点
 *
 *【备注】
 * 教材中对于该方法的描述有些问题，这里是修正过的版本
 */
Status InsFirst( ELinkList *L, Link h, Link s ) {
        if ( L == nullptr || L->head == nullptr || h == nullptr ||
             s == nullptr ) {
                return ERROR;
        }

        s->next = h->next;
        h->next = s;

        // 若h为尾结点，则需要更新尾结点
        if ( h == L->tail ) {
                L->tail = h->next;
        }

        L->len++;

        return OK;
}

/*
 * 删除
 *
 * 删除h结点后的第一个结点，并用q存储被删除结点的引用
 *
 *【备注】
 * 教材中对于该方法的定义略显粗糙，这里是修正过的版本
 */
Status DelFirst( ELinkList *L, Link h, Link *q ) {
        if ( L == nullptr || L->head == nullptr || h == nullptr ||
             q == nullptr ) {
                return ERROR;
        }

        // 如果没有结点可删除，返回错误信息
        if ( h->next == nullptr ) {
                return ERROR;
        }

        *q = h->next;

        h->next = ( *q )->next;

        // 将被删除结点变成孤立的结点
        ( *q )->next = nullptr;

        // 如果h后只有一个结点，更改尾结点指针
        if ( h->next == nullptr ) {
                L->tail = h;
        }

        // 并不释放被删结点所占空间
        L->len--;

        return OK;
}

/*
 * 前向插入
 *
 * 将s结点插入到p结点之前，并将p指向新结点
 */
Status InsBefore( ELinkList *L, Link *p, Link s ) {

        if ( L == nullptr || L->head == nullptr || p == nullptr ||
             s == nullptr ) {
                return ERROR;
        }

        // 指向头结点
        Link pre = L->head;

        // 查找p结点的广义前驱：即对于第一个元素，其前驱为头结点
        while ( pre != nullptr && pre->next != ( *p ) ) {
                pre = pre->next;
        }

        // 没找到广义前驱
        if ( pre == nullptr ) {
                return ERROR;
        }

        s->next = *p;
        pre->next = s;
        *p = s;

        L->len++; // 修改len，需用到*L

        return OK;
}

/*
 * 后向插入
 *
 * 将s结点插入到p结点之前，并将p指向新结点
 */
Status InsAfter( ELinkList *L, Link *p, Link s ) {

        if ( L == nullptr || L->head == nullptr || p == nullptr ||
             s == nullptr ) {
                return ERROR;
        }

        Link r = L->head;

        while ( r != nullptr && r != ( *p ) ) {
                r = r->next;
        }

        // 如果未找到结点p，返回错误信息
        if ( r == nullptr ) {
                return ERROR;
        }

        // 如果p指向最后一个结点，则需要更新尾指针
        if ( *p == L->tail ) {
                L->tail = s;
        }

        s->next = ( *p )->next;
        ( *p )->next = s;
        *p = s;

        L->len++;

        return OK;
}

/*
 * 向尾部添加
 *
 * 将s所指的一串结点链接在链表L后面
 */
Status Append( ELinkList *L, Link s ) {

        if ( L == nullptr || L->head == nullptr || s == nullptr ) {
                return ERROR;
        }

        int count = 0;
        L->tail->next = s;

        // 确定新的尾结点位置
        while ( s != nullptr ) {
                L->tail = s;
                s = s->next;
                ++count;
        }

        L->len += count;

        return OK;
}

/*
 * 从尾部移除
 *
 * 将链表的尾结点移除，并将被移除的结点引用存储在q中
 */
Status Remove( ELinkList *L, Link *q ) {

        if ( L == nullptr || L->head == nullptr || q == nullptr ) {
                return ERROR;
        }

        // 没有元素可供移除
        if ( L->len == 0 ) {
                *q = nullptr;
                return ERROR;
        }

        *q = L->tail;

        // 确定新的尾结点位置
        Link p = L->head;
        while ( p->next != L->tail ) {
                p = p->next;
        }
        p->next = nullptr;
        L->tail = p;

        L->len--;

        return OK;
}
