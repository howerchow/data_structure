/*************************
 *                       *
 * 文件夹: ▲10 内部排序 *
 *                       *
 * 文件名: SelectSort.c  *
 *                       *
 * 算  法: 10.9          *
 *                       *
 *************************/

#include "SelectSort.h" //**▲10 内部排序**//

static void SelectSwap( SqList_sort *L, int i, int j ) {
        if ( L == nullptr || i == j ) {
                return;
        }

        RcdType temp = L->r[ i ];
        L->r[ i ] = L->r[ j ];
        L->r[ j ] = temp;
}

/* (02)在L.r[i..L.length]中选择key最小的记录。*/
static int SelectMinKey( SqList_sort L, int i ) {
        int min = i;

        for ( int k = i; k < L.length; ++k ) {
                if ( L.r[ k ].key < L.r[ min ].key ) {
                        min = k;
                }
        }

        return min;
}

/*
 * 算法10.9: 对顺序表L作简单选择排序
 *
 * 选择排序 较冒泡排序 交换次数较少 效率更高
 *
 */
void SelectSort( SqList_sort *L ) {

        for ( int i = 0; i < L->length; ++i ) {
                int min = SelectMinKey( *L, i );
                SelectSwap( L, i, min ); // key最小的记录交换到i
        }
}
