add_executable(SelectSort SelectSort.h SelectSort.c SelectSort-main.c SequenceListType.h SequenceListType.c)
target_include_directories(SelectSort PUBLIC ${CMAKE_SOURCE_DIR}/Status)
target_link_libraries(SelectSort PUBLIC Scanf_lib)

file(GLOB TestData TestData*.txt)
file(COPY ${TestData} DESTINATION ${CMAKE_CURRENT_BINARY_DIR})
