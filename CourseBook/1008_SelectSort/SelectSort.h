/************************************
 *                                  *
 * 文件夹: ▲10 内部排序            *
 *                                  *
 * 文件名: SelectSort.h             *
 *                                  *
 * 内  容: 简单选择排序相关操作列表 *
 *                                  *
 ************************************/

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include "SequenceListType.h" //**▲10 内部排序**//

/* 简单选择排序函数列表 */
void SelectSort( SqList_sort *L );
/*━━━━━━━━━━━━━━━━━━━━┓
┃(01)算法10.9：对顺序表L作简单选择排序。 ┃
┗━━━━━━━━━━━━━━━━━━━━*/

#ifdef __cplusplus
}
#endif
