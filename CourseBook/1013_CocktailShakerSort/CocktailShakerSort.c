/********************************
 *                               *
 * 文件夹: ▲10 鸡尾酒排序        *
 *                               *
 * 文件名: CocktailShakerSort.c  *
 *                               *
 ********************************/

#include "CocktailShakerSort.h" //**▲10 内部排序**//
#include <stddef.h>

static void CocktailShakerSwap( SqList_sort *L, int i, int j ) {
        if ( L == nullptr || i == j || i < 0 || j < 0 ) {
                return;
        }

        RcdType temp = L->r[ i ];
        L->r[ i ] = L->r[ j ];
        L->r[ j ] = temp;
}

void CocktailShakerSort( SqList_sort *L ) {
        if ( L == nullptr ) {
                return;
        }

        int start = 0;
        int end = L->length - 1;
        bool swapped = true; // 标记是否进行了交换

        while ( swapped ) {
                swapped = false;

                // 从左到右遍历，将最大的元素移到最右端
                for ( int i = start; i < end; ++i ) {
                        if ( L->r[ i ].key > L->r[ i + 1 ].key ) {
                                CocktailShakerSwap( L, i, i + 1 );
                                swapped = true;
                        }
                }

                // 如果没有交换，说明数组已经有序，退出循环
                if ( !swapped ) {
                        break;
                }

                // 更新右边界
                --end;
                swapped = false;

                // 从右到左遍历，将最小的元素移到最左端
                for ( int i = end; i > start; --i ) {
                        if ( L->r[ i ].key < L->r[ i - 1 ].key ) {
                                CocktailShakerSwap( L, i, i - 1 );
                                swapped = true;
                        }
                }

                // 更新左边界
                ++start;
        }
}

void CocktailShakerSort_1( SqList_sort *L ) {
        if ( L == nullptr ) {
                return;
        }

        int start = 0;
        int end = L->length - 1;
        bool swapped = true; // 标记是否进行了交换

        int k1, k2;
        while ( swapped ) {
                swapped = false;

                // 从左到右遍历，将最大的元素移到最右端
                for ( int i = start; i < end; ++i ) {
                        if ( L->r[ i ].key > L->r[ i + 1 ].key ) {
                                CocktailShakerSwap( L, i, i + 1 );
                                swapped = true;
                                k1 = i;
                        }
                }

                // 如果没有交换，说明数组已经有序，退出循环
                if ( !swapped ) {
                        break;
                }

                // 更新右边界
                // --end;
                end = k1;
                swapped = false;

                // 从右到左遍历，将最小的元素移到最左端
                for ( int i = end; i > start; --i ) {
                        if ( L->r[ i ].key < L->r[ i - 1 ].key ) {
                                CocktailShakerSwap( L, i, i - 1 );
                                swapped = true;
                                k2 = i;
                        }
                }

                // 更新左边界
                //++start;
                start = k2;
        }
}
