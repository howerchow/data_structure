/********************************
 *                              *
 * 文件夹: ▲10 鸡尾酒排序        *
 *                              *
 * 文件名: CocktailShakerSort.h         *
 *                              *
 * 内  容: 鸡尾酒排序相关操作列表 *
 *                              *
 ********************************/

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include "SequenceListType.h" //**▲10 内部排序**//

/* 鸡尾酒排序函数列表 */
void CocktailShakerSort( SqList_sort *L );
void CocktailShakerSort_1( SqList_sort *L );
/*━━━━━━━━━━━━━┓
┃(01)对顺序表L作鸡尾酒排序。 ┃
┗━━━━━━━━━━━━━*/

#ifdef __cplusplus
}
#endif
