/**********************************
 *                                *
 * 文件夹: ▲09 查找              *
 *                                *
 * 文件名: BitMap.h               *
 *                                *
 * 内  容: 位图相关操作列表      *
 *                                *
 **********************************/

#pragma once

#include "Status.h" //**▲01 绪论**//

typedef int* BitMap;


BitMap BitMap_InitMap( size_t n );

Status BitMap_DestroyMap( BitMap B);

Status BitMap_Add( BitMap B, int n );

Status BitMap_Remove( BitMap B, int n );

Status BitMap_Flip( BitMap B, int n );

bool BitMap_Contain(const BitMap B, int n );
