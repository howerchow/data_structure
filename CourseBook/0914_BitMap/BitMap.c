#include "BitMap.h" //**▲09 查找**//

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

BitMap BitMap_InitMap( size_t n ) {
        BitMap B = nullptr;
        B = calloc( ( n + 32 - 1 ) / 32, sizeof( *B ) );
        return B;
}

Status BitMap_DestroyMap( BitMap B ) {
        free( B );
        return OK;
}

Status BitMap_Add( BitMap B, int n ) {
        if ( B == nullptr ) {
                return ERROR;
        }

        B[ n / 32 ] |= 1 << ( n % 32 );

        if ( ( B[ n / 32 ] >> ( n % 32 ) & 0x01 ) == 1 ) {
                return OK;
        }
        return ERROR;
}

Status BitMap_Remove( BitMap B, int n ) {
        if ( B == nullptr ) {
                return ERROR;
        }

        B[ n / 32 ] &= ~( 1 << ( n % 32 ) );

        if ( ( B[ n / 32 ] >> ( n % 32 ) & 0x01 ) == 0 ) {
                return OK;
        }
        return ERROR;

        return OK;
}

Status BitMap_Flip( BitMap B, int n ) {
        if ( B == nullptr ) {
                return ERROR;
        }

        B[ n / 32 ] ^= 1 << ( n % 32 );

        return OK;
}

bool BitMap_Contain( const BitMap B, int n ) {
        if ( B == nullptr ) {
                return false;
        }

        return ( ( B[ n / 32 ] >> ( n % 32 ) ) & 0x01 ) == 1 ? true : false;
}
