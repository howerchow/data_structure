/**********************************
 *						          *
 * 文件夹: ▲09 查找              *
 * 						          *
 * 文件名: BitMap-main.c       *
 * 						          *
 * 内  容: 位图查找相关函数测试 *
 *                                *
 **********************************/

#include "BitMap.h" //**▲09 查找**//
#include <stdio.h>

static Status ReadIntoBitMap( FILE *fp1, FILE *fp2, BitMap B, size_t size ) {
        if ( fp1 == nullptr || fp2 == nullptr || B == nullptr ) {
                return ERROR;
        }

        int temp;
        for ( size_t i = 0; i < size / 2; ++i ) {
                ReadData( fp1, "%d", &temp );
                if ( BitMap_Add( B, temp ) != OK ) {
                        printf( "BitMap_Add() ERROR \n" );
                }
        }
        for ( size_t i = size / 2; i < size; ++i ) {
                ReadData( fp2, "%d", &temp );
                if ( BitMap_Add( B, temp ) != OK ) {
                        printf( "BitMap_Add() ERROR \n" );
                }
        }
        return OK;
}

int main( int argc, char *argv[] ) {
        size_t Size = 20000000;
        int t = 4235602;

        printf( "测试 %d 是否在位图里: \n", t );
        {
                BitMap B = BitMap_InitMap( Size );
                FILE *fp1 = fopen( "TestData1.txt", "r" );
                FILE *fp2 = fopen( "TestData2.txt", "r" );

                ReadIntoBitMap( fp1, fp2, B, Size );
                printf( "%d \n", BitMap_Contain( B, t ) );

                BitMap_Remove( B, t );
                printf( "%d \n", BitMap_Contain( B, t ) );

                BitMap_Flip( B, t );
                printf( "%d \n", BitMap_Contain( B, t ) );

                BitMap_DestroyMap( B );
        }
        PressEnterToContinue( debug );

        return 0;
}
