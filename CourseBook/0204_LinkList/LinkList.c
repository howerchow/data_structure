/*===============================
 * 线性表的链式存储结构（链表）
 *
 * 包含算法: 2.8、2.9、2.10、2.11
 ================================*/

#include "LinkList.h" //**▲02 线性表**//
#include "Status.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h> // strstr

/*
 * 初始化
 *
 * 只是初始化一个头结点。
 * 初始化成功则返回OK，否则返回ERROR。
 */
Status LinkList_InitList( LinkList *L ) {

        *L = calloc( 1, sizeof( LinkNode ) );
        if ( *L == nullptr ) {
                exit( OVERFLOW );
        }

        ( *L )->next = nullptr;

        return OK;
}

LinkList CreateList( ) {
        LinkList L = calloc( 1, sizeof( LinkNode ) );
        if ( L == nullptr ) {
                exit( OVERFLOW );
        }

        L->next = nullptr;

        return L;
}

/*
 * 销毁(结构)
 *
 * 释放链表所占内存，头结点也会被清理。
 */
Status LinkList_DestroyList( LinkList *L ) {
        if ( L == nullptr || *L == nullptr ) {
                return ERROR;
        }

        LinkList p = *L;

        while ( p != nullptr ) {
                p = ( *L )->next;
                free( *L );
                ( *L ) = p;
        }

        *L = nullptr;

        return OK;
}

/*
 * 置空(内容)
 *
 * 这里需要释放链表中非头结点处的空间。
 */
Status LinkList_ClearList( LinkList L ) {
        if ( L == nullptr ) {
                return ERROR;
        }

        LinkList pre = L->next;
        LinkList p = L->next;

        // 释放链表上所有结点所占内存
        while ( p != nullptr ) {
                pre = p;
                p = p->next;
                free( pre );
        }

        L->next = nullptr;

        return OK;
}

/*
 * 判空
 *
 */
inline bool LinkList_ListEmpty( const LinkList L ) {
        if ( L == nullptr || L->next == nullptr ) {
                return true;
        }
        return false;
}

/*
 * 长度
 *
 * 返回链表包含的有效元素的数量。
 */
int LinkList_ListLength( const LinkList L ) {
        if ( L == nullptr ) {
                return 0;
        }

        int i = 0;
        LinkList p = L->next;

        // 遍历所有结点
        while ( p != nullptr ) {
                ++i;
                p = p->next;
        }

        return i;
}

/*
 * ████████ 算法2.8 ████████
 *
 * 取值
 *
 * 获取链表中第i个元素，将其存储到e中。
 * 如果可以找到，返回OK，否则，返回ERROR。
 *
 *【备注】
 * 教材中i的含义是元素位置，从1开始计数，但这不符合编码的通用约定。
 * 通常，i的含义应该指索引，即从0开始计数。
 */
Status LinkList_GetElem( const LinkList L, int i, ElemType *e ) {
        if ( L == nullptr ) {
                return ERROR;
        }

        LinkList p = L;
        int j = 0;

        // 寻找第i-1个结点，且保证该结点的后继不为nullptr
        while ( p->next != nullptr && j < i - 1 ) {
                p = p->next;
                ++j;
        }

        // 如果遍历到头了，或者i的值不合规(比如i<=0)，说明没找到合乎目标的结点
        if ( p->next == nullptr || j > i - 1 ) {
                return ERROR;
        }

        *e = p->next->data;

        return OK;
}

ElemType GetElem_1( const LinkList L, const int i ) {
        if ( L == nullptr ) {
                return ERROR;
        }

        LinkList p = L;
        int j = 0;

        // 寻找第i-1个结点，且保证该结点的后继不为nullptr
        while ( p->next != nullptr && j < i - 1 ) {
                p = p->next;
                ++j;
        }

        // 如果遍历到头了，或者i的值不合规(比如i<=0)，说明没找到合乎目标的结点
        if ( p->next == nullptr || j > i - 1 ) {
                return ERROR;
        }

        return p->next->data;
}

LinkListIter *LinkList_GetIterator( LinkList list ) {
        if ( list == nullptr ) {
                return nullptr;
        }
        LinkListIter *iter = malloc( sizeof( *iter ) );
        if ( iter == nullptr ) {
                return nullptr;
        }

        iter->next = list->next;

        return iter;
}

LinkNode *LinkList_ListNext( LinkListIter *iter ) {
        if ( iter == nullptr ) {
                return nullptr;
        }

        LinkNode *current = iter->next;

        if ( current != nullptr ) {
                iter->next = current->next;
        }
        return current;
}

void LinkList_ReleaseIterator( LinkListIter *iter ) {
        if ( iter == nullptr ) {
                return;
        }

        free( iter );
}

/*
 * 查找
 *
 * 返回链表中首个与e满足Compare关系的元素位序。
 * 如果不存在这样的元素，则返回0。
 *
 *【备注】
 * 元素e是Compare函数第二个形参
 */
int LinkList_LocateElem( const LinkList L, ElemType e,
                         bool ( *Compare )( ElemType, ElemType ) ) {
        if ( L == nullptr ) {
                return 0;
        }

        int i = 1;            // i的初值为第1个元素的位序
        LinkList p = L->next; // p的初值为第1个元素的指针

        while ( p != nullptr && !Compare( p->data, e ) ) {
                ++i;
                p = p->next;
        }

        if ( p != nullptr ) {
                return i;
        }
        return 0;
}

/*
 *  中间结点
 *  F: 第一个结点(非头结点)
 * 结点数为偶数时判断q->next是否为nullptr
 * 结点数为奇数时判断q是否为nullptr
 */
LinkNode *LinkList_ListMiddleElem( const LinkList L ) {
        if ( L == nullptr ) {
                return nullptr;
        }

        LinkNode *slow = L;
        LinkNode *fast = L;

        while ( fast != nullptr && fast->next != nullptr ) {
                slow = slow->next;
                fast = fast->next->next;
        }
        return slow;
}

LinkNode *LinkList_ListLastKElem( const LinkList L, int K ) {
        if ( L == nullptr ) {
                return nullptr;
        }

        int Length = LinkList_ListLength( L );
        if ( K <= 0 || K > Length ) {
                return nullptr;
        }
        LinkNode *p = L->next;
        for ( int i = 0; i < Length - K; ++i ) {
                p = p->next;
        }
        return p;
}

/*
 * 前驱
 *
 * 获取元素cur_e的前驱，
 * 如果存在，将其存储到pre_e中，返回OK，
 * 如果不存在，则返回ERROR。
 */
Status LinkList_PriorElem( const LinkList L, ElemType cur_e, ElemType *pre_e ) {

        if ( L == nullptr ) {
                return ERROR;
        }
        LinkList pre, next;

        // 指向第1个元素
        pre = L->next;

        // 第1个元素没有前驱
        if ( pre->data == cur_e ) {
                return ERROR;
        }

        // 指向第2个元素
        next = pre->next;

        // 从第2个元素开始，查找cur_e的位置
        while ( next != nullptr && next->data != cur_e ) {
                pre = next;
                next = next->next;
        }

        // 如果没找到元素cur_e，查找失败，返回ERROR
        if ( next == nullptr ) {
                return ERROR;
        }

        *pre_e = pre->data;

        return OK;
}

/*
 * 后继
 *
 * 获取元素cur_e的后继，
 * 如果存在，将其存储到next_e中，返回OK，
 * 如果不存在，则返回ERROR。
 */
Status LinkList_NextElem( const LinkList L, ElemType cur_e, ElemType *next_e ) {
        if ( L == nullptr ) {
                return ERROR;
        }
        // 指向第1个元素
        LinkList pre = L->next;

        // 从第1个元素开始，查找cur_e的位置，且保证该结点的后继不为nullptr
        while ( pre->next != nullptr && pre->data != cur_e ) {
                pre = pre->next;
        }

        // 如果没找到cur_e，或者找到了，但它没有后继，均返回ERROR
        if ( pre->next == nullptr ) {
                return ERROR;
        }

        *next_e = pre->next->data;

        return OK;
}

/*
 * ████████ 算法2.9 ████████
 *
 * 插入
 *
 * 向链表第i个位置上插入e，插入成功则返回OK，否则返回ERROR。
 *
 *【备注】
 * 教材中i的含义是元素位置，从1开始计数
 */
Status LinkList_ListInsert( LinkList L, int i, ElemType e ) {
        if ( L == nullptr ) {
                return ERROR;
        }

        LinkList p = L;
        int j = 0;

        // 寻找第i-1个结点，且保证该结点本身不为nullptr
        while ( p != nullptr && j < i - 1 ) {
                p = p->next;
                ++j;
        }

        // 如果遍历到头了，或者i的值不合规(比如i<=0)，说明没找到合乎目标的结点
        if ( p == nullptr || j > i - 1 ) {
                return ERROR;
        }

        // 生成新结点
        LinkList s = malloc( sizeof( LinkNode ) );
        if ( s == nullptr ) {
                exit( EXIT_FAILURE );
        }
        s->data = e;
        s->next = p->next;
        p->next = s;

        return OK;
}

bool ListInsert_1( LinkList L, ElemType e ) {
        if ( L == nullptr ) {
                return false;
        }

        LinkList s = malloc( sizeof( LinkNode ) );
        if ( s == nullptr ) {
                exit( EXIT_FAILURE );
        }
        s->data = e;
        s->next = nullptr;

        LinkNode **CurNode = &L->next;
        while ( *CurNode != nullptr ) {
                if ( ( *CurNode )->data > e ) {
                        break;
                }

                CurNode = &( *CurNode )->next;
        }
        s->next = *CurNode;
        *CurNode = s;

        return true;
}
/*
 * ████████ 算法2.10 ████████
 *
 * 删除
 *
 * 删除链表第i个位置上的元素，并将被删除元素存储到e中。
 * 删除成功则返回OK，否则返回ERROR。
 *
 *【备注】
 * 教材中i的含义是元素位置，从1开始计数
 */
Status LinkList_ListDelete( LinkList L, int i, ElemType *e ) {
        if ( L == nullptr ) {
                return ERROR;
        }

        LinkList q;
        LinkList p = L;
        int j = 0;

        // 寻找第i-1个结点，且保证该结点的后继不为nullptr
        while ( p->next != nullptr && j < i - 1 ) {
                p = p->next;
                ++j;
        }

        // 如果遍历到头了，或者i的值不合规(比如i<=0)，说明没找到合乎目标的结点
        if ( p->next == nullptr || j > i - 1 ) {
                return ERROR;
        }

        // 删除第i个结点
        q = p->next;
        p->next = q->next;
        *e = q->data;
        free( q );

        return OK;
}

/*
 * 删除所有元素e
 *
 * 双指针法：用不重复元素替换重复元素
 */
Status LinkList_ListDeleteElem( LinkList L, ElemType e ) {
        if ( L == nullptr ) {
                return ERROR;
        }

        LinkList p = L;       // pre
        LinkList q = L->next; // cur

        // 头结点是要删除的元素e
        if ( p->data == e ) {
                L = p->next;
                free( p );
                p = nullptr;
        }

        // 遍历删除e
        while ( q != nullptr ) {
                if ( q->data == e ) {
                        p->next = q->next;
                        free( q );
                        q = p->next;
                } else {
                        p = q;
                        q = q->next;
                }
        }

        return OK;
}

/*
 * 删除所有元素e
 *
 * 二级指针法
 */
Status LinkList_ListDeleteElem_1( LinkList L, ElemType e ) {
        if ( L == nullptr || L->next == nullptr ) {
                return ERROR;
        }

        LinkNode **curr = &L->next;

        while ( *curr != nullptr ) {
                LinkNode *entry = *curr;
                if ( entry->data == e ) {
                        *curr = entry->next;
                        free( entry );
                } else {
                        curr = &entry->next;
                }
        }
        return OK;
}

/*
 * 遍历
 *
 * 用visit函数访问链表L
 */
void ListTraverse( const LinkList L, void ( *Visit )( ElemType ) ) {
        if ( L == nullptr ) {
                return;
        }

        LinkList p = L->next;

        while ( p != nullptr ) {
                Visit( p->data );
                p = p->next;
        }

        printf( "\n" );
}

void LinkList_TraverseByIter( const LinkList L, void ( *Visit )( ElemType ) ) {
        if ( L == nullptr ) {
                return;
        }
        LinkNode *node = nullptr;
        LinkListIter *iter = LinkList_GetIterator( L );

        while ( ( node = LinkList_ListNext( iter ) ) != nullptr ) {
                Visit( node->data );
        }
        LinkList_ReleaseIterator( iter );

        printf( "\n" );
}

/*
 * ████████ 算法2.11 ████████
 *
 * 头插法创建链表
 *
 *
 *【备注】
 *
 * 教材中默认从控制台读取数据。
 * 这里为了方便测试，避免每次运行都手动输入数据，
 * 因而允许选择从预设的文件path中读取测试数据。
 *
 * 如果需要从控制台读取数据，则path为nullptr或者为空串，
 * 如果需要从文件中读取数据，则需要在path中填写文件名信息。
 */
Status LinkList_CreateListHead( LinkList *L, int n, char *path ) {
        LinkList p = nullptr;

        // 如果没有文件路径信息，则从控制台读取输入
        bool readFromConsole = path == nullptr || strcmp( path, "" ) == 0;

        if ( readFromConsole ) {
                // 建立头结点
                *L = malloc( sizeof( LinkNode ) );
                if ( *L == nullptr ) {
                        return ERROR;
                }
                ( *L )->next = nullptr;

                printf( "请输入%d个降序元素：", n );

                for ( int i = 1; i <= n; ++i ) {
                        // 生成新结点
                        p = malloc( sizeof( LinkNode ) );
                        if ( p == nullptr ) {
                                return ERROR;
                        }
                        // 填充数据，并插入到链表中
                        scanf( "%d", &( p->data ) );

                        p->next = ( *L )->next;
                        ( *L )->next = p;
                }
        } else {
                // 打开文件，准备读取测试数据
                FILE *fp = fopen( path, "r" );
                if ( fp == nullptr ) {
                        return ERROR;
                }

                // 建立头结点
                *L = malloc( sizeof( LinkNode ) );
                if ( *L == nullptr ) {
                        return ERROR;
                }
                ( *L )->next = nullptr;

                for ( int i = 1; i <= n; ++i ) {
                        // 生成新结点
                        p = malloc( sizeof( LinkNode ) );
                        if ( p == nullptr ) {
                                return ERROR;
                        }
                        // 填充数据，并插入到链表中
                        ReadData( fp, "%d", &( p->data ) );

                        p->next = ( *L )->next;
                        ( *L )->next = p;
                }
                fclose( fp );
        }

        return OK;
}

/*
 * 尾插法创建链表
 *
 *
 *【备注】
 *
 * 教材中默认从控制台读取数据。
 * 这里为了方便测试，避免每次运行都手动输入数据，
 * 因而允许选择从预设的文件path中读取测试数据。
 *
 * 如果需要从控制台读取数据，则path为nullptr或者为空串，
 * 如果需要从文件中读取数据，则需要在path中填写文件名信息。
 */
Status LinkList_CreateListTail( LinkList *L, int n, char *path ) {
        int i;
        LinkList p, q;
        FILE *fp;
        int readFromConsole; // 是否从控制台读取数据

        // 如果没有文件路径信息，则从控制台读取输入
        readFromConsole = path == nullptr || strcmp( path, "" ) == 0;

        if ( readFromConsole ) {
                // 建立头结点
                *L = malloc( sizeof( LinkNode ) );
                memset( ( *L ), 0, sizeof( LinkNode ) );
                ( *L )->next = nullptr;

                printf( "请输入%d个升序元素：", n );

                for ( i = 1, q = *L; i <= n; ++i ) {
                        // 生成新结点
                        p = malloc( sizeof( LinkNode ) );

                        // 填充数据，并插入到链表中
                        scanf( "%d", &( p->data ) );

                        q->next = p;
                        q = q->next;
                }

                q->next = nullptr;
        } else {
                // 打开文件，准备读取测试数据
                fp = fopen( path, "r" );
                if ( fp == nullptr ) {
                        return ERROR;
                }

                // 建立头结点
                *L = malloc( sizeof( LinkNode ) );
                ( *L )->next = nullptr;

                for ( i = 1, q = *L; i <= n; ++i ) {
                        // 生成新结点
                        p = malloc( sizeof( LinkNode ) );

                        // 填充数据，并插入到链表中
                        ReadData( fp, "%d", &( p->data ) );

                        q->next = p;
                        q = q->next;
                }

                q->next = nullptr;

                fclose( fp );
        }

        return OK;
}

/*
 *  链表逆置
 */
static Status ___ListReverseH( LinkList L ) {
        if ( L == nullptr ) {
                return ERROR;
        }
        LinkList q;

        LinkList p = L->next;
        L->next = nullptr;

        // 头插法
        while ( p != nullptr ) {
                q = p->next;
                p->next = L->next;
                L->next = p;
                p = q;
        }

        return OK;
}

static Status __ListReverseP( LinkList L ) {
        if ( L == nullptr ) {
                return ERROR;
        }
        LinkList r;

        LinkList p = L->next;
        LinkList q = p->next;
        p->next = nullptr;

        // 三指针法
        while ( q != nullptr ) {
                r = q->next;
                q->next = p;
                p = q;
                q = r;
        }

        L->next = p;

        return OK;
}

Status LinkList_ListReverse( LinkList L ) {
        if ( L == nullptr ) {
                return ERROR;
        }
        // return __ListReverseH(L);
        return __ListReverseP( L );
}

static LinkNode *LMerge( LinkNode *Left, LinkNode *Right ) {
        LinkList temp = malloc( sizeof( LinkNode ) );
        LinkNode *cur = temp;
        while ( Left != nullptr && Right != nullptr ) {
                if ( Left->data <= Right->data ) {
                        cur->next = Left;
                        Left = Left->next;
                        cur = cur->next;
                } else {
                        cur->next = Right;
                        Right = Right->next;
                        cur = cur->next;
                }
        }
        cur->next = Left ? Left : Right;

        cur = temp->next;
        free( temp );

        return cur;
}

static void LMPartition( LinkNode *N, LinkNode **Left, LinkNode **Right ) {
        LinkNode *slow = N, *fast = N->next;
        while ( fast != nullptr ) {
                fast = fast->next;
                if ( fast != nullptr ) {
                        fast = fast->next;
                        slow = slow->next;
                }
        }
        *Left = N;
        *Right = slow->next;
        slow->next = nullptr;

        /* LinkNode *M = ListMiddleElem(N); */
        /* if (M == nullptr) { */
        /*     return; */
        /* } */
        /* *Left = N; */
        /* *Right = M->next; */
        /* M->next = nullptr; */
}

static LinkNode *LMSort( LinkNode *N ) {
        if ( N == nullptr || N->next == nullptr ) {
                return N;
        }

        LinkNode *Left, *Right;
        LMPartition( N, &Left, &Right );
        Left = LMSort( Left );
        Right = LMSort( Right );

        return LMerge( Left, Right );
}

/*
 *  归并排序 ListMergeSort
 *
 */
Status LinkList_ListMergeSort( LinkList L ) {
        if ( L == nullptr ) {
                return ERROR;
        }

        LinkNode *N = LMSort( L->next );
        L->next = N;

        return OK;
}

static LinkNode *LQPartition( LinkList L, LinkNode *begin, LinkNode *end ) {
        ElemType pivotdata = L->data, temp;
        LinkNode *left = L, *right = begin;

        while ( right != end ) {
                if ( right->data < pivotdata ) {
                        left = left->next;

                        temp = left->data;
                        left->data = right->data;
                        right->data = temp;
                }
                right = right->next;
        }

        temp = L->data;
        L->data = left->data;
        left->data = temp;

        return left;
}

static void LQSort( LinkList L, LinkNode *begin, LinkNode *end ) {

        if ( begin != end ) {
                LinkNode *pivot = LQPartition( L, begin, end );
                LQSort( L, begin, pivot );
                LQSort( L, pivot->next, end );
        }
}

/*
 *  快排 ListQuickSort
 *
 */
Status LinkList_ListQuickSort( LinkList L ) {
        if ( L == nullptr ) {
                return ERROR;
        }

        LQSort( L, L->next, nullptr );

        return OK;
}

static LinkNode *LinkList_MinElem( LinkList L ) {
        if ( L == nullptr || L->next == nullptr ) {
                return nullptr;
        }

        LinkNode **curr = &L->next;
        LinkNode **forw = &L->next;

        while ( *forw != nullptr ) {
                LinkNode *entry_curr = *curr;
                LinkNode *entry_forw = *forw;
                if ( entry_forw->data < entry_curr->data ) {
                        curr = forw;
                } else {
                        forw = &entry_forw->next;
                }
        }
        LinkNode *min_node = *curr;

        ( *curr ) = ( *curr )->next;

        return min_node;
}

Status LinkList_SelectSort( LinkList L ) {
        if ( L == nullptr || L->next == nullptr ) {
                return ERROR;
        }
        LinkNode *p = L;
        LinkNode *min_p = nullptr;
        while ( ( min_p = LinkList_MinElem( p ) ) != nullptr ) {
                min_p->next = p->next;
                p->next = min_p;
                p = min_p;
        }
        return OK;
}

/*
 * 单链表去除相邻重复
 *
 */
Status LinkList_DeleteAdjacentDuplicates( LinkList L ) {
        if ( L == nullptr || L->next == nullptr ) {
                return ERROR;
        }
        LinkNode **curr = &L->next;

        while ( *curr != nullptr && ( *curr )->next != nullptr ) {
                LinkNode *entry = *curr;
                if ( entry->data == entry->next->data ) {
                        *curr = entry->next;
                        free( entry );
                } else {
                        curr = &entry->next;
                }
        }

        return OK;
}
