#include "LinkList.h" //**▲02 线性表**//
#include "Status.h"
#include <stdio.h>

bool CmpGreater( ElemType data, ElemType e ) {
        return data > e ? true : false;
}

void PrintElem( ElemType e ) {
        printf( "%d ", e );
}

int main( int argc, char **argv ) {
        LinkList L;
        int i;
        ElemType e;

        printf( "████████ InitList \n" );
        {
                printf( "█ 初始化单链表 L ...\n" );
                // InitList(&L);
                L = CreateList( );
        }
        PressEnterToContinue( debug );

        printf( "████████ ListEmpty \n" );
        {
                LinkList_ListEmpty( L ) ? printf( "█ L 为空！！\n" )
                                        : printf( "█ L 不为空！\n" );
        }
        PressEnterToContinue( debug );

        printf( "████████ ListInsert \n" );
        {
                for ( i = 1; i <= 8; i++ ) {
                        printf( "█ 在 L 第 %d 个位置插入 \"%d\" ...\n", i,
                                2 * i );
                        LinkList_ListInsert( L, i, 2 * i );
                }
        }
        PressEnterToContinue( debug );

        printf( "████████ ListInsert_1 \n" );
        {
                printf( "█ 在 L 插入 11 \n" );
                ListInsert_1( L, 11 );
        }
        PressEnterToContinue( debug );

        printf( "████████ ListTraverse \n" );
        {
                printf( "█ L 中的元素为：L = " );
                ListTraverse( L, PrintElem );
        }
        PressEnterToContinue( debug );

        printf( "████████ ListTraverseByIter \n" );
        {
                printf( "█ L 中的元素为：L = " );
                LinkList_TraverseByIter( L, PrintElem );
        }
        PressEnterToContinue( debug );

        printf( "████████ ListLength \n" );
        { printf( "█ L 的长度为 %d \n", LinkList_ListLength( L ) ); }
        PressEnterToContinue( debug );

        printf( "████████ ListDelete \n" );
        {
                printf( "█ 删除前的元素：L = " );
                ListTraverse( L, PrintElem );

                printf( "█ 尝试删除 L 中第 6 个元素...\n" );

                if ( LinkList_ListDelete( L, 6, &e ) == OK ) {
                        printf( "█ 删除成功，被删除元素是：\"%d\"\n", e );
                } else {
                        printf( "█ 删除失败，第 6 个元素不存在！\n" );
                }

                printf( "█ 删除后的元素：L = " );
                ListTraverse( L, PrintElem );
        }
        PressEnterToContinue( debug );

        printf( "████████ ListDeleteElem_1 \n" );
        {
                printf( "█ 删除前的元素：L = " );
                ListTraverse( L, PrintElem );

                printf( "█ 尝试删除 L 中元素 10...\n" );

                if ( LinkList_ListDeleteElem_1( L, 10 ) == OK ) {
                        printf( "█ 删除成功，被删除元素是：\"%d\"\n", 10 );
                } else {
                        printf( "█ 删除失败，元素10不存在！\n" );
                }

                printf( "█ 删除后的元素：L = " );
                ListTraverse( L, PrintElem );
        }
        PressEnterToContinue( debug );

        printf( "████████ GetElem \n" );
        {
                LinkList_GetElem( L, 4, &e );
                printf( "█ L 中第 4 个位置的元素为 \"%d\" \n", e );
                printf( "█ L 中第 4 个位置的元素为 \"%d\" \n",
                        GetElem_1( L, 4 ) );
        }
        PressEnterToContinue( debug );

        printf( "████████ LocateElem \n" );
        {
                i = LinkList_LocateElem( L, 7, CmpGreater );
                LinkList_GetElem( L, i, &e );
                printf( "█ L 中第一个元素值大于 \"7\" 的元素是 \"%d\" \n", e );
                printf( "█ L 中第一个元素值大于 \"7\" 的元素是 \"%d\" \n",
                        GetElem_1( L, i ) );
        }
        PressEnterToContinue( debug );

        printf( "████████ PriorElem \n" );
        {
                ElemType cur_e = 6;

                if ( LinkList_PriorElem( L, cur_e, &e ) == OK ) {
                        printf( "█ 元素 \"%d\" 的前驱为 \"%d\" \n", cur_e, e );
                } else {
                        printf( "█ 元素 \"%d\" 的前驱不存在！\n", cur_e );
                }
        }
        PressEnterToContinue( debug );

        printf( "████████ NextElem \n" );
        {
                ElemType cur_e = 6;

                if ( LinkList_NextElem( L, cur_e, &e ) == OK ) {
                        printf( "█ 元素 \"%d\" 的后继为 \"%d\" \n", cur_e, e );
                } else {
                        printf( "█ 元素 \"%d\" 的后继不存在！\n", cur_e );
                }
        }
        PressEnterToContinue( debug );

        printf( "████████ ClearList \n" );
        {
                printf( "█ 清空 L 前：" );
                LinkList_ListEmpty( L ) ? printf( " L 为空！！\n" )
                                        : printf( " L 不为空！\n" );

                LinkList_ClearList( L );

                printf( "█ 清空 L 后：" );
                LinkList_ListEmpty( L ) ? printf( " L 为空！！\n" )
                                        : printf( " L 不为空！\n" );
        }
        PressEnterToContinue( debug );

        printf( "████████ DestroyList \n" );
        {
                printf( "█ 销毁 L 前：" );
                L ? printf( " L 存在！\n" ) : printf( " L 不存在！！\n" );

                LinkList_DestroyList( &L );

                printf( "█ 销毁 L 后：" );
                L ? printf( " L 存在！\n" ) : printf( " L 不存在！！\n" );
        }
        PressEnterToContinue( debug );

        printf( "████████ CreateList_Head \n" );
        {
                LinkList L;
                LinkList_CreateListHead( &L, 5, "./TestData_Head.txt" );
                printf( "█ 头插法建立单链表 L = " );
                ListTraverse( L, PrintElem );
                LinkList_DestroyList( &L );
        }
        PressEnterToContinue( debug );

        printf( "████████ CreateList_Tail \n" );
        {
                LinkList L;
                LinkList_CreateListTail( &L, 5, "./TestData_Tail.txt" );
                printf( "█ 尾插法建立单链表 L = " );
                ListTraverse( L, PrintElem );
                LinkList_DestroyList( &L );
        }
        PressEnterToContinue( debug );

        printf( "████████ Reverse_List \n" );
        {
                LinkList L;
                LinkList_CreateListTail( &L, 5, "./TestData_Tail.txt" );
                printf( "█ 尾插法建立单链表 L = " );
                ListTraverse( L, PrintElem );
                printf( "█ 单链表逆置 L = " );
                LinkList_ListReverse( L );
                ListTraverse( L, PrintElem );
                LinkList_DestroyList( &L );
        }
        PressEnterToContinue( debug );

        printf( "████████ ListMiddleElem \n" );
        {
                LinkList L1;
                LinkList_CreateListTail( &L1, 5, "./TestData_Tail.txt" );
                printf( "█ 尾插法建立单链表 L1 = " );
                ListTraverse( L1, PrintElem );
                LinkList L1_M = LinkList_ListMiddleElem( L1->next );
                printf( "█ 单链表中间结点 L1_M = %d\n", L1_M->data );
                LinkList_DestroyList( &L );
        }
        PressEnterToContinue( debug );

        printf( "████████ ListMergeSort \n" );
        {
                LinkList L1;
                LinkList_CreateListTail( &L1, 12, "./TestData_Sort.txt" );
                printf( "█ 尾插法建立单链表 L1 = " );
                ListTraverse( L1, PrintElem );
                LinkList_ListMergeSort( L1 );
                printf( "█ 单链表归并排序 L1 = " );
                ListTraverse( L1, PrintElem );

                int k = 4;
                printf( "█ 单链表倒数第%d个元素: ", k );
                LinkNode *KElem = LinkList_ListLastKElem( L1, k );
                PrintElem( KElem->data );

                printf( "\n█ 单链表去除相邻重复 L1 = " );
                LinkList_DeleteAdjacentDuplicates( L1 );
                ListTraverse( L1, PrintElem );

                LinkList_DestroyList( &L1 );
        }
        PressEnterToContinue( debug );

        /* printf("████████ ListQuickSort \n"); */
        /* { */
        /*     LinkList L2; */
        /*     CreateList_Tail(&L2, 6, "./TestData_Sort.txt"); */
        /*     printf("█ 尾插法建立单链表 L2 = "); */
        /*     ListTraverse(L2, PrintElem); */
        /*     ListQuickSort(L2); */
        /*     printf("█ 单链表快速排序 L2 = "); */
        /*     ListTraverse(L2, PrintElem); */
        /*     DestroyList(&L2); */
        /* } */
        /* PressEnterToContinue(debug); */

        printf( "████████ LinkList_SelectSort \n" );
        {
                LinkList L2;
                LinkList_CreateListTail( &L2, 12, "./TestData_Sort.txt" );
                printf( "█ 尾插法建立单链表 L2 = " );
                ListTraverse( L2, PrintElem );
                LinkList_SelectSort( L2 );
                printf( "█ 单链表选择排序 L2 = " );
                ListTraverse( L2, PrintElem );

                LinkList_DestroyList( &L2 );
        }
        PressEnterToContinue( debug );
        return 0;
}
