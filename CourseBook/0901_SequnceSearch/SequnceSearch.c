/***************************
 *                         *
 * 文件夹: ▲09 查找       *
 *                         *
 * 文件名: SequnceSearch.c *
 *                         *
 * 算  法: 9.1             *
 *                         *
 ***************************/

#include "SequnceSearch.h" //**▲09 查找**//

/*════╗
║ 算法9.1║
╚════*/
int Search_Seq( Table T, KeyType key ) {
        int i;

        T.elem[ 0 ].key = key;

        for ( i = T.length; !EQ( T.elem[ i ].key, key ); --i ) {
        };

        return i;
}
