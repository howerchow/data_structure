/*=============================
 * 线性表的顺序存储结构（顺序表）
 *
 * 包含算法: 2.3、2.4、2.5、2.6
 =============================*/

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include "Status.h" //**▲01 绪论**//
#include <stddef.h>

/* 宏定义 */
#define LIST_INIT_SIZE 100 // 顺序表存储空间的初始分配量
#define LISTINCREMENT 10   // 顺序表存储空间的分配增量

/* 顺序表元素类型定义 */
typedef int ElemType;

/*
 * 顺序表结构
 *
 * 注：elem在使用前需要先为其分配内存，且元素从elem[0]处开始存储
 */
typedef struct {
        ElemType *elem;  // 顺序表存储空间的基址（指向顺序表所占内存的起始位置）
        size_t length;   // 当前顺序表长度（包含多少元素）
        size_t capacity; // 当前分配的存储容量（可以存储多少元素）
} SqList;

/*
 * ████████ 算法2.3 ████████
 *
 * 初始化
 *
 * 初始化成功则返回OK，否则返回ERROR。
 */
Status SqList_InitList( SqList *L );

/*
 * 销毁(结构)
 *
 * 释放顺序表所占内存。
 */
  Status SqList_DestroyList( SqList *L );

/*
 * 置空(内容)
 *
 * 只是清理顺序表中存储的数据，不释放顺序表所占内存。
 */
Status SqList_ClearList( SqList *L );

/*
 * ████████ 算法2.4 ████████
 *
 * 插入
 *
 * 向顺序表第i个位置上插入e，插入成功则返回OK，否则返回ERROR。
 *
 *【备注】
 * 教材中i的含义是元素位置，从0开始计数
 */
Status SqList_ListInsert( SqList *L, size_t i, ElemType e );

/*
 * 取值
 *
 * 获取顺序表中第i个元素，将其存储到e中。
 * 如果可以找到，返回OK，否则，返回ERROR。
 *
 *【备注】
 * 通常，i的含义应该指索引，即从0开始计数。
 */
ElemType SqList_GetElem( SqList L, size_t i );
