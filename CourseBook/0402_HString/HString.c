/*=========================
 * 串的堆分配存储表示（堆串）
 *
 * 包含算法: 4.4
 ==========================*/

#include "HString.h" //**▲04 串**//
#include "SqList.h"
#include "Status.h"
#include <stddef.h>
#include <stdlib.h> // 提供malloc、realloc、free、exit原型
#include <string.h> // 提供strlen原型

static Status HStr_SetLength( HString *T, size_t len ) {
        T->length = len;
        return OK;
}

Status HStr_Init( HString *T ) {
        T->ch = nullptr;
        T->length = 0;
        return OK;
}

/*
 * 初始化
 *
 * 构造一个值为chars的串T。
 *
 *【注】
 * 该操作属于最小操作子集
 */
Status HStr_Assign( HString *T, const char *chars ) {
        if ( chars == nullptr ) {
                return ERROR;
        }
        size_t len = strlen( chars );

        // 没有有效元素
        if ( len == 0 ) {
                T->ch = nullptr;
                T->length = 0;
                return OK;
        }

        // 存在有效元素时，需要分配存储空间
        char *new_ch = malloc( len + 1 );
        if ( new_ch == nullptr ) {
                exit( EXIT_FAILURE );
        }

        // strncpy( T->ch, chars, len );
        memcpy( new_ch, chars, len );
        new_ch[ len ] = '\0';

        free( T->ch );
        T->ch = new_ch;
        T->length = len;

        return OK;
}

/*
 * 销毁
 *
 * 将串S销毁。
 *
 *【注】
 * 堆串的结构可以销毁，但不是在销毁操作中
 */
Status DestroyHString( HString *S ) {
        return OK;
}

/*
 * 清空
 *
 * 将串S清空。
 */
Status HStr_Clear( HString *S ) {
        // 没有有效元素时，销毁堆串结构
        if ( S->ch != nullptr ) {
                free( S->ch );
                S->ch = nullptr;
        }

        S->length = 0;

        return OK;
}

/*
 * 判空
 *
 * 判断串S中是否包含有效数据。
 *
 * 返回值：
 * true : 串S为空
 * false: 串S不为空
 */
bool HStr_IsEmpty( HString S ) {
        return S.length == 0;
}

/*
 * 计数
 *
 * 返回串S中元素的个数。
 *
 *【注】
 * 该操作属于最小操作子集
 */
size_t HStr_GetLength( HString S ) {
        return S.length;
}

/*
 * 求子串
 *
 * 用Sub返回S[pos, pos+len-1]。
 * 返回值指示是否截取成功。
 *
 *【注】
 * 该操作属于最小操作子集
 */
Status HStr_SubHStr( HString *Sub, HString S, size_t pos, size_t len ) {
        if ( pos > S.length || pos + len > S.length ) {
                return ERROR;
        }

        // 如果是截取0个字符，不需要分配空间
        if ( len == 0 ) {
                Sub->ch = nullptr;
                Sub->length = 0;
                return OK;
        }

        if ( Sub->ch != nullptr && Sub->length >= len ) {
                // 复制元素
                memcpy( Sub->ch, S.ch + pos, len );
                Sub->ch[ len ] = '\0';

                // 确定新长度
                Sub->length = len;

                return OK;
        } else {
                char *temp = realloc( Sub->ch, len + 1 );
                if ( temp == nullptr ) {
                        return ERROR;
                }
                // 复制元素
                Sub->ch = temp;
                memcpy( Sub->ch, S.ch + pos, len );
                Sub->ch[ len ] = '\0';

                // 确定新长度
                Sub->length = len;

                return OK;
        }
}

/* 模式串 next数组(部分匹配表)
 *   不含当前字符,前面字符串,前后缀最大匹配长度(不能是整体)
 *   a   a   b   a   a   a   b
 *   -1  0   1   0   1   2   0
 */
static Status KMP_GetNext( const HString T, SqList *next ) {
        if ( T.length <= 0 || next == nullptr || next->elem == nullptr ) {
                return ERROR;
        }
        SqList_ClearList( next ); // 清理数据

        /* 固定值 */
        if ( T.length == 1 ) {
                SqList_ListInsert( next, 0, -1 );
                return OK;
        }
        if ( T.length == 2 ) {
                SqList_ListInsert( next, 0, -1 );
                SqList_ListInsert( next, 1, 0 );
                return OK;
        }

        SqList_ListInsert( next, 0, -1 );
        SqList_ListInsert( next, 1, 0 );

        size_t i = 2; // 当前要求next值的位置
        int cn = 0;   // 当前要和前一个字符比对的下标

        // 模式串第一个字符处失配时，模式串需要从头比较，主串需要前进到下一个位置比较
        // next->elem[ 1 ] = 0;

        // 遍历模式串上的字符
        while ( i < T.length ) {
                if ( T.ch[ i - 1 ] == T.ch[ cn ] ) {
                        ++cn;
                        SqList_ListInsert( next, i, cn );
                        ++i;
                } else if ( cn > 0 ) {
                        cn = SqList_GetElem( *next, cn );
                } else {
                        SqList_ListInsert( next, i, 0 );
                        ++i;
                }
        }
        return OK;
}

/*
 * 查找
 *
 * 从pos处开始搜索模式串T在主串S中首次出现的位置，如果不存在，则返回0。
 * 如果查找成功，返回匹配的位置。
 *
 * S: 文本串
 * T: 模式串
 * pos: 从文本串pos出开始搜索
 *
 */
size_t HStr_IndexHStrKMP( HString S, HString T, size_t pos ) {
        size_t T_len = HStr_GetLength( T );
        size_t S_len = HStr_GetLength( S );

        if ( HStr_IsEmpty( T ) || HStr_IsEmpty( T ) || pos + T_len > S_len ) {
                return SIZE_MAX;
        }

        /* next数组(部分匹配表) */
        SqList next;
        SqList_InitList( &next );

        KMP_GetNext( T, &next );

        size_t i = pos; // 文本串下标
        size_t j = 0;   // 模式串下标

        while ( i < S.length && j < T.length ) {
                if ( S.ch[ i ] == T.ch[ j ] ) {
                        ++i;
                        ++j;
                } else if ( j == 0 ) {
                        ++i;
                } else {
                        j = SqList_GetElem( next, j );
                }
        }

        SqList_DestroyList( &next ); // 销毁next数组内存

        if ( j == T.length ) {
                return i - j;
        }

        return SIZE_MAX; // 查找失败
}

/*
 * 查找
 *
 * 从pos处开始搜索模式串T在主串S中首次出现的位置，如果不存在，则返回0。
 * 如果查找成功，返回匹配的位置。
 *
 *【注】
 * 1.此实现需要依赖串的最小操作子集
 * 2.该实现比较低效
 */
size_t HStr_IndexHStr( HString S, HString T, size_t pos ) {

        size_t T_len = HStr_GetLength( T );
        size_t S_len = HStr_GetLength( S );

        if ( HStr_IsEmpty( T ) || HStr_IsEmpty( T ) || pos + T_len > S_len ) {
                return SIZE_MAX;
        }

        size_t i = pos;

        HString sub;
        HStr_Init( &sub );

        // 保证长度不越界
        while ( i + T_len < S_len ) {
                // 获取S[i, i+t]
                if ( HStr_SubHStr( &sub, S, i, T_len ) == ERROR ) {
                        HStr_Clear( &sub );
                        return SIZE_MAX;
                }

                // 如果子串与模式串不匹配，则需要继续推进
                if ( HStr_Compare( sub, T ) != 0 ) {
                        ++i;
                } else {
                        HStr_Clear( &sub );
                        return i;
                }
        }

        HStr_Clear( &sub );
        return SIZE_MAX; // 查找失败
}

/*
 * ████████ 算法4.4 ████████
 *
 * 插入
 *
 * 将串T插入到主串S的pos位置处。
 */
Status HStr_Insert( HString *S, size_t pos, HString T ) {
        if ( pos > S->length - 1 ) {
                return ERROR;
        }

        // 如果待插入的串为空，则提前返回
        if ( HStr_IsEmpty( T ) ) {
                return OK;
        }

        // 分配新空间，会将旧元素一起复制过去
        char *p =
            realloc( S->ch, ( S->length + T.length + 1 ) * sizeof( char ) );
        if ( p == nullptr ) {
                return ERROR;
        }

        S->ch = p;
        S->ch[ S->length + T.length ] = '\0';

        // 在S中腾出位置，为插入T做准备
        memmove( S->ch + pos + T.length, S->ch + pos,
                 ( S->length - pos + 1 ) * sizeof( char ) ); // 包含了 \0 的移动
        //
        // 将串T插入在S中腾出的位置上
        memcpy( S->ch + pos, T.ch, T.length );

        HStr_SetLength( S, S->length + T.length );

        return OK;
}

/*
 * 删除
 *
 * 删除S[pos, pos+len]。
 */
Status HStr_Delete( HString *S, size_t pos, size_t len ) {
        if ( pos + len > S->length ) {
                return ERROR;
        }

        // 如果待删除的长度为0，则提前返回
        if ( len == 0 ) {
                return OK;
        }

        // 把后面的元素挪到前面，覆盖掉被删除的元素
        memmove( S->ch + pos, S->ch + pos + len, len );

        // 长度减少
        S->length -= len;

        // 缩减分配的空间（如果长度减少为0，这里会返回空指针）
        S->ch = realloc( S->ch, S->length * sizeof( char ) );

        return OK;
}

/*
 * 比较
 *
 * 比较串S和串T，返回比较结果。
 *
 *【注】
 * 该操作属于最小操作子集
 */
Status HStr_Compare( HString S, HString T ) {
        size_t S_len = HStr_GetLength( S );
        size_t T_len = HStr_GetLength( T );

        if ( S_len != T_len ) {
                return S_len - T_len;
        }

        return memcmp( S.ch, T.ch, S_len * sizeof( char ) );
}

/*
 * 复制
 *
 * 将串S复制到串T。
 */
Status HStr_Copy( HString *T, HString S ) {
        if ( HStr_IsEmpty( S ) ) {
                T->ch = nullptr;
                T->length = 0;
                return OK;
        }

        size_t S_len = HStr_GetLength( S );
        if ( T->ch != nullptr && T->length >= S_len ) {
                memcpy( T->ch, S.ch, S_len );
                T->ch[ S_len ] = '\0';
                HStr_SetLength( T, S_len );
                return OK;
        } else {
                HStr_Clear( T );
                HStr_Assign( T, S.ch );

                return OK;
        }
}

/*
 * 替换
 *
 * 用V替换主串S中出现的所有与T相等的且不重叠的子串。
 *
 *【注】
 * 1.该操作依赖最小操作子集
 * 2.该实现比较低效
 */
Status HStr_Replace( HString *S, HString T, HString V ) {

        if ( HStr_IsEmpty( *S ) || HStr_IsEmpty( T ) ) {
                return ERROR;
        }
        size_t T_len = HStr_GetLength( T );
        size_t V_len = HStr_GetLength( V );

        // 在主串S中寻找模式串T第一次出现的位置
        size_t i = HStr_IndexHStr( *S, T, 1 );

        // 如果存在匹配的字符串
        while ( i != SIZE_MAX ) {
                if ( T_len == V_len ) {
                        memcpy( S->ch + i, V.ch, V.length );
                } else {
                        HStr_Delete( S, i, T_len ); // 从S中删除T
                        HStr_Insert( S, i, V );     // 向S中插入V
                }
                i += V_len; // i切换到下一个位置

                i = HStr_IndexHStr( *S, T, i ); // 查找下一个匹配的字符串
        }

        return OK;
}

Status HStr_ReplaceKMP( HString *S, HString T, HString V ) {

        if ( HStr_IsEmpty( *S ) || HStr_IsEmpty( T ) ) {
                return ERROR;
        }
        size_t T_len = HStr_GetLength( T );
        size_t V_len = HStr_GetLength( V );

        // 在主串S中寻找模式串T第一次出现的位置
        // size_t i = HStr_IndexHStr( *S, T, 1 );
        size_t i = HStr_IndexHStrKMP( *S, T, 0 );

        // 如果存在匹配的字符串
        while ( i != SIZE_MAX ) {
                if ( T_len == V_len ) {
                        memcpy( S->ch + i, V.ch, V.length );
                } else {
                        HStr_Delete( S, i, T_len ); // 从S中删除T
                        HStr_Insert( S, i, V );     // 向S中插入V
                }
                i += V_len; // i切换到下一个位置

                // i = HStr_IndexHStr( *S, T, i ); // 查找下一个匹配的字符串
                i = HStr_IndexHStrKMP( *S, T, i );
        }

        return OK;
}
/*
 * 串联接
 *
 * 联接S1和S2，并存储到T中返回。如果联接后的长度溢出，则只保留未溢出的部分。
 * 返回值表示联接后的串是否完整。
 * 堆串的空间被认为是无限的，因此这里总是返回true，指示串不会被裁剪。
 *
 *【注】
 * 该操作属于最小操作子集
 */
Status HStr_Concat( HString *T, HString S1, HString S2 ) {

        // 确定新长度
        T->length = S1.length + S2.length;

        // 分配空间
        T->ch = malloc( ( T->length + 1 ) * sizeof( char ) );
        if ( T->ch == nullptr ) {
                return ERROR;
        }

        // 先把S1的内容拷贝出来
        memcpy( T->ch, S1.ch, S1.length );

        // 再拷贝S2的内容
        memcpy( T->ch + S1.length, S2.ch, S2.length );

        T->ch[ T->length ] = '\0';

        return true;
}
