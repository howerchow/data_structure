#include "SqList.h"
#include <stdlib.h>
#include <string.h>

/*
 * ████████ 算法2.3 ████████
 *
 * 初始化
 *
 * 初始化成功则返回OK，否则返回ERROR。
 */
Status SqList_InitList( SqList *L ) {
        // 分配指定容量的内存，如果分配失败，则返回nullptr
        L->elem = calloc( LIST_INIT_SIZE, sizeof( ElemType ) );
        if ( L->elem == nullptr ) {
                // 存储内存失败
                exit( EXIT_FAILURE );
        }

        L->length = 0;                // 初始化顺序表长度为0
        L->capacity = LIST_INIT_SIZE; // 顺序表初始内存分配量

        return OK; // 初始化成功
}

/*
 * 销毁(结构)
 *
 * 释放顺序表所占内存。
 */
Status SqList_DestroyList( SqList *L ) {
        // 确保顺序表结构存在
        if ( L == nullptr || L->elem == nullptr ) {
                return ERROR;
        }

        // 释放顺序表内存
        free( L->elem );

        // 释放内存后置空指针
        L->elem = nullptr;

        // 顺序表长度跟容量都归零
        L->length = 0;
        L->capacity = 0;

        return OK;
}


/*
 * 置空(内容)
 *
 * 只是清理顺序表中存储的数据，不释放顺序表所占内存。
 */
Status SqList_ClearList( SqList *L ) {

        if ( L == nullptr || L->elem == nullptr ) {
                return ERROR;
        }

        memset( L->elem, '\0', ( int )L->capacity );

        L->length = 0;

        return OK;
}


/*
 * ████████ 算法2.4 ████████
 *
 * 插入
 *
 * 向顺序表第i个位置上插入e，插入成功则返回OK，否则返回ERROR。
 *
 *【备注】
 * i从0开始计数
 */
Status SqList_ListInsert( SqList *L, size_t i, ElemType e ) {
        // 确保顺序表结构存在
        if ( L == nullptr || L->elem == nullptr ) {
                return ERROR;
        }

        // i值越界
        if ( i > L->length ) {
                return ERROR;
        }

        // 若存储空间已满，则增加新空间
        if ( L->length >= L->capacity ) {
                // 基于现有空间扩容
                ElemType *newbase = ( ElemType * )realloc(
                    L->elem,
                    ( L->capacity + LISTINCREMENT ) * sizeof( ElemType ) );
                if ( newbase == nullptr ) {
                        // 存储内存失败
                        exit( EXIT_FAILURE );
                }

                // 新基址
                L->elem = newbase;
                // 存的存储空间
                L->capacity += LISTINCREMENT;
        }

        // q为插入位置
        ElemType *q = &L->elem[ i ];

        /* // 1.右移元素，腾出位置 */
        for ( ElemType *p = &L->elem[ L->length - 1 ]; p >= q; --p ) {
                // *( p + 1 ) = *p;
                memmove( p + 1, p, sizeof( *p ) );
        }

        // 2.插入e
        *q = e;

        // 3.表长增1
        ++L->length;

        return OK;
}


/*
 * 取值
 *
 * 获取顺序表中第i个元素，将其存储到e中。
 * 如果可以找到，返回OK，否则，返回ERROR。
 *
 *【备注】
 * 通常，i的含义应该指索引，即从0开始计数。
 */
ElemType SqList_GetElem( SqList L, size_t i ) {
        // 因为i的含义是位置，所以其合法范围是：[0, length-1]
        if ( i > L.length - 1 ) {
                return ERROR; // i值不合法
        }

        return L.elem[ i ];
}
