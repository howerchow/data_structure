#include "HString.h" //**▲04 串**//
#include <stddef.h>
#include <stdio.h>

// 测试函数，打印字符串
void PrintElem( HString S ) {

        for ( size_t i = 0; i < S.length; ++i ) {
                printf( "%c", S.ch[ i ] );
        }

        printf( "\n" );
}

int main( int argc, char **argv ) {
        char *chars = "01234567899876543210";
        size_t i = 0;

        HString S, T, sub, V;

        HStr_Init( &S );
        HStr_Init( &T );
        HStr_Init( &sub );
        HStr_Init( &V );

        printf( "████████ HStr_Assign \n" );
        {
                printf( "█ 为堆串 S 赋值...\n" );
                HStr_Assign( &S, chars );
                printf( "█ S = " );
                PrintElem( S );
        }
        PressEnterToContinue( debug );

        printf( "████████ HStr_IsEmpty \n" );
        {
                HStr_IsEmpty( S ) ? printf( "█ S 为空！！\n" )
                                  : printf( "█ S 不为空！\n" );
        }
        PressEnterToContinue( debug );

        printf( "████████ HStr_GetLength \n" );
        {
                i = HStr_GetLength( S );
                printf( "█ S 的长度为 %zu \n", i );
        }
        PressEnterToContinue( debug );

        printf( "████████ HStr_Copy \n" );
        {
                printf( "█ 复制 S 到 T ...\n" );
                HStr_Copy( &T, S );
                printf( "█ T = " );
                PrintElem( T );
        }
        PressEnterToContinue( debug );

        printf( "████████ HStr_Compare \n" );
        {
                printf( "█ 比较字符串 S 和 T ...\n" );
                i = HStr_Compare( S, T );
                i == 0 ? printf( "█ S==T\n" )
                       : ( i < 0 ? printf( "█ S<T\n" ) : printf( "█ S>T\n" ) );
        }
        PressEnterToContinue( debug );

        printf( "████████ HStr_Insert \n" );
        {
                HStr_Assign( &T, "*****" );
                printf( "█ 将 \"*****\" 插入到串 S 的第 11 个位置处... \n" );
                HStr_Insert( &S, 11, T );
                printf( "█ S = " );
                PrintElem( S );
        }
        PressEnterToContinue( debug );

        printf( "████████ HStr_IndexHStr \n" );
        {
                HStr_Assign( &T, "*****" );
                printf( "█ 获取 \"*****\" 在串 S 中的第一次出现的位置...\n" );
                // i = HStr_IndexHStr( S, T, 0 );
                i = HStr_IndexHStrKMP( S, T, 0 );
                printf( "█ 串 \"*****\" 在 S 中第一次出现的位置为 %zu \n", i );
        }
        PressEnterToContinue( debug );

        printf( "████████ HStr_SubHStr \n" );
        {
                printf( "█ 用 sub 返回 S 中第 11 个字符起的 5 个字符...\n" );
                HStr_SubHStr( &sub, S, 11, 5 );
                printf( "█ Sub = " );
                PrintElem( sub );
        }
        PressEnterToContinue( debug );

        printf( "████████ HStr_Replace \n" );
        {
                HStr_Assign( &T, "*****" );
                HStr_Assign( &V, "#####@@@@@" );
                printf( "█ 用 \"#####@@@@@\" 替换S中的 \"*****\" ...\n" );
                HStr_Replace( &S, T, V );
                //HStr_ReplaceKMP( &S, T, V );
                printf( "█ S = " );
                PrintElem( S );
        }
        PressEnterToContinue( debug );

        printf( "████████ HStr_Replace \n" );
        {
                HString T1, V1;
                HStr_Init( &T1 );
                HStr_Init( &V1 );

                HStr_Assign( &T1, "#####@@@@@" );
                HStr_Assign( &V1, "##########" );
                printf( "█ 用 \"##########\" 替换S中的 \"#####@@@@@\" ...\n" );
                HStr_Replace( &S, T1, V1 );
                printf( "█ S = " );
                PrintElem( S );

                HStr_Clear( &T1 );
                HStr_Clear( &V1 );
        }
        PressEnterToContinue( debug );

        printf( "████████ HStr_Delete \n" );
        {
                printf( "█ 删除 S 中第 16 个字符起的 5 个字符...\n" );
                HStr_Delete( &S, 16, 5 );
                printf( "█ S = " );
                PrintElem( S );
        }
        PressEnterToContinue( debug );

        printf( "████████ HStr_Concat \n" );
        {
                HString Tmp, S1, S2;
                HStr_Init( &Tmp );
                HStr_Init( &S1 );
                HStr_Init( &S2 );

                HStr_Assign( &S1, "+++++" );
                HStr_Assign( &S2, "-----" );

                printf( "█ 联接 \"+++++\" 和 \"-----\" 形成 Tmp ...\n" );
                HStr_Concat( &Tmp, S1, S2 );
                printf( "█ Tmp = " );
                PrintElem( Tmp );

                HStr_Clear( &Tmp );
                HStr_Clear( &S1 );
                HStr_Clear( &S2 );
        }
        PressEnterToContinue( debug );

        HStr_Clear( &S );
        HStr_Clear( &T );
        HStr_Clear( &sub );
        HStr_Clear( &V );

        return 0;
}
