/*=========================
 * 串的堆分配存储表示（堆串）
 *
 * 包含算法: 4.4
 ==========================*/

#pragma once

#include "Status.h" //**▲01 绪论**//
#include <stddef.h>

/*
 * 串的堆存储表示
 *
 * 注：有效元素从ch的0号单元开始存储
 */
typedef struct HString {
        char *ch; // 若是非空串，则按串长分配存储区，否则ch为nullptr, 以'\0'结尾
        size_t length;
} HString;



Status HStr_Init( HString *T );
/*
 * ████ 提示 ████
 *
 * 遵循教材的书写习惯，pos指示字符的位序(不是索引)，从1开始计数
 */

/*
 * 初始化
 *
 * 构造一个值为chars的串T。
 *
 *【注】
 * 该操作属于最小操作子集
 */
Status HStr_Assign( HString *T, const char *chars );

/*
 * 销毁
 *
 * 将串S销毁。
 */
Status DestroyHString( HString *S );

/*
 * 清空
 *
 * 将串S清空。
 */
Status HStr_Clear( HString *S );

/*
 * 判空
 *
 * 判断串S中是否包含有效数据。
 *
 * 返回值：
 * true : 串S为空
 * false: 串S不为空
 */
bool HStr_IsEmpty( HString S );

/*
 * 计数
 *
 * 返回串S中元素的个数。
 *
 *【注】
 * 该操作属于最小操作子集
 */
size_t HStr_GetLength( HString S );

/*
 * 求子串
 *
 * 用Sub返回S[pos, pos+len-1]。
 * 返回值指示是否截取成功。
 *
 *【注】
 * 该操作属于最小操作子集
 */
Status HStr_SubHStr( HString *Sub, HString S, size_t pos, size_t len );

/*
 * 查找
 *
 * 从pos处开始搜索模式串T在主串S中首次出现的位置，如果不存在，则返回0。
 * 如果查找成功，返回匹配的位置。
 *
 *【注】
 * 1.此实现需要依赖串的最小操作子集
 * 2.该实现比较低效
 */
size_t HStr_IndexHStr( HString S, HString T, size_t pos );

size_t HStr_IndexHStrKMP( HString S, HString T, size_t pos );

/*
 * ████████ 算法4.4 ████████
 *
 * 插入
 *
 * 将串T插入到主串S的pos位置处。 从0开始
 */
Status HStr_Insert( HString *S, size_t pos, HString T );

/*
 * 删除
 *
 * 删除S[pos, pos+len]。
 */
Status HStr_Delete( HString *S, size_t pos, size_t len );

/*
 * 比较
 *
 * 比较串S和串T，返回比较结果。
 *
 *【注】
 * 该操作属于最小操作子集
 */
Status HStr_Compare( HString S, HString T );

/*
 * 复制
 *
 * 将串S复制到串T。
 */
Status HStr_Copy( HString *T, HString S );

/*
 * 替换
 *
 * 用V替换主串S中出现的所有与T相等的且不重叠的子串。
 *
 *【注】
 * 1.该操作依赖最小操作子集
 * 2.该实现比较低效
 */
Status HStr_Replace( HString *S, HString T, HString V );

Status HStr_ReplaceKMP( HString *S, HString T, HString V );

/*
 * 串联接
 *
 * 联接S1和S2，并存储到T中返回。如果联接后的长度溢出，则只保留未溢出的部分。
 * 返回值表示联接后的串是否完整。
 *
 *【注】
 * 该操作属于最小操作子集
 */
Status HStr_Concat( HString *T, HString S1, HString S2 );
