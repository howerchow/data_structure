
add_executable(NearlyOptimalSearchTree NearlyOptimalSearchTree.h NearlyOptimalSearchTree.c NearlyOptimalSearchTree-main.c BiTree.h BiTree.c LinkQueue.h LinkQueue.c SqStack.h SqStack.c Base.h Base.c)
target_include_directories(NearlyOptimalSearchTree PUBLIC ${CMAKE_SOURCE_DIR}/Status)
target_link_libraries(NearlyOptimalSearchTree Scanf_lib)

# 记录要拷贝到*.exe目录下的资源文件
file(GLOB TestData TestData*.txt)
# 将资源文件拷贝到*.exe目录下，不然无法加载
file(COPY ${TestData} DESTINATION ${CMAKE_CURRENT_BINARY_DIR})
