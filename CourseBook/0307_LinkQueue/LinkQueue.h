/*=========================
 * 队列的链式存储结构（链队）
 ==========================*/

#pragma once

#include "Status.h" //**▲01 绪论**//

/* 链队元素类型定义 */
typedef int QElemType;

// 队列元素结构
typedef struct QNode {
        QElemType data;
        struct QNode *next;
} QNode;

// 队列结构
typedef struct {
        QNode *front; // 队头指针
        QNode *rear;  // 队尾指针
} LinkQueue;          // 队列的链式存储表示

/*
 * 初始化
 *
 * 构造一个空的链队。
 * 初始化成功则返回OK，否则返回ERROR。
 *
 *【注】
 * 这里的队列带有头结点
 */
Status LinkQueue_InitQueue( LinkQueue *Q );

/*
 * 销毁(结构)
 *
 * 释放链队所占内存。
 */
Status LinkQueue_DestroyQueue( LinkQueue *Q );

/*
 * 置空(内容)
 *
 * 这里需要释放链队中非头结点处的空间。
 */
Status LinkQueue_ClearQueue( LinkQueue *Q );

/*
 * 判空
 *
 * 判断链队中是否包含有效数据。
 *
 * 返回值：
 * true : 链队为空
 * false: 链队不为空
 */
bool LinkQueue_QueueEmpty( LinkQueue Q );

/*
 * 计数
 *
 * 返回链队包含的有效元素的数量。
 */
int LinkQueue_QueueLength( LinkQueue Q );

/*
 * 取值
 *
 * 获取队头元素，将其存储到e中。
 * 如果可以找到，返回OK，否则，返回ERROR。
 */
Status LinkQueue_GetHead( LinkQueue Q, QElemType *e );

/*
 * 入队
 *
 * 将元素e添加到队列尾部。
 */
Status LinkQueue_EnQueue( LinkQueue *Q, QElemType e );

/*
 * 出队
 *
 * 移除队列头部的元素，将其存储到e中。
 */
Status LinkQueue_DeQueue( LinkQueue *Q, QElemType *e );

/*
 * 遍历
 *
 * 用visit函数访问队列Q
 */
Status LinkQueue_QueueTraverse( LinkQueue Q, void ( *Visit )( QElemType ) );
