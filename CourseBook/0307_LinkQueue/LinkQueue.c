/*=========================
 * 队列的链式存储结构（链队）
 ==========================*/

#include "LinkQueue.h" //**▲03 栈和队列**//
#include <stdio.h>
#include <stdlib.h> // malloc、realloc、free、exit

/*
 * 初始化
 *
 * 构造一个空的链队。
 * 初始化成功则返回OK，否则返回ERROR。
 *
 *【注】
 * 这里的队列带有头结点
 */
Status LinkQueue_InitQueue( LinkQueue *Q ) {
        if ( Q == nullptr ) {
                return ERROR;
        }

        Q->front = Q->rear = malloc( sizeof( QNode ) );
        if ( Q->front == nullptr ) {
                exit( OVERFLOW );
        }

        Q->front->next = nullptr;

        return OK;
}

/*
 * 销毁(结构)
 *
 * 释放链队所占内存。
 */
Status LinkQueue_DestroyQueue( LinkQueue *Q ) {
        if ( Q == nullptr ) {
                return ERROR;
        }

        while ( Q->front ) {
                Q->rear = Q->front->next;
                free( Q->front );
                Q->front = Q->rear;
        }

        return OK;
}

/*
 * 置空(内容)
 *
 * 这里需要释放链队中非头结点处的空间。
 */
Status LinkQueue_ClearQueue( LinkQueue *Q ) {
        if ( Q == nullptr ) {
                return ERROR;
        }

        Q->rear = Q->front->next;

        while ( Q->rear ) {
                Q->front->next = Q->rear->next;
                free( Q->rear );
                Q->rear = Q->front->next;
        }

        Q->rear = Q->front;

        return OK;
}

/*
 * 判空
 *
 * 判断链队中是否包含有效数据。
 *
 * 返回值：
 * true : 链队为空
 * false: 链队不为空
 */
bool LinkQueue_QueueEmpty( LinkQueue Q ) {
        if ( Q.front == Q.rear ) {
                return true;
        }
        return false;
}

/*
 * 计数
 *
 * 返回链队包含的有效元素的数量。
 */
int LinkQueue_QueueLength( LinkQueue Q ) {
        int count = 0;
        QNode *p = Q.front;

        while ( p != Q.rear ) {
                ++count;
                p = p->next;
        }

        return count;
}

/*
 * 取值
 *
 * 获取队头元素，将其存储到e中。
 * 如果可以找到，返回OK，否则，返回ERROR。
 */
Status LinkQueue_GetHead( LinkQueue Q, QElemType *e ) {

        if ( Q.front == nullptr || Q.front == Q.rear ) {
                return ERROR;
        }

        QNode *p = Q.front->next;
        *e = p->data;

        return OK;
}

/*
 * 入队
 *
 * 将元素e添加到队列尾部。
 */
Status LinkQueue_EnQueue( LinkQueue *Q, QElemType e ) {

        if ( Q == nullptr || Q->front == nullptr ) {
                return ERROR;
        }

        QNode *p = malloc( sizeof( QNode ) );
        if ( p == nullptr ) {
                exit( OVERFLOW );
        }

        p->data = e;
        p->next = nullptr;

        Q->rear->next = p;
        Q->rear = p;

        return OK;
}

/*
 * 出队
 *
 * 移除队列头部的元素，将其存储到e中。
 */
Status LinkQueue_DeQueue( LinkQueue *Q, QElemType *e ) {

        if ( Q == nullptr || Q->front == nullptr || Q->front == Q->rear ) {
                return ERROR;
        }

        QNode *p = Q->front->next;
        *e = p->data;

        Q->front->next = p->next;
        if ( Q->rear == p ) {
                Q->rear = Q->front;
        }

        free( p );

        return OK;
}

/*
 * 遍历
 *
 * 用visit函数访问队列Q
 */
Status LinkQueue_QueueTraverse( LinkQueue Q, void ( *Visit )( QElemType ) ) {

        if ( Q.front == nullptr ) {
                return ERROR;
        }

        QNode *p = Q.front->next;

        while ( p != nullptr ) {
                Visit( p->data );
                p = p->next;
        }

        printf( "\n" );

        return OK;
}
