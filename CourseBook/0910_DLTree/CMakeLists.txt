
add_executable(DLTree DLTree.h DLTree.c DLTree-main.c)
target_include_directories(DLTree PUBLIC ${CMAKE_SOURCE_DIR}/Status)
target_link_libraries(DLTree Scanf_lib m)

# 记录要拷贝到*.exe目录下的资源文件
file(GLOB TestData TestData*.txt)
# 将资源文件拷贝到*.exe目录下，不然无法加载
file(COPY ${TestData} DESTINATION ${CMAKE_CURRENT_BINARY_DIR})
