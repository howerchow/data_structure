#include "SqList.h" //**02 线性表**//
#include <stdio.h>

// 判断data>e是否成立
bool CmpGreater( ElemType data, ElemType e ) {
        return data > e ? true : false;
}

// 测试函数，打印元素
void PrintElem( ElemType e ) {
        printf( "%d ", e );
}

int main( int argc, char **argv ) {
        SqList L; // 待操作的顺序表

        size_t i;
        ElemType e;

        printf( "████████ InitList \n" );
        {
                printf( "█ 初始化顺序表 L ...\n" );
                SqList_InitList( &L );
        }
        PressEnterToContinue( debug );

        printf( "████████ ListEmpty \n" );
        {
                if ( SqList_ListEmpty( L ) == true ) {
                        printf( "█ L 为空！！\n" );
                } else {
                        printf( "█ L 不为空！\n" );
                }
        }
        PressEnterToContinue( debug );

        printf( "████████ ListInsert \n" );
        {
                for ( i = 0; i < 8; i++ ) {
                        printf(
                            "█ 作为示范，在 L 第 %zu 个位置插入\"%zu\"...\n", i,
                            2 * i );
                        SqList_ListInsert( &L, i, 2 * i );
                }
        }
        PressEnterToContinue( debug );

        printf( "████████ ListTraverse \n" );
        {
                printf( "█ L 中的元素为：L = " );
                SqList_ListTraverse( L, PrintElem );
        }
        PressEnterToContinue( debug );

        printf( "████████ ListLength \n" );
        {
                i = SqList_ListLength( L );
                printf( "█ L 的长度为 %zu \n", i );
        }
        PressEnterToContinue( debug );

        printf( "████████ ListDelete \n" );
        {
                printf( "█ 删除前的元素：L = " );
                SqList_ListTraverse( L, PrintElem );

                printf( "█ 尝试删除 L 中第 6 个元素...\n" );

                if ( SqList_ListDelete( &L, 6, &e ) == OK ) {
                        printf( "█ 删除成功，被删除元素是：\"%d\"\n", e );
                } else {
                        printf( "█ 删除失败，第 6 个元素不存在！\n" );
                }

                printf( "█ 删除后的元素：L = " );
                SqList_ListTraverse( L, PrintElem );
        }
        PressEnterToContinue( debug );

        printf( "████████ GetElem \n" );
        {
                printf( "█ L 中第 4 个位置的元素为 \"%d\" \n",
                        SqList_GetElem( L, 4 ) );
        }
        PressEnterToContinue( debug );

        printf( "████████ LocateElem \n" );
        {
                i = SqList_LocateElem( L, 7, CmpGreater );
                printf( "█ L 中第一个元素值大于 \"7\" 的元素是 \"%d\" \n",
                        L.elem[ i ] );
        }
        PressEnterToContinue( debug );

        printf( "████████ PriorElem \n" );
        {
                ElemType cur_e = 6;

                if ( SqList_PriorElem( L, cur_e, &e ) == OK ) {
                        printf( "█ 元素 \"%d\" 的前驱为 \"%d\" \n", cur_e, e );
                } else {
                        printf( "█ 元素 \"%d\" 的前驱不存在！\n", cur_e );
                }
        }
        PressEnterToContinue( debug );

        printf( "████████ NextElem \n" );
        {
                ElemType cur_e = 6;

                if ( SqList_NextElem( L, cur_e, &e ) == OK ) {
                        printf( "█ 元素 \"%d\" 的后继为 \"%d\" \n", cur_e, e );
                } else {
                        printf( "█ 元素 \"%d\" 的后继不存在！\n", cur_e );
                }
        }
        PressEnterToContinue( debug );

        printf( "████████ ClearList \n" );
        {
                printf( "█ 清空 L 前：" );
                if ( SqList_ListEmpty( L ) == true ) {
                        printf( " L 为空！！\n" );
                } else {
                        printf( " L 不为空！\n" );
                }

                SqList_ClearList( &L );

                printf( "█ 清空 L 后：" );
                if ( SqList_ListEmpty( L ) == true ) {
                        printf( " L 为空！！\n" );
                } else {
                        printf( " L 不为空！\n" );
                }
        }
        PressEnterToContinue( debug );

        printf( "████████ DestroyList \n" );
        {
                printf( "█ 销毁 L 前：" );
                if ( L.elem != nullptr ) {
                        printf( " L 存在！\n" );
                } else {
                        printf( " L 不存在！！\n" );
                }

                SqList_DestroyList( &L );

                printf( "█ 销毁 L 后：" );
                if ( L.elem != nullptr ) {
                        printf( " L 存在！\n" );
                } else {
                        printf( " L 不存在！！\n" );
                }
        }
        PressEnterToContinue( debug );

        printf( "████████ SqList_ListDeleteElement \n" );
        {
                SqList_InitList( &L );

                for ( int i = 0; i < 10; ++i ) {
                        printf( "█ 作为示范，在 L 第 %d 个位置插入 \"%d\"...\n",
                                i, 2 * i );
                        SqList_ListInsert( &L, i, 2 * i );
                }

                printf( "█ L 中的元素为：L = " );
                SqList_ListTraverse( L, PrintElem );
                printf( "█ L 的长度为：L.length = %zu\n", L.length );

                SqList_ListDeleteElement( &L, 4 );

                printf( "█ L 中的元素为：L = " );
                SqList_ListTraverse( L, PrintElem );
                printf( "█ L 的长度为：L.length = %zu\n", L.length );

                SqList_DestroyList( &L );
        }
        PressEnterToContinue( debug );

        printf( "████████ SqList_DeleteDuplicates \n" );
        {
                SqList_InitList( &L );

                for ( int i = 0; i < 10; ++i ) {
                        printf( "█ 作为示范，在 L 第 %d 个位置插入 \"%d\"...\n",
                                i, 2 * i );
                        SqList_ListInsert( &L, i, 2 * i );
                }
                printf( "\n" );
                for ( int j = 10; j < 20; ++j ) {
                        printf( "█ 作为示范，在 L 第 %d 个位置插入\"%d\"...\n",
                                j, 2 * j / 10 );
                        SqList_ListInsert( &L, j, 2 * j / 10 );
                }
                printf( "█ L 中的元素为：L = " );
                SqList_ListTraverse( L, PrintElem );
                printf( "█ L 的长度为：L.length = %zu\n", L.length );

                // SqList_DeleteDuplicates( &L );
                SqList_DeleteAdjacentDuplicates( &L );

                printf( "█ L 中的元素为：L = " );
                SqList_ListTraverse( L, PrintElem );
                printf( "█ L 的长度为：L.length = %zu\n", L.length );

                SqList_DestroyList( &L );
        }
        PressEnterToContinue( debug );

        return 0;
}
