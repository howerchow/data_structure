#include "BiTree.h" //**▲06 树和二叉树**//
#include "Status.h" //**▲01 绪论**//
#include <stdio.h>

// 测试函数，打印元素
Status PrintElem( TElemType c ) {
        printf( "%c", c );
        return OK;
}

int main( int argc, char *argv[] ) {
        BiTree T;

        printf( "████████ InitBiTree \n" );
        {
                printf( "█ 初始化空二叉树 T ...\n" );
                BiTree_InitBiTree( &T );
        }
        PressEnterToContinue( debug );

        printf( "████████ CreateBiTree \n" );
        {
                printf( "█ 按先序序列创建二叉树 T ...\n" );
                BiTree_CreateBiTree( &T, "TestData_Pre.txt" );
        }
        PressEnterToContinue( debug );

        printf( "████████ BiTreeDepth \n" );
        { printf( "█ 二叉树 T 的深度为：%d \n", BiTree_GetDepth( T ) ); }
        PressEnterToContinue( debug );

        printf( "████████ BiTree_GetDepthNoneRecursive \n" );
        { printf( "█ 二叉树 T 的深度为：%d \n", BiTree_GetDepthNoneRecursive( T ) ); }
        PressEnterToContinue( debug );

        printf( "████████ PrintTree \n" );
        {
                printf( "█ 按二叉树的结构打印树 T ...\n" );
                PrintTree( T );
        }
        PressEnterToContinue( debug );

        printf( "████████ PreOrderTraverse \n" );
        {
                printf( "█ 前序遍历二叉树 T = " );
                BiTree_PreOrderTraverse( T, PrintElem );
        }
        PressEnterToContinue( debug );

        printf( "████████ BiTree_MorrisPreOrderTraverse \n" );
        {
                printf( "█ Morris前序遍历二叉树 T = " );
                BiTree_MorrisPreOrderTraverse( T, PrintElem );
                printf( "\n" );
        }
        PressEnterToContinue( debug );

        printf( "████████ InOrderTraverse \n" );
        {
                printf( "█ 中序遍历二叉树 T = " );
                BiTree_InOrderTraverse( T, PrintElem );
        }
        PressEnterToContinue( debug );

        printf( "████████ BiTree_InOrderTraverse_4 \n" );
        {
                printf( "█ 中序遍历二叉树 T = " );
                BiTree_InOrderTraverse_4( T, PrintElem );
                printf( "\n" );
        }
        PressEnterToContinue( debug );

        printf( "████████ BiTree_MorrisInOrderTraverse \n" );
        {
                printf( "█ Morris中序遍历二叉树 T = " );
                BiTree_MorrisInOrderTraverse( T, PrintElem );
                printf( "\n" );
        }
        PressEnterToContinue( debug );

        printf( "████████ PostOrderTraverse \n" );
        {
                printf( "█ 后序遍历二叉树 T = " );
                BiTree_PostOrderTraverse( T, PrintElem );
        }
        PressEnterToContinue( debug );

        printf( "████████ BiTree_MorrisPostOrderTraverse \n" );
        {
                printf( "█ Morris后序遍历二叉树 T = " );
                BiTree_MorrisPostOrderTraverse( T, PrintElem );
                printf( "\n" );
        }
        PressEnterToContinue( debug );

        printf( "████████ LevelOrderTraverse \n" );
        {
                printf( "█ 层序遍历二叉树 T = " );
                BiTree_LevelOrderTraverse( T, PrintElem );
        }
        PressEnterToContinue( debug );

        printf( "████████ BiTree_GetValue \n" );
        {
                TElemType e = 'F';
                printf( "█ 结点 %c 的值为 %c\n", e, BiTree_GetValue( T, e ) );
        }
        PressEnterToContinue( debug );

        printf( "████████ BiTree_Assign \n" );
        {
                TElemType e = 'F';
                TElemType value = 'X';
                printf( "█ 将结点 %c 赋值为 %c 后，T = \n", e, value );
                BiTree_Assign( T, e, value );
                PrintTree( T );
        }
        PressEnterToContinue( debug );

        printf( "████████ BiTree_GetRoot \n" );
        { printf( "█ T 的根结点为 %c\n", BiTree_GetRoot( T ) ); }
        PressEnterToContinue( debug );

        printf( "████████ BiTree_GetParent \n" );
        {
                TElemType e = 'E';
                printf( "█ 结点 %c 的双亲为：%c \n", e,
                        BiTree_GetParent( T, e ) );
        }
        PressEnterToContinue( debug );

        printf( "████████ LeftChild、RightChild \n" );
        {
                TElemType e = 'E';
                printf( "█ 结点 %c 的左孩子结点值为：%c ，右孩子结点值为：%c\n",
                        e, BiTree_LeftChild( T, e ),
                        BiTree_RightChild( T, e ) );
        }
        PressEnterToContinue( debug );

        printf( "████████ LeftSibling \n" );
        {
                TElemType e = 'I';
                printf( "█ 结点 %c 的左兄弟为：%c\n", e,
                        BiTree_LeftSibling( T, e ) );
        }
        PressEnterToContinue( debug );

        printf( "████████ RightSibling \n" );
        {
                TElemType e = 'H';
                printf( "█ 结点 %c 的右兄弟为：%c\n", e,
                        BiTree_RightSibling( T, e ) );
        }
        PressEnterToContinue( debug );

        printf( "████████ BiTree_LowestCommonAncestorByValue \n" );
        {
                TElemType e1 = 'H';
                TElemType e2 = 'J';
                TElemType a = BiTree_LowestCommonAncestorByValue( T, e1, e2 );
                printf( "█ 结点 %c %c 最近公共祖先为：%c\n", e1, e2, a );
        }
        PressEnterToContinue( debug );

        printf( "████████ InsertChild \n" );
        {
                BiTree c1, c2;
                TElemType p1 = 'D';
                TElemType p2 = 'E';

                printf( "█ 创建子树 c1 ...\n" );
                BiTree_InitBiTree( &c1 );
                BiTree_CreateBiTree( &c1, "TestData_c1.txt" );
                PrintTree( c1 );

                printf( "█ 创建子树 c2 ...\n" );
                BiTree_InitBiTree( &c2 );
                BiTree_CreateBiTree( &c2, "TestData_c2.txt" );
                PrintTree( c2 );

                printf( "█ 将子树 c1 插入为二叉树 T 中 %c 结点的右子树 ...\n",
                        p1 );
                BiTree_InsertChild( T, p1, 1, c1 );
                PrintTree( T );

                printf( "█ 将子树 c2 插入为二叉树 T 中 %c 结点的左子树 ...\n",
                        p2 );
                BiTree_InsertChild( T, p2, 0, c2 );
                PrintTree( T );
        }
        PressEnterToContinue( debug );

        printf( "████████ DeleteChild \n" );
        {
                TElemType p1 = 'D';
                TElemType p2 = 'E';

                printf( "█ 删除二叉树 T 中 %c 结点的右子树 ...\n", p1 );
                BiTree_DeleteChild( T, p1, 1 );
                PrintTree( T );

                printf( "█ 删除二叉树 T 中 %c 结点的左子树 ...\n", p2 );
                BiTree_DeleteChild( T, p2, 0 );
                PrintTree( T );
        }
        PressEnterToContinue( debug );

        printf( "████████ BiTree_InvertTree \n" );
        {
                printf( "█ 翻转二叉树T \n" );
                BiTree_InvertTree( T );
                PrintTree( T );
        }
        PressEnterToContinue( debug );

        printf( "████████ BiTree_IsCompleteBiTree \n" );
        {
                bool b = BiTree_IsCompleteBiTree( T );
                printf( "█ 是否是完全二叉树: %s\n", b ? "true" : "false" );
        }
        PressEnterToContinue( debug );

        printf( "████████ ClearBiTree、BiTree_IsEmpty \n" );
        {
                printf( "█ 清空前：" );
                BiTree_IsEmpty( T ) ? printf( "T 为空！\n" )
                                    : printf( "T 不为空！\n" );

                BiTree_ClearBiTree( &T );

                printf( "█ 清空后：" );
                BiTree_IsEmpty( T ) ? printf( "T 为空！\n" )
                                    : printf( "T 不为空！\n" );
        }
        PressEnterToContinue( debug );

        return 0;
}
