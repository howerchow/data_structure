/*=========================
 * 队列的链式存储结构（链队）
 ==========================*/

#include "LinkQueue.h" //**▲03 栈和队列**//

/*
 * 初始化
 *
 * 构造一个空的链队。
 * 初始化成功则返回OK，否则返回ERROR。
 *
 *【注】
 * 这里的队列带有头结点
 */
Status LinkQueue_InitQueue( LinkQueue *Q ) {
        if ( Q == nullptr ) {
                return ERROR;
        }

        ( *Q ).front = ( *Q ).rear = ( QueuePtr )malloc( sizeof( QNode ) );
        if ( !( *Q ).front ) {
                exit( OVERFLOW );
        }

        ( *Q ).front->next = nullptr;

        return OK;
}

/*
 * 销毁(结构)
 *
 * 释放链队所占内存。
 */
Status LinkQueue_DestroyQueue( LinkQueue *Q ) {
        if ( Q == nullptr ) {
                return ERROR;
        }

        while ( Q->front ) {
                Q->rear = Q->front->next;
                free( Q->front );
                Q->front = Q->rear;
        }

        return OK;
}

/*
 * 判空
 *
 * 判断链队中是否包含有效数据。
 *
 * 返回值：
 * true : 链队为空
 * false: 链队不为空
 */
Status LinkQueue_IsEmpty( LinkQueue Q ) {
        if ( Q.front == Q.rear ) {
                return true;
        } else {
                return false;
        }
}

/*
 * 入队
 *
 * 将元素e添加到队列尾部。
 */
Status LinkQueue_EnQueue( LinkQueue *Q, QElemType e ) {
        if ( Q == nullptr || ( *Q ).front == nullptr ) {
                return ERROR;
        }

        QueuePtr p = malloc( sizeof( QNode ) );
        if ( p == nullptr ) {
                exit( OVERFLOW );
        }

        p->data = e;
        p->next = nullptr;

        ( *Q ).rear->next = p;
        ( *Q ).rear = p;

        return OK;
}

/*
 * 出队
 *
 * 移除队列头部的元素，将其存储到e中。
 */
Status LinkQueue_DeQueue( LinkQueue *Q, QElemType *e ) {
        if ( Q == nullptr || ( *Q ).front == nullptr ||
             ( *Q ).front == ( *Q ).rear ) {
                return ERROR;
        }

        QueuePtr p = ( *Q ).front->next;
        *e = p->data;

        ( *Q ).front->next = p->next;
        if ( ( *Q ).rear == p ) {
                ( *Q ).rear = ( *Q ).front;
        }

        free( p );

        return OK;
}
