/*=============================
 * 二叉树的二叉链表存储结构
 *
 * 包含算法: 6.1、6.2、6.3、6.4
 =============================*/

#include "BiTree.h"
#include "LinkQueue.h" //**▲03 栈和队列**//
#include "SqStack.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h> // malloc、free
#include <string.h> // memset、strcmp

/*━━━━━━━━━━━━━━━━━━━━━━ 仅限内部使用的函数 ━━━━━━━━━━━━━━━━━━━━━━*/

// 创建二叉树的内部函数
static void CreateTree( BiTree *T, FILE *fp ) {
        char ch;

        // 读取当前结点的值
        if ( fp == nullptr ) {
                scanf( "%c", &ch );
        } else {
                ReadData( fp, "%c", &ch );
        }

        if ( ch == '^' ) {
                *T = nullptr;
        } else {
                // 生成根结点
                *T = malloc( sizeof( BiTNode ) );
                if ( *T == nullptr ) {
                        exit( OVERFLOW );
                }
                ( *T )->data = ch;
                CreateTree( &( ( *T )->lchild ), fp ); // 创建左子树
                CreateTree( &( ( *T )->rchild ), fp ); // 创建右子树
        }
}

// 返回指向二叉树结点e的指针
static BiTree EPtr( BiTree T, TElemType e ) {
        BiTree pl, pr;

        if ( T == nullptr ) {
                return nullptr;
        }

        // 如果找到了目标结点，直接返回其指针
        if ( T->data == e ) {
                return T;
        }

        // 在左子树中查找e
        pl = EPtr( T->lchild, e );
        if ( pl != nullptr ) {
                return pl;
        }

        // 在右子树中查找e
        pr = EPtr( T->rchild, e );
        if ( pr != nullptr ) {
                return pr;
        }

        return nullptr;
}

// 返回指向二叉树结点e的双亲结点的指针
static BiTree PPtr( BiTree T, TElemType e ) {
        BiTree pl, pr;

        if ( T == nullptr || T->data == e ) {
                return nullptr;
        }

        // e是T的左孩子
        if ( T->lchild != nullptr && T->lchild->data == e ) {
                return T;
        }

        // e是T的右孩子
        if ( T->rchild != nullptr && T->rchild->data == e ) {
                return T;
        }

        // 在左子树中查找e
        pl = PPtr( T->lchild, e );
        if ( pl != nullptr ) {
                return pl;
        }

        // 在右子树中查找e
        pr = PPtr( T->rchild, e );
        if ( pr != nullptr ) {
                return pr;
        }

        return nullptr;
}

// 先序遍历的内部实现
static Status PreTraverse( BiTree T, Status ( *Visit )( TElemType ) ) {
        if ( T ) {
                if ( Visit( T->data ) ) {
                        if ( PreTraverse( T->lchild, Visit ) ) {
                                if ( PreTraverse( T->rchild, Visit ) ) {
                                        return OK;
                                }
                        }
                }
                return ERROR;
        } else { // 遇到空树则无需继续计算
                return OK;
        }
}

// 中序遍历的内部实现
static Status InTraverse( BiTree T, Status ( *Visit )( TElemType ) ) {
        if ( T ) {
                if ( InTraverse( T->lchild, Visit ) ) {
                        if ( Visit( T->data ) ) {
                                if ( InTraverse( T->rchild, Visit ) ) {
                                        return OK;
                                }
                        }
                }
                return ERROR;
        } else { // 遇到空树则无需继续计算
                return OK;
        }
}

// 后序遍历的内部实现
static Status PostTraverse( BiTree T, Status ( *Visit )( TElemType ) ) {
        if ( T ) {
                if ( PostTraverse( T->lchild, Visit ) ) {
                        if ( PostTraverse( T->rchild, Visit ) ) {
                                if ( Visit( T->data ) ) {
                                        return OK;
                                }
                        }
                }
                return ERROR;
        } else { // 遇到空树则无需继续计算
                return OK;
        }
}

/*
 * 初始化
 *
 * 构造空二叉树。
 */
Status BiTree_InitBiTree( BiTree *T ) {
        if ( T == nullptr ) {
                return ERROR;
        }

        *T = nullptr;

        return OK;
}

/*
 * 销毁
 *
 * 释放二叉树所占内存。
 *
 *【注】
 * 二叉树的二叉链表存储结构可以销毁，但是没必要销毁。
 * 因为二叉链表销毁后的状态与置空后的状态是一致的。
 */
Status BiTree_DestroyBiTree( BiTree *T ) {
        // 无需销毁，使用置空就可以
        return ERROR;
}

/*
 * 置空
 *
 * 清理二叉树中的数据，使其成为空树。
 */
Status BiTree_ClearBiTree( BiTree *T ) {
        if ( T == nullptr ) {
                return ERROR;
        }

        // 在*T不为空时进行递归清理
        if ( *T ) {
                if ( ( *T )->lchild != nullptr ) {
                        BiTree_ClearBiTree( &( ( *T )->lchild ) );
                }

                if ( ( *T )->rchild != nullptr ) {
                        BiTree_ClearBiTree( &( ( *T )->rchild ) );
                }

                free( *T );
                *T = nullptr;
        }

        return OK;
}

/*
 * ████████ 算法6.4 ████████
 *
 * 创建
 *
 * 按照预设的定义来创建二叉树。
 * 这里约定使用【先序序列】来创建二叉树。
 *
 *
 *【备注】
 *
 * 教材中默认从控制台读取数据。
 * 这里为了方便测试，避免每次运行都手动输入数据，
 * 因而允许选择从预设的文件path中读取测试数据。
 *
 * 如果需要从控制台读取数据，则path为nullptr或者为空串，
 * 如果需要从文件中读取数据，则需要在path中填写文件名信息。
 */
Status BiTree_CreateBiTree( BiTree *T, char *path ) {
        // 是否从控制台读取数据
        bool readFromConsole = false;
        // 如果没有文件路径信息，则从控制台读取输入
        if ( path == nullptr || strcmp( path, "" ) == 0 ) {
                readFromConsole = true;
        }

        if ( readFromConsole ) {
                printf( "请输入二叉树的先序序列，如果没有子结点，使用^代替：" );
                CreateTree( T, nullptr );
        } else {
                // 打开文件，准备读取测试数据
                FILE *fp = fopen( path, "r" );
                if ( fp == nullptr ) {
                        return ERROR;
                }
                CreateTree( T, fp );
                fclose( fp );
        }

        return OK;
}

/*
 * 判空
 *
 * 判断二叉树是否为空树。
 */
bool BiTree_IsEmpty( BiTree T ) {
        return T == nullptr ? true : false;
}

/*
 * 树深
 *
 * 返回二叉树的深度（层数）。
 */
int BiTree_GetDepth( BiTree T ) {

        if ( BiTree_IsEmpty( T ) ) {
                return 0; // 空树深度为0
        }

        int LD = BiTree_GetDepth( T->lchild ); // 求左子树深度
        int RD = BiTree_GetDepth( T->rchild ); // 求右子树深度

        return ( LD >= RD ? LD : RD ) + 1;
}

int BiTree_GetDepthNoneRecursive( BiTree T ) {
        if ( BiTree_IsEmpty( T ) ) {
                return 0;
        }

        BiTNode *curr = nullptr;
        int level = 0;

        LinkQueue Q;
        LinkQueue_InitQueue( &Q );
        LinkQueue_EnQueue( &Q, T );

        BiTNode *last = Q.front->next->data;

        while ( !LinkQueue_IsEmpty( Q ) ) {
                LinkQueue_DeQueue( &Q, &curr );

                if ( curr->lchild != nullptr ) {
                        LinkQueue_EnQueue( &Q, curr->lchild );
                }

                if ( curr->rchild != nullptr ) {
                        LinkQueue_EnQueue( &Q, curr->rchild );
                }

                if ( curr == last ) {
                        ++level;
                        last = Q.rear->data;
                }
        }
        LinkQueue_DestroyQueue( &Q );

        return level;
}

/*
 * 取值
 *
 * 返回二叉树中指定结点的值。
 */
TElemType BiTree_GetValue( BiTree T, TElemType e ) {
        // 遇到空树则无需继续计算
        if ( BiTree_IsEmpty( T ) ) {
                return '\0';
        }

        // 获取结点e的指针
        BiTree p = EPtr( T, e );

        // 如果没有找到元素e
        if ( p == nullptr ) {
                return '\0';
        } else {
                return p->data;
        }
}

/*
 * 赋值
 *
 * 为二叉树指定的结点赋值。
 */
Status BiTree_Assign( BiTree T, TElemType e, TElemType value ) {
        // 遇到空树则无需继续计算
        if ( BiTree_IsEmpty( T ) ) {
                return ERROR;
        }

        // 获取结点e的指针
        BiTree p = EPtr( T, e );

        // 如果没有找到元素e
        if ( p == nullptr ) {
                return ERROR;
        }
        // 进行赋值
        p->data = value;

        return OK;
}

/*
 * 根
 *
 * 返回二叉树的根结点。
 */
TElemType BiTree_GetRoot( BiTree T ) {
        // 遇到空树则无需继续计算
        if ( BiTree_IsEmpty( T ) ) {
                return '\0';
        }

        return T->data;
}

/*
 * 双亲
 *
 * 返回二叉树中结点e的双亲结点。
 */
TElemType BiTree_GetParent( BiTree T, TElemType e ) {
        // 遇到空树则无需继续计算
        if ( BiTree_IsEmpty( T ) ) {
                return '\0';
        }

        // 获取结点e的双亲结点的指针
        BiTree p = PPtr( T, e );

        // 如果没有找到元素e的双亲
        if ( p == nullptr ) {
                return '\0';
        } else {
                return p->data;
        }
}

/*
 * 左孩子
 *
 * 返回二叉树中结点e的左孩子结点。
 */
TElemType BiTree_LeftChild( BiTree T, TElemType e ) {

        // 遇到空树则无需继续计算
        if ( BiTree_IsEmpty( T ) ) {
                return '\0';
        }

        // 获取结点e的指针
        BiTree p = EPtr( T, e );

        // 如果找到了元素e
        if ( p != nullptr && p->lchild != nullptr ) {
                return p->lchild->data;
        }

        return '\0';
}

/*
 * 右孩子
 *
 * 返回二叉树中结点e的右孩子结点。
 */
TElemType BiTree_RightChild( BiTree T, TElemType e ) {

        // 遇到空树则无需继续计算
        if ( BiTree_IsEmpty( T ) ) {
                return '\0';
        }

        // 获取结点e的指针
        BiTree p = EPtr( T, e );

        // 如果找到了元素e
        if ( p != nullptr && p->rchild != nullptr ) {
                return p->rchild->data;
        }

        return '\0';
}

/*
 * 左兄弟
 *
 * 返回二叉树中结点e的左兄弟结点。
 */
TElemType BiTree_LeftSibling( BiTree T, TElemType e ) {

        // 遇到空树则无需继续计算
        if ( BiTree_IsEmpty( T ) ) {
                return '\0';
        }

        // 获取结点e的双亲结点的指针
        BiTree p = PPtr( T, e );

        // 如果找到了元素e的双亲
        if ( p != nullptr && p->lchild != nullptr ) {
                return p->lchild->data;
        }

        return '\0';
}

/*
 * 右兄弟
 *
 * 返回二叉树中结点e的右兄弟结点。
 */
TElemType BiTree_RightSibling( BiTree T, TElemType e ) {

        // 遇到空树则无需继续计算
        if ( BiTree_IsEmpty( T ) ) {
                return '\0';
        }

        // 获取结点e的双亲结点的指针
        BiTree p = PPtr( T, e );

        // 如果找到了元素e的双亲
        if ( p != nullptr && p->rchild != nullptr ) {
                return p->rchild->data;
        }

        return '\0';
}

/*
 * 插入
 *
 * 已知c为与T不相交的非空二叉树，且c的右子树为空，
 * 根据LR的取值(0或1)，将c插入为T中结点p的左子树/右子树，
 * 并且，将p结点原有的左子树/右子树嫁接为二叉树c的右子树。
 */
Status BiTree_InsertChild( BiTree T, TElemType p, int LR, BiTree c ) {

        // 如果待插入的树为空树则无需继续计算
        if ( BiTree_IsEmpty( c ) ) {
                return ERROR;
        }

        // 获取结点p的指针
        BiTree p_ptr = EPtr( T, p );

        // 如果p结点不存在，则返回错误提示
        if ( p_ptr == nullptr ) {
                return ERROR;
        }

        // 将c插入为p的左子树
        if ( LR == 0 ) {
                // 如果p处存在左子树，则摘下p的左子树，插入为c的右子树
                if ( p_ptr->lchild != nullptr ) {
                        c->rchild = p_ptr->lchild;
                }

                p_ptr->lchild = c;
        } else {
                // 如果p处存在右子树，则摘下p的右子树，插入为c的右子树
                if ( p_ptr->rchild != nullptr ) {
                        c->rchild = p_ptr->rchild;
                }

                p_ptr->rchild = c;
        }

        return OK;
}

/*
 * 删除
 *
 * 根据LR的取值(0或1)，删除结点p的左子树/右子树。
 */
Status BiTree_DeleteChild( BiTree T, TElemType p, int LR ) {

        // 遇到空树则无需继续计算
        if ( BiTree_IsEmpty( T ) ) {
                return ERROR;
        }

        // 获取结点p的指针
        BiTree p_ptr = EPtr( T, p );

        // 如果p结点不存在，则返回错误提示
        if ( p_ptr == nullptr ) {
                return ERROR;
        }

        // 如果需要删除p的左子树
        if ( LR == 0 ) {
                BiTree_ClearBiTree( &( p_ptr->lchild ) );

                // 如果需要删除p的右子树
        } else {
                BiTree_ClearBiTree( &( p_ptr->rchild ) );
        }

        return OK;
}

/*
 * ████████ 算法6.1 ████████
 *
 * 先序遍历
 */
Status BiTree_PreOrderTraverse( BiTree T, Status ( *Visit )( TElemType ) ) {

        Status status = PreTraverse( T, Visit );
        printf( "\n" );

        return status;
}

/*
 * 中序遍历
 */
Status BiTree_InOrderTraverse( BiTree T, Status ( *Visit )( TElemType ) ) {

        Status status = InTraverse( T, Visit );
        printf( "\n" );

        return status;
}

/*
 * ████████ 算法6.2 ████████
 *
 * 中序遍历
 *
 *【注】
 * 非递归算法
 */
Status BiTree_InOrderTraverse_2( BiTree T, Status ( *Visit )( TElemType ) ) {
        BiTree p;

        SqStack S;
        SqStack_InitStack( &S );
        SqStack_Push( &S, T ); // 根指针入栈

        while ( !SqStack_IsEmpty( S ) ) {
                while ( SqStack_GetTop( S, &p ) &&
                        p != nullptr ) { // 向左走到尽头
                        SqStack_Push( &S, p->lchild );
                }

                SqStack_Pop( &S, &p ); // 空指针退栈

                if ( !SqStack_IsEmpty( S ) ) {
                        SqStack_Pop( &S, &p );
                        if ( !Visit( p->data ) ) { // 访问结点
                                return ERROR;
                        }

                        SqStack_Push( &S, p->rchild ); // 向右一步
                }
        }

        printf( "\n" );

        return OK;
}

/*
 * ████████ 算法6.3 ████████
 *
 * 中序遍历
 *
 *【注】
 * 非递归算法
 */
Status BiTree_InOrderTraverse_3( BiTree T, Status ( *Visit )( TElemType ) ) {
        SqStack S;
        SqStack_InitStack( &S );

        BiTree p = T;

        while ( p != nullptr || !SqStack_IsEmpty( S ) ) {
                if ( p != nullptr ) {
                        SqStack_Push( &S, p ); // 根指针进栈
                        p = p->lchild;         // 遍历左子树
                } else {
                        // 访问结点
                        SqStack_Pop( &S, &p );
                        if ( !Visit( p->data ) ) {
                                return ERROR;
                        }

                        p = p->rchild;
                }
        }
        SqStack_DestroyStack( &S );
        printf( "\n" );

        return OK;
}

InOrderIter *BiTree_GetInOrderIter( BiTree T ) {
        if ( BiTree_IsEmpty( T ) ) {
                return nullptr;
        }

        InOrderIter *iter = malloc( sizeof( InOrderIter ) );
        if ( iter == nullptr ) {
                return nullptr;
        }
        iter->S = malloc( sizeof( SqStack ) );
        SqStack_InitStack( iter->S );

        BiTree p = T;

        while ( p != nullptr ) {
                SqStack_Push( iter->S, p ); // 根指针进栈
                p = p->lchild;              // 遍历左子树
        }

        return iter;
}

BiTNode *BiTree_GetNext( InOrderIter *iter ) {
        if ( iter == nullptr ||
             ( iter->S != nullptr && SqStack_IsEmpty( *( iter->S ) ) ) ) {
                return nullptr;
        }
        BiTNode *top = nullptr;
        SqStack_Pop( iter->S, &top );

        if ( top != nullptr && top->rchild != nullptr ) {
                BiTNode *current = top->rchild;
                while ( current != nullptr ) {
                        SqStack_Push( iter->S, current );
                        current = current->lchild;
                }
        }
        return top;
}

void BiTree_ReleaseInOrderIter( InOrderIter *iter ) {
        if ( iter == nullptr ) {
                return;
        }
        SqStack_DestroyStack( iter->S );
        free( iter->S );
        free( iter );
}

/*
 *
 * 中序遍历
 *
 *【注】
 * 迭代器中序遍历
 *
 */
Status BiTree_InOrderTraverse_4( BiTree T, Status ( *Visit )( TElemType ) ) {
        if ( BiTree_IsEmpty( T ) ) {
                return ERROR;
        }
        BiTNode *current = nullptr;

        InOrderIter *iter = BiTree_GetInOrderIter( T );
        while ( ( current = BiTree_GetNext( iter ) ) != nullptr ) {
                Visit( current->data );
        }
        BiTree_ReleaseInOrderIter( iter );

        return OK;
}

/*
 * 后序遍历
 */
Status BiTree_PostOrderTraverse( BiTree T, Status ( *Visit )( TElemType ) ) {
        Status status = PostTraverse( T, Visit );
        printf( "\n" );

        return status;
}

/*
 * 层序遍历
 */
Status BiTree_LevelOrderTraverse( BiTree T, Status ( *Visit )( TElemType ) ) {
        // 二叉树为空
        if ( BiTree_IsEmpty( T ) ) {
                printf( "\n" );
                return OK;
        }

        BiTree e;

        // 借助队列实现层序遍历
        LinkQueue Q;
        LinkQueue_InitQueue( &Q );
        // 根指针入队
        LinkQueue_EnQueue( &Q, T );

        // 一直循环，直到队列为空
        while ( !LinkQueue_IsEmpty( Q ) ) {

                LinkQueue_DeQueue( &Q, &e );

                // 访问元素
                if ( !Visit( e->data ) ) {
                        return ERROR;
                }

                // 左孩子入队
                if ( e->lchild != nullptr ) {
                        LinkQueue_EnQueue( &Q, e->lchild );
                }

                // 右孩子入队
                if ( e->rchild != nullptr ) {
                        LinkQueue_EnQueue( &Q, e->rchild );
                }
        }
        LinkQueue_DestroyQueue( &Q );

        printf( "\n" );

        return OK;
}

/*
 * Morris遍历
 *
 * 思路: 以某个根节点开始,找到其左子树最右侧节点后,与这个根节点进行连接
 *
 * https://www.cnblogs.com/AnnieKim/archive/2013/06/15/morristraversal.html
 */
Status BiTree_MorrisPreOrderTraverse( BiTree T,
                                      Status ( *Visit )( TElemType ) ) {
        BiTNode *curr = T;
        BiTNode *mostRight = nullptr; // [左子树最右结点]

        while ( curr != nullptr ) {
                if ( curr->lchild == nullptr ) { // 1. 没有左子树
                        if ( !Visit( curr->data ) ) {
                                return ERROR;
                        }
                        curr = curr->rchild; // 向右子树移动
                } else {                     // 2. 有左子树
                        mostRight = curr->lchild;
                        while ( mostRight->rchild != nullptr &&
                                mostRight->rchild != curr ) {
                                mostRight =
                                    mostRight->rchild; // 找到 [左子树最右结点]
                        }
                        if ( mostRight->rchild ==
                             nullptr ) { // a. 左子树最右结点 为空
                                if ( !Visit( curr->data ) ) {
                                        return ERROR;
                                }
                                mostRight->rchild = curr; // 建立连接
                                curr = curr->lchild;      // curr往左走
                        } else { // b. 左子树最右结点 不为空
                                mostRight->rchild = nullptr; // 断开连接
                                curr = curr->rchild;         // curr往右走
                        }
                }
        }

        return OK;
}

Status BiTree_MorrisInOrderTraverse( BiTree T,
                                     Status ( *Visit )( TElemType ) ) {
        BiTNode *curr = T;
        BiTNode *mostRight = nullptr;

        while ( curr != nullptr ) {
                if ( curr->lchild == nullptr ) {
                        if ( !Visit( curr->data ) ) {
                                return ERROR;
                        }
                        curr = curr->rchild;
                } else {
                        mostRight = curr->lchild;
                        while ( mostRight->rchild != nullptr &&
                                mostRight->rchild != curr ) {
                                mostRight = mostRight->rchild;
                        }

                        if ( mostRight->rchild == nullptr ) {
                                mostRight->rchild = curr;
                                curr = curr->lchild;
                        } else {
                                mostRight->rchild = nullptr;
                                if ( !Visit( curr->data ) ) {
                                        return ERROR;
                                }
                                curr = curr->rchild;
                        }
                }
        }

        return OK;
}

static void __Reverse( BiTNode *from,
                       BiTNode *to ) // reverse the tree nodes 'from' -> 'to'.
{
        if ( from == to ) {
                return;
        }

        BiTNode *x = from, *y = from->rchild, *z;
        while ( true ) {
                z = y->rchild;
                y->rchild = x;
                x = y;
                y = z;
                if ( x == to ) {
                        break;
                }
        }
}

static void __PrintReverse( BiTNode *from, BiTNode *to,
                            Status ( *Visit )( TElemType ) ) {
        __Reverse( from, to );

        BiTNode *p = to;
        while ( true ) {
                Visit( p->data );
                if ( p == from ) {
                        break;
                }
                p = p->rchild;
        }

        __Reverse( to, from );
}

Status BiTree_MorrisPostOrderTraverse( BiTree T,
                                       Status ( *Visit )( TElemType ) ) {
        BiTNode dump = { .lchild = T };

        BiTNode *curr = &dump, *mostRight = nullptr;
        while ( curr ) {
                if ( curr->lchild == nullptr ) {
                        curr = curr->rchild;
                } else {
                        mostRight = curr->lchild;
                        while ( mostRight->rchild != nullptr &&
                                mostRight->rchild != curr ) {
                                mostRight = mostRight->rchild;
                        }

                        if ( mostRight->rchild == nullptr ) {
                                mostRight->rchild = curr;
                                curr = curr->lchild;
                        } else { // 能到达两次的结点 第二次到达时
                                 // 逆序打印左子树右边界
                                __PrintReverse( curr->lchild, mostRight,
                                                Visit );
                                mostRight->rchild = nullptr;
                                curr = curr->rchild;
                        }
                }
        }

        return OK;
}

BiTNode *BiTree_LowestCommonAncestor( BiTree T, BiTNode *p, BiTNode *q ) {
        if ( T == nullptr ) {
                return nullptr;
        }
        if ( p == T || q == T ) {
                return T;
        }

        BiTNode *left = BiTree_LowestCommonAncestor( T->lchild, p, q );
        BiTNode *right = BiTree_LowestCommonAncestor( T->rchild, p, q );

        if ( left != nullptr && right != nullptr ) {
                return T;
        }
        return left != nullptr ? left : right;
}

TElemType BiTree_LowestCommonAncestorByValue( BiTree T, TElemType e1,
                                              TElemType e2 ) {
        if ( BiTree_IsEmpty( T ) ) {
                return ERROR;
        }

        BiTNode *p = EPtr( T, e1 );
        BiTNode *q = EPtr( T, e2 );
        BiTNode *ancestor = BiTree_LowestCommonAncestor( T, p, q );

        return ancestor->data;
}

BiTree BiTree_InvertTree( BiTree T ) {
        if ( BiTree_IsEmpty( T ) ) {
                return nullptr;
        }

        BiTree p = T;

        BiTNode *tmp = p->rchild;
        p->rchild = p->lchild;
        p->lchild = tmp;

        BiTree_InvertTree( p->lchild );
        BiTree_InvertTree( p->rchild );

        return T;
}

bool BiTree_IsCompleteBiTree( BiTree T ) {
        LinkQueue Q;
        LinkQueue_InitQueue( &Q );
        LinkQueue_EnQueue( &Q, T );

        BiTree p = nullptr;

        while ( !LinkQueue_IsEmpty( Q ) ) {
                LinkQueue_DeQueue( &Q, &p );
                if ( p == nullptr ) {
                        break;
                }
                LinkQueue_EnQueue( &Q, p->lchild );
                LinkQueue_EnQueue( &Q, p->rchild );
        }

        while ( !LinkQueue_IsEmpty( Q ) ) {
                LinkQueue_DeQueue( &Q, &p );
                if ( p != nullptr ) {
                        LinkQueue_DestroyQueue( &Q );
                        return false;
                }
        }

        LinkQueue_DestroyQueue( &Q );
        return true;
}

/*━━━━━━━━━━━━━━━━━━━━━━ 图形化输出 ━━━━━━━━━━━━━━━━━━━━━━*/

// 以图形化形式输出当前结构，仅限内部测试使用
void PrintTree( BiTree T ) {
        if ( BiTree_IsEmpty( T ) ) {
                printf( "\n" );
                return;
        }

        int begin;
        int distance;

        int level = BiTree_GetDepthNoneRecursive( T ); // （完全）二叉树结构高度
        int width = ( int )pow( 2, level ) - 1;        // （完全）二叉树结构宽度

        // 动态创建行
        TElemType **tmp = malloc( level * sizeof( TElemType * ) );

        // 动态创建列
        for ( int i = 0; i < level; ++i ) {
                tmp[ i ] = malloc( width * sizeof( TElemType ) );

                // 初始化内存值为空字符
                memset( tmp[ i ], '\0', width );
        }

        // 借助队列实现层序遍历
        LinkQueue Q;
        LinkQueue_InitQueue( &Q );
        LinkQueue_EnQueue( &Q, T );

        BiTree e;
        // 遍历树中所有元素，将其安排到二维数组tmp中合适的位置
        for ( int i = 0; i < level; ++i ) {
                int w = ( int )pow( 2, i ); // 二叉树当前层的宽度
                distance = width / w;       // 二叉树当前层的元素间隔
                begin =
                    width /
                    ( int )pow( 2, i + 1 ); // 二叉树当前层首个元素之前的空格数

                for ( int k = 0; k < w; ++k ) {
                        LinkQueue_DeQueue( &Q, &e );

                        if ( e == nullptr ) {
                                LinkQueue_EnQueue( &Q, nullptr );
                                LinkQueue_EnQueue( &Q, nullptr );
                        } else {
                                int j = begin + k * ( 1 + distance );
                                tmp[ i ][ j ] = e->data;

                                // 左孩子入队
                                LinkQueue_EnQueue( &Q, e->lchild );

                                // 右孩子入队
                                LinkQueue_EnQueue( &Q, e->rchild );
                        }
                }
        }

        for ( int i = 0; i < level; ++i ) {
                for ( int j = 0; j < width; ++j ) {
                        if ( tmp[ i ][ j ] != '\0' ) {
                                printf( "%c", tmp[ i ][ j ] );
                        } else {
                                printf( " " );
                        }
                }
                printf( "\n" );
        }

        LinkQueue_DestroyQueue( &Q );
}
