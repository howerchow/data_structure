#include "Queue.h"

// 测试函数，打印整型
void PrintElem( ElemType e ) {
        printf( "%d ", e );
}


int main( int argc, char *argv[] ) {
        Queue Q;
        Queue_InitQueue( &Q );

        ElemType e;

        for ( int i = 0; i < 10; ++i ) {
                Queue_EnQueue( &Q, 2 * i );
        }
        SqStack_StackTraverse( Q.S_IN, PrintElem );

        for ( int i = 0; i < 10; ++i ) {
                Queue_DeQueue( &Q, &e );
                printf( "%d ", e );
        }
	printf("\n");

        Queue_DestroyQueue( &Q );

        return 0;
}
