#include "Queue.h"
#include "SqStack.h"

Status Queue_InitQueue( Queue *Q ) {

        SqStack_InitStack( &Q->S_IN );
        SqStack_InitStack( &Q->S_OUT );

        return OK;
}

Status Queue_DestroyQueue( Queue *Q ) {

        SqStack_DestroyStack( &Q->S_IN );
        SqStack_DestroyStack( &Q->S_OUT );

        return OK;
}

Status Queue_EnQueue( Queue *Q, ElemType e ) {
        if ( Q == nullptr ) {
                return ERROR;
        }
        SqStack_Push( &Q->S_IN, e );

        return OK;
}

Status Queue_DeQueue( Queue *Q, ElemType *e ) {
        if ( SqStack_StackEmpty( Q->S_OUT ) ) {
                ElemType e1;
                while ( !SqStack_StackEmpty( Q->S_IN ) ) {
                        SqStack_Pop( &Q->S_IN, &e1 );
                        SqStack_Push( &Q->S_OUT, e1 );
                }
        }
        SqStack_Pop( &Q->S_OUT, e );

        return OK;
}
