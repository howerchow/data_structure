/*=========================
 * 两个栈模拟队列
 ==========================*/

#pragma once

#include "SqStack.h"
#include <stdio.h>

/* 队列元素类型定义 */
typedef int ElemType;

// 队列元素结构
typedef struct Queue {
        SqStack S_IN;
        SqStack S_OUT;
} Queue;

Status Queue_InitQueue( Queue *Q );
Status Queue_DestroyQueue( Queue *Q );

Status Queue_EnQueue( Queue *Q, ElemType e );
Status Queue_DeQueue( Queue *Q, ElemType *e );

// Status QueueTraverse( SqStack S, void ( *Visit )( SElemType ) );
