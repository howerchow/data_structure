/*=====================
 * 双端队列(双向链表实现)
 *
 * 包含算法: 3.10
 ======================*/

#pragma once

#include "Status.h" //**▲01 绪论**//

/* 双端队列元素类型定义 */

typedef int DeQue_ElemType;

/*
 * 双端队列链表结构
 *
 * 注：这里的双端队列链表存在头结点
 */
typedef struct DeLNode {
        DeQue_ElemType data;
        struct DeLNode *prior; // 前驱
        struct DeLNode *next;  // 后继
} DeLNode;

typedef struct DeQueue {
        size_t size;    // 元素个数
        DeLNode *front; // 队头指针
        DeLNode *rear;  // 队尾指针
} DeQueue;              // 双端队列的链式存储表示

Status DeQueue_InitQueue( DeQueue *Q );
Status DeQueue_DestroyQueue( DeQueue *Q );
Status DeQueue_ClearQueue( DeQueue *Q );

Status DeQueue_PushFront( DeQueue *Q, DeQue_ElemType e );
Status DeQueue_PushBack( DeQueue *Q, DeQue_ElemType e );

Status DeQueue_PopFront( DeQueue *Q, DeQue_ElemType *e );
Status DeQueue_PopBack( DeQueue *Q, DeQue_ElemType *e );

DeQue_ElemType DeQueue_PeekFirst( DeQueue *Q );
DeQue_ElemType DeQueue_PeekLast( DeQueue *Q );

bool DeQueue_IsEmpty( DeQueue Q );
size_t DeQueue_GetLength( DeQueue Q );

/*
 * 遍历
 *
 * 用visit函数访问队列Q
 */
Status DeQueue_TraverseQueue( DeQueue Q, void ( *Visit )( DeQue_ElemType ) );
