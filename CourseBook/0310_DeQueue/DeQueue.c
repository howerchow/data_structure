#include "DeQueue.h"
#include <stdio.h>
#include <stdlib.h> // malloc、realloc、free、exit

Status DeQueue_InitQueue( DeQueue *Q ) {
        if ( Q == nullptr ) {
                return ERROR;
        }

        Q->front = Q->rear = malloc( sizeof( DeLNode ) );
        Q->front->prior = Q->front->next = nullptr;
        Q->size = 0;

        return OK;
}

/*
 * 销毁(结构)
 *
 * 释放链队所占内存(包括头结点)
 */
Status DeQueue_DestroyQueue( DeQueue *Q ) {
        if ( Q == nullptr ) {
                return ERROR;
        }

        while ( Q->front ) {
                Q->rear = Q->front->next;
                free( Q->front );
                Q->front = Q->rear;
        }

        return OK;
}

/*
 * 置空(内容)
 *
 * 这里需要释放链队中非头结点处的空间。
 */
Status DeQueue_ClearQueue( DeQueue *Q ) {
        if ( Q == nullptr ) {
                return ERROR;
        }

        Q->rear = Q->front->next;

        while ( Q->rear ) {
                Q->front->next = Q->rear->next;
                free( Q->rear );
                Q->rear = Q->front->next;
        }

        Q->rear = Q->front;
        Q->size = 0;

        return OK;
}

Status DeQueue_PushFront( DeQueue *Q, DeQue_ElemType e ) {
        if ( Q == nullptr ) {
                return ERROR;
        }

        DeLNode *p = malloc( sizeof( DeLNode ) );
        if ( p == nullptr ) {
                exit( EXIT_FAILURE );
        }
        p->data = e;
        p->next = p->prior = nullptr;

        if ( Q->front->next == nullptr ) {
                Q->front->next = p;
                p->prior = Q->front;
                Q->rear = p;
        } else {
                p->next = Q->front->next;
                Q->front->next->prior = p;

                p->prior = Q->front;
                Q->front->next = p;
        }

        ++Q->size;

        return OK;
}

Status DeQueue_PushBack( DeQueue *Q, DeQue_ElemType e ) {
        if ( Q == nullptr ) {
                return ERROR;
        }

        DeLNode *p = malloc( sizeof( DeLNode ) );
        if ( p == nullptr ) {
                exit( EXIT_FAILURE );
        }
        p->data = e;
        p->prior = p->next = nullptr;

        p->prior = Q->rear;
        Q->rear->next = p;

        Q->rear = p;

        ++Q->size;

        return OK;
}

Status DeQueue_PopFront( DeQueue *Q, DeQue_ElemType *e ) {
        if ( Q == nullptr || Q->front->next == nullptr ) {
                return ERROR;
        }

        DeLNode *p = Q->front->next;
        *e = p->data;
        Q->front->next = p->next;
        if ( p->next != nullptr ) {
                p->next->prior = Q->front;
        } else {
                Q->rear = Q->front;
        }

        free( p );

        --Q->size;

        return OK;
}

Status DeQueue_PopBack( DeQueue *Q, DeQue_ElemType *e ) {
        if ( Q == nullptr || Q->rear == nullptr ) {
                return ERROR;
        }

        DeLNode *p = Q->rear;
        *e = p->data;
        Q->rear = Q->rear->prior;
        Q->rear->next = nullptr;
        free( p );

        --Q->size;

        return OK;
}

DeQue_ElemType DeQueue_PeekFirst( DeQueue *Q ) {
        if ( Q != nullptr && Q->front->next != nullptr ) {
                return Q->front->next->data;
        }
}

DeQue_ElemType DeQueue_PeekLast( DeQueue *Q ) {
        if ( Q != nullptr && Q->rear != nullptr ) {
                return Q->rear->data;
        }
}

bool DeQueue_IsEmpty( DeQueue Q ) {
        if ( Q.front == Q.rear ) {
                return true;
        }
        return false;
}

size_t DeQueue_GetLength( DeQueue Q ) {
        return Q.size;
}

Status DeQueue_TraverseQueue( DeQueue Q, void ( *Visit )( DeQue_ElemType ) ) {
        if ( Q.front == nullptr ) {
                return ERROR;
        }

        DeLNode *p = Q.front->next;

        while ( p != nullptr ) {
                Visit( p->data );
                p = p->next;
        }

        printf( "\n" );

        return OK;
}
