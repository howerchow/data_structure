#include "DeQueue.h"
#include "SqList.h"
#include <stdio.h>

// 测试函数，打印整型
void PrintElem( SqList_ElemType e ) {
        printf( "%d ", e );
}

// 滑动窗口 窗口內最大值
Status GetMaxWindow( const SqList *L, const int w, SqList *rL ) {
        if ( L == nullptr || w < 1 || L->length < w ) {
                return ERROR;
        }
        int index = 0;
        DeQue_ElemType e;

        DeQueue QMax;
        DeQueue_InitQueue( &QMax );

        // int res[ L->length - w + 1 ];

        for ( int i = 0; i < L->length; ++i ) {
                while ( !DeQueue_IsEmpty( QMax ) &&
                        L->elem[ DeQueue_PeekLast( &QMax ) ] <= L->elem[ i ] ) {
                        DeQueue_PopBack( &QMax, &e );
                }
                DeQueue_PushBack( &QMax, i );
                if ( DeQueue_PeekFirst( &QMax ) == i - w ) {
                        DeQueue_PopFront( &QMax, &e );
                }
                if ( i >= w - 1 ) {
                        SqList_ListInsert( rL, index++,
                            L->elem[ DeQueue_PeekFirst( &QMax ) ] );
                }
        }

        DeQueue_DestroyQueue( &QMax );

        return OK;
}

int main( int argc, char *argv[] ) {

        SqList L;
        SqList_InitList( &L );

        SqList_ListInsert( &L, 0, 4 );
        SqList_ListInsert( &L, 1, 3 );
        SqList_ListInsert( &L, 2, 5 );
        SqList_ListInsert( &L, 3, 4 );
        SqList_ListInsert( &L, 4, 3 );
        SqList_ListInsert( &L, 5, 3 );
        SqList_ListInsert( &L, 6, 6 );
        SqList_ListInsert( &L, 7, 7 );
        SqList_ListInsert( &L, 8, 8 );
        SqList_ListTraverse( L, PrintElem );

        int w = 3;

        SqList rL;
        SqList_InitList( &rL );

        GetMaxWindow( &L, w, &rL );

        SqList_ListTraverse( rL, PrintElem ); // 5 5 5 4 6 7 8

        SqList_DestroyList( &L );
        SqList_DestroyList( &rL );

        return 0;
}
