#include "DeQueue.h"
#include <stdio.h>

// 测试函数，打印整型
void PrintElem( DeQue_ElemType e ) {
        printf( "%d ", e );
}

int main( int argc, char *argv[] ) {
        DeQueue Q;
        DeQue_ElemType e;

        printf( "████████ 函数 DeQueue_InitQueue \n" );
        {
                printf( "█ 初始化双端队列 Q ...\n" );
                DeQueue_InitQueue( &Q );
        }
        PressEnterToContinue( debug );

        printf( "████████ 函数 DeQueue_IsEmpty \n" );
        {
                DeQueue_IsEmpty( Q ) ? printf( "█ Q 为空！！\n" )
                                     : printf( "█ Q 不为空！\n" );
        }
        PressEnterToContinue( debug );

        printf( "████████ 函数 DeQueue_PushBack \n" );
        {
                for ( int i = 1; i <= 6; ++i ) {
                        DeQueue_PushBack( &Q, 2 * i );
                        printf( "█ 元素 \"%2d\" 入队...\n", 2 * i );
                }
        }
        PressEnterToContinue( debug );

        printf( "████████ 函数 DeQueue_PushFront \n" );
        {
                for ( int i = 1; i <= 6; ++i ) {
                        DeQueue_PushFront( &Q, 2 * i + 1 );
                        printf( "█ 元素 \"%2d\" 入队...\n", 2 * i + 1 );
                }
        }
        PressEnterToContinue( debug );

        printf( "████████ 函数 DeQueue_TraverseQueue \n" );
        {
                printf( "█ Q 中的元素为：Q = " );
                DeQueue_TraverseQueue( Q, PrintElem );
        }
        PressEnterToContinue( debug );

        printf( "████████ 函数 QueueLength \n" );
        {
                size_t len = DeQueue_GetLength( Q );
                printf( "█ Q 的长度为 %zu \n", len );
        }
        PressEnterToContinue( debug );

        printf( "████████ 函数 DeQueue_PopFront \n" );
        {
                DeQueue_PopFront( &Q, &e );
                printf( "█ 队头元素 \"%d\" 出队...\n", e );
                printf( "█ Q 中的元素为：Q = " );
                DeQueue_TraverseQueue( Q, PrintElem );
        }
        PressEnterToContinue( debug );

        printf( "████████ 函数 DeQueue_PopBack \n" );
        {
                DeQueue_PopBack( &Q, &e );
                printf( "█ 队尾元素 \"%d\" 出队...\n", e );
                printf( "█ Q 中的元素为：Q = " );
                DeQueue_TraverseQueue( Q, PrintElem );
        }
        PressEnterToContinue( debug );

        printf( "████████ 函数 DeQueue_PopBack DeQueue_PopFront \n" );
        {
                printf( "█ DeQueue_PopFront: " );
                for ( size_t i = 0; i < 5; ++i ) {
                        DeQueue_PopFront( &Q, &e );
                        printf( "%d ", e );
                }
                printf( "\n" );

                printf( "█ DeQueue_PopBack: " );
                for ( size_t i = 0; i < 4; ++i ) {
                        DeQueue_PopBack( &Q, &e );
                        printf( "%d ", e );
                }
                printf( "\n" );
        }
        PressEnterToContinue( debug );

        printf( "████████ 函数 DeQueue_PeekFirst DeQueue_PeekLast \n" );
        {
                printf( "█ DeQueue_PeekFirst: %d \n", DeQueue_PeekFirst( &Q ) );
                printf( "█ DeQueue_PeekLast: %d \n", DeQueue_PeekLast( &Q ) );
        }
        PressEnterToContinue( debug );

        printf( "████████ 函数 ClearQueue \n" );
        {
                printf( "█ 清空 Q 前：" );
                DeQueue_IsEmpty( Q ) ? printf( " Q 为空！！\n" )
                                     : printf( " Q 不为空！\n" );

                DeQueue_ClearQueue( &Q );

                printf( "█ 清空 Q 后：" );
                DeQueue_IsEmpty( Q ) ? printf( " Q 为空！！\n" )
                                     : printf( " Q 不为空！\n" );
        }
        PressEnterToContinue( debug );

        printf( "████████ 函数 DestroyQueue \n" );
        {
                printf( "█ 销毁 Q 前：" );
                Q.front != nullptr &&Q.rear != nullptr
                    ? printf( " Q 存在！\n" )
                    : printf( " Q 不存在！！\n" );

                DeQueue_DestroyQueue( &Q );

                printf( "█ 销毁 Q 后：" );
                Q.front != nullptr &&Q.rear != nullptr
                    ? printf( " Q 存在！\n" )
                    : printf( " Q 不存在！！\n" );
        }
        PressEnterToContinue( debug );
        return 0;
}
