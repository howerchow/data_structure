/***************************
 *                         *
 * 文件夹: ▲09 查找       *
 *                         *
 * 文件名: RBTree.h        *
 *                         *
 * 内  容: RB树相关操作列表 *
 *                         *
 ***************************/
#ifndef RBTREE_H
#define RBTREE_H

#include "Base.h"

typedef int TKeyType;

typedef enum TColorType {
        RED = 0,   // 红色节点
        BLACK = 1, // 黑色节点
} TColorType;

// RB树节点
typedef struct RBTreeNode {
        TKeyType key; // 关键字(键值)
        TColorType color;
        struct RBTreeNode *lchild; // 左孩子
        struct RBTreeNode *rchild; // 右孩子
        struct RBTreeNode *parent; // 父结点
} RBTreeNode;

// RB树
typedef struct RBTree {
        RBTreeNode *root;
} RBTree;

RBTree *CreateRBTree( Table Ta );
/*━━━━━━━┓
┃(01)创建RB树。 ┃
┗━━━━━━━*/

void DestroyRBTree( RBTree *T );
/*━━━━━━━┓
┃(02)销毁RB树。 ┃
┗━━━━━━━*/

Status InsertRBTree( RBTree *T, TKeyType key );
/*━━━━━━━┓
┃(03)插入结点。 ┃
┗━━━━━━━*/

void DeleteRBTree( RBTree *T, TKeyType key );
/*━━━━━━━┓
┃(04) 删除结点 ┃
┗━━━━━━━*/

Status SearchRBTree( RBTree *T, TKeyType key );
/*━━━━━━━━━━━┓
┃(05) 查找结点(递归) ┃
┗━━━━━━━━━━━*/

Status SearchRBTreeIterative( RBTree *T, TKeyType key );
/*━━━━━━━━━━━┓
┃(06) 查找结点(非递归) ┃
┗━━━━━━━━━━━*/

Status MinRBTree( RBTree *T, int *val );
/*━━━━━━━━━━━┓
┃(07) 最小结点 ┃
┗━━━━━━━━━━━*/

Status MaxRBTree( RBTree *T, int *val );
/*━━━━━━━━━━━┓
┃(08) 最大结点 ┃
┗━━━━━━━━━━━*/

void PrintRBT_PreOreder( RBTree *T );
/*━━━━━━━━━━━┓
┃(09) 前序遍历 ┃
┗━━━━━━━━━━━*/

void PrintRBT_InOreder( RBTree *T );
/*━━━━━━━━━━━┓
┃(10) 中序遍历 ┃
┗━━━━━━━━━━━*/

void PrintRBT_PostOreder( RBTree *T );
/*━━━━━━━━━━━┓
┃(11) 后序遍历 ┃
┗━━━━━━━━━━━*/

/* 打印红黑树 */
void Print_RBTree( RBTree *T );

#endif
