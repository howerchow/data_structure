/***************************
 *                         *
 * 文件夹: ▲09 查找       *
 *                         *
 * 文件名: RBTree.c        *
 *                         *
 * 内  容: RB树相关操作实现 *
 *                         *
 * @author skywang         *
 * @date 2013/11/18        *
 *                         *
 * @modified Hower Chow    *
 * @date 2023/10/13        *
 *                         *
 ***************************/
#include "RBTree.h"
#include "Status.h"
#include <stdio.h>
#include <stdlib.h>

// 删去macro
/* #define rb_parent( r ) ( ( r )->parent ) */

static inline RBTreeNode* rb_parent(RBTreeNode *r) {
  if(r != nullptr) {
     return r->parent;
  }
  return nullptr;
}


/* TODO: 改写为inline函数 */
#define rb_color( r ) ( ( r )->color )
#define rb_is_red( r ) ( ( r )->color == RED )
#define rb_is_black( r ) ( ( r )->color == BLACK )
#define rb_set_black( r )                                                      \
        do {                                                                   \
                ( r )->color = BLACK;                                          \
        } while ( 0 )
#define rb_set_red( r )                                                        \
        do {                                                                   \
                ( r )->color = RED;                                            \
        } while ( 0 )
#define rb_set_parent( r, p )                                                  \
        do {                                                                   \
                ( r )->parent = ( p );                                         \
        } while ( 0 )
#define rb_set_color( r, c )                                                   \
        do {                                                                   \
                ( r )->color = ( c );                                          \
        } while ( 0 )

/*
 * 创建红黑树
 */
RBTree *CreateRBTree( Table Ta ) {
        RBTree *T = malloc( sizeof( RBTree ) );
        if ( T == nullptr ) {
                return nullptr;
        }
        T->root = nullptr;

        if ( Ta.length > 0 ) {
                for ( size_t i = 1; i <= Ta.length; ++i ) {
                        if ( InsertRBTree( T, Ta.elem[ i ].key ) !=
                             OK ) // 插入失败则跳出循环
                                break;
                }
        }

        return T;
}

/*
 * 前序遍历"红黑树"
 */
static void preorder( RBTreeNode *root ) {
        if ( root != nullptr ) {
                printf( "%d ", root->key );
                preorder( root->lchild );
                preorder( root->rchild );
        }
}
void PrintRBT_PreOreder( RBTree *T ) {
        if ( T != nullptr )
                preorder( T->root );
}

/*
 * 中序遍历"红黑树"
 */
static void inorder( RBTreeNode *root ) {
        if ( root != nullptr ) {
                inorder( root->lchild );
                printf( "%d ", root->key );
                inorder( root->rchild );
        }
}

void PrintRBT_InOreder( RBTree *T ) {
        if ( T != nullptr )
                inorder( T->root );
}

/*
 * 后序遍历"红黑树"
 */
static void postorder( RBTreeNode *root ) {
        if ( root != nullptr ) {
                postorder( root->lchild );
                postorder( root->rchild );
                printf( "%d ", root->key );
        }
}

void PrintRBT_PostOreder( RBTree *T ) {
        if ( T != nullptr )
                postorder( T->root );
}

/*
 * (递归实现)查找"红黑树root"中键值为key的节点
 */
static RBTreeNode *search( RBTreeNode *root, TKeyType key ) {
        RBTreeNode *node = root;
        if ( node == nullptr || node->key == key )
                return node;

        if ( key < node->key )
                return search( node->lchild, key );
        else
                return search( node->rchild, key );
}
Status SearchRBTree( RBTree *T, TKeyType key ) {
        if ( T != nullptr )
                return search( T->root, key ) ? OK : ERROR;
}

/*
 * (非递归实现)查找"红黑树root"中键值为key的节点
 */
static RBTreeNode *iterative_search( RBTreeNode *root, TKeyType key ) {
        RBTreeNode *node = root;
        while ( ( node != nullptr ) && ( node->key != key ) ) {
                if ( key < node->key )
                        node = node->lchild;
                else
                        node = node->rchild;
        }

        return node;
}
Status SearchRBTreeIterative( RBTree *T, TKeyType key ) {
        if ( T != nullptr )
                return iterative_search( T->root, key ) ? OK : ERROR;
}

/*
 * 查找最小结点：返回tree为根结点的红黑树的最小结点。
 */
static RBTreeNode *minimum( RBTreeNode *root ) {
        RBTreeNode *node = root;
        if ( node == nullptr )
                return nullptr;

        while ( node->lchild != nullptr )
                node = node->lchild;
        return node;
}

Status MinRBTree( RBTree *T, int *val ) {
        RBTreeNode *node;

        if ( T != nullptr )
                node = minimum( T->root );

        if ( node == nullptr )
                return ERROR;

        *val = node->key;
        return OK;
}

/*
 * 查找最大结点：返回tree为根结点的红黑树的最大结点。
 */
static RBTreeNode *maximum( RBTreeNode *root ) {

        RBTreeNode *node = root;
        if ( node == nullptr )
                return nullptr;

        while ( node->rchild != nullptr )
                node = node->rchild;
        return node;
}

Status MaxRBTree( RBTree *T, int *val ) {
        RBTreeNode *node;

        if ( T != nullptr )
                node = maximum( T->root );

        if ( node == nullptr )
                return ERROR;

        *val = node->key;
        return OK;
}

/*
 * 找结点(x)的后继结点。即，查找"红黑树中数据值大于该结点"的"最小结点"。
 */
static RBTreeNode *rbtree_successor( RBTreeNode *x ) {
        // 如果x存在右孩子，则"x的后继结点"为 "以其右孩子为根的子树的最小结点"。
        if ( x->rchild != nullptr )
                return minimum( x->rchild );

        // 如果x没有右孩子。则x有以下两种可能：
        // (01) x是"一个左孩子"，则"x的后继结点"为 "它的父结点"。
        // (02)
        // x是"一个右孩子"，则查找"x的最低的父结点，并且该父结点要具有左孩子"，找到的这个"最低的父结点"就是"x的后继结点"。
        RBTreeNode *y = x->parent;
        while ( ( y != nullptr ) && ( x == y->rchild ) ) {
                x = y;
                y = y->parent;
        }

        return y;
}

/*
 * 找结点(x)的前驱结点。即，查找"红黑树中数据值小于该结点"的"最大结点"。
 */
static RBTreeNode *rbtree_predecessor( RBTreeNode *x ) {
        // 如果x存在左孩子，则"x的前驱结点"为 "以其左孩子为根的子树的最大结点"。
        if ( x->lchild != nullptr )
                return maximum( x->lchild );

        // 如果x没有左孩子。则x有以下两种可能：
        // (01) x是"一个右孩子"，则"x的前驱结点"为 "它的父结点"。
        // (01)
        // x是"一个左孩子"，则查找"x的最低的父结点，并且该父结点要具有右孩子"，找到的这个"最低的父结点"就是"x的前驱结点"。
        RBTreeNode *y = x->parent;
        while ( ( y != nullptr ) && ( x == y->lchild ) ) {
                x = y;
                y = y->parent;
        }

        return y;
}

/*
 * 对红黑树的节点(x)进行左旋转
 *
 * 左旋示意图(对节点x进行左旋)：
 *      px                              px
 *     /                               /
 *    x                               y
 *   /  \      --(左旋)-->           / \
 *  lx   y                          x  ry
 *     /   \                       /  \
 *    ly   ry                     lx  ly
 *
 *
 */
static void rbtree_lchild_rotate( RBTree *T, RBTreeNode *x ) {
        // 设置x的右孩子为y
        RBTreeNode *y = x->rchild;

        // 将 “y的左孩子” 设为 “x的右孩子”；
        // 如果y的左孩子非空，将 “x” 设为 “y的左孩子的父亲”
        x->rchild = y->lchild;
        if ( y->lchild != nullptr )
                y->lchild->parent = x;

        // 将 “x的父亲” 设为 “y的父亲”
        y->parent = x->parent;

        if ( x->parent == nullptr ) {
                // tree = y;			// 如果 “x的父亲”
                // 是空节点，则将y设为根节点
                T->root = y; // 如果 “x的父亲” 是空节点，则将y设为根节点
        } else {
                if ( x->parent->lchild == x )
                        x->parent->lchild =
                            y; // 如果
                               // x是它父节点的左孩子，则将y设为“x的父节点的左孩子”
                else
                        x->parent->rchild =
                            y; // 如果
                               // x是它父节点的左孩子，则将y设为“x的父节点的左孩子”
        }

        // 将 “x” 设为 “y的左孩子”
        y->lchild = x;
        // 将 “x的父节点” 设为 “y”
        x->parent = y;
}

/*
 * 对红黑树的节点(y)进行右旋转
 *
 * 右旋示意图(对节点y进行左旋)：
 *            py                               py
 *           /                                /
 *          y                                x
 *         /  \      --(右旋)-->            /  \                     #
 *        x   ry                           lx   y
 *       / \                                   / \                   #
 *      lx  rx                                rx  ry
 *
 */
static void rbtree_rchild_rotate( RBTree *T, RBTreeNode *y ) {
        // 设置x是当前节点的左孩子。
        RBTreeNode *x = y->lchild;

        // 将 “x的右孩子” 设为 “y的左孩子”；
        // 如果"x的右孩子"不为空的话，将 “y” 设为 “x的右孩子的父亲”
        y->lchild = x->rchild;
        if ( x->rchild != nullptr )
                x->rchild->parent = y;

        // 将 “y的父亲” 设为 “x的父亲”
        x->parent = y->parent;

        if ( y->parent == nullptr ) {
                // tree = x;	        // 如果 “y的父亲”
                // 是空节点，则将x设为根节点
                T->root = x; // 如果 “y的父亲” 是空节点，则将x设为根节点
        } else {
                if ( y == y->parent->rchild )
                        y->parent->rchild =
                            x; // 如果
                               // y是它父节点的右孩子，则将x设为“y的父节点的右孩子”
                else
                        y->parent->lchild = x; // (y是它父节点的左孩子)
                                               // 将x设为“x的父节点的左孩子”
        }

        // 将 “y” 设为 “x的右孩子”
        x->rchild = y;

        // 将 “y的父节点” 设为 “x”
        y->parent = x;
}

/*
 * 红黑树插入修正函数
 *
 * 在向红黑树中插入节点之后(失去平衡)，再调用该函数；
 * 目的是将它重新塑造成一颗红黑树。
 *
 * 参数说明：
 *     T 红黑树的根
 *     node 插入的结点		// 对应《算法导论》中的z
 */
static void rbtree_insert_fixup( RBTree *T, RBTreeNode *node ) {
        RBTreeNode *parent, *gparent;

        // 若“父节点存在，并且父节点的颜色是红色”
        while ( ( parent = rb_parent( node ) ) && rb_is_red( parent ) ) {
                gparent = rb_parent( parent );

                // 若“父节点”是“祖父节点的左孩子”
                if ( parent == gparent->lchild ) {
                        // Case 1条件：叔叔节点是红色
                        {
                                RBTreeNode *uncle = gparent->rchild;
                                if ( uncle && rb_is_red( uncle ) ) {
                                        rb_set_black( uncle );
                                        rb_set_black( parent );
                                        rb_set_red( gparent );
                                        node = gparent;
                                        continue;
                                }
                        }

                        // Case 2条件：叔叔是黑色，且当前节点是右孩子
                        if ( parent->rchild == node ) {
                                RBTreeNode *tmp;
                                rbtree_lchild_rotate( T, parent );
                                tmp = parent;
                                parent = node;
                                node = tmp;
                        }

                        // Case 3条件：叔叔是黑色，且当前节点是左孩子。
                        rb_set_black( parent );
                        rb_set_red( gparent );
                        rbtree_rchild_rotate( T, gparent );
                } else // 若“z的父节点”是“z的祖父节点的右孩子”
                {
                        // Case 1条件：叔叔节点是红色
                        {
                                RBTreeNode *uncle = gparent->lchild;
                                if ( uncle && rb_is_red( uncle ) ) {
                                        rb_set_black( uncle );
                                        rb_set_black( parent );
                                        rb_set_red( gparent );
                                        node = gparent;
                                        continue;
                                }
                        }

                        // Case 2条件：叔叔是黑色，且当前节点是左孩子
                        if ( parent->lchild == node ) {
                                RBTreeNode *tmp;
                                rbtree_rchild_rotate( T, parent );
                                tmp = parent;
                                parent = node;
                                node = tmp;
                        }

                        // Case 3条件：叔叔是黑色，且当前节点是右孩子。
                        rb_set_black( parent );
                        rb_set_red( gparent );
                        rbtree_lchild_rotate( T, gparent );
                }
        }

        // 将根节点设为黑色
        rb_set_black( T->root );
}

/*
 * 添加节点：将节点(node)插入到红黑树中
 *
 * 参数说明：
 *     T 红黑树的根
 *     node 插入的结点		// 对应《算法导论》中的z
 */
static void rbtree_insert( RBTree *T, RBTreeNode *node ) {
        RBTreeNode *y = nullptr;
        RBTreeNode *x = T->root;
        RBTreeNode* n_parent = rb_parent( node );

        // 1. 将红黑树当作一颗二叉查找树，将节点添加到二叉查找树中。
        while ( x != nullptr ) {
                y = x;
                if ( node->key < x->key ){
		    x = x->lchild;
		} else {
		  x = x->rchild;
		}

        }
        n_parent = y;

        if ( y != nullptr ) {
                if ( node->key < y->key )
                        y->lchild =
                            node; // 情况2：若“node所包含的值” <
                                  // “y所包含的值”，则将node设为“y的左孩子”
                else
                        y->rchild = node; // 情况3：(“node所包含的值” >=
                                          // “y所包含的值”)将node设为“y的右孩子”
        } else {
                T->root = node; // 情况1：若y是空节点，则将node设为根
        }

        // 2. 设置节点的颜色为红色
        node->color = RED;

        // 3. 将它重新修正为一颗二叉查找树
        rbtree_insert_fixup( T, node );
}

/*
 * 创建结点
 *
 * 参数说明：
 *     key 是键值。
 *     parent 是父结点。
 *     lchild 是左孩子。
 *     rchild 是右孩子。
 */
static RBTreeNode *create_rbtree_node( TKeyType key, RBTreeNode *parent,
                                       RBTreeNode *lchild,
                                       RBTreeNode *rchild ) {
        RBTreeNode *p = malloc( sizeof( RBTreeNode ) );
        if ( p == nullptr )
                return nullptr;
        p->key = key;
        p->lchild = lchild;
        p->rchild = rchild;
        p->parent = parent;
        p->color = BLACK; // 默认为黑色

        return p;
}

/*
 * 新建结点(节点键值为key)，并将其插入到红黑树中
 */

Status InsertRBTree( RBTree *T, TKeyType key ) {
        RBTreeNode *node; // 新建结点

        // 不允许插入相同键值的节点。
        // (若想允许插入相同键值的节点，注释掉下面两句话即可！)
        if ( search( T->root, key ) != nullptr )
                return ERROR;

        // 如果新建结点失败，则返回。
        if ( ( node = create_rbtree_node( key, nullptr, nullptr, nullptr ) ) == nullptr )
                return ERROR;

        rbtree_insert( T, node );

        return OK;
}

/*
 * 红黑树删除修正函数
 *
 * 在从红黑树中删除插入节点之后(红黑树失去平衡)，再调用该函数；
 * 目的是将它重新塑造成一颗红黑树。
 *
 * 参数说明：
 *     T 红黑树的根
 *     node 待修正的节点
 */
static void rbtree_delete_fixup( RBTree *T, RBTreeNode *node,
                                 RBTreeNode *parent ) {
        RBTreeNode *other;

        while ( ( !node || rb_is_black( node ) ) && node != T->root ) {
                if ( parent->lchild == node ) {
                        other = parent->rchild;
                        if ( rb_is_red( other ) ) {
                                // Case 1: x的兄弟w是红色的
                                rb_set_black( other );
                                rb_set_red( parent );
                                rbtree_lchild_rotate( T, parent );
                                other = parent->rchild;
                        }
                        if ( ( !other->lchild ||
                               rb_is_black( other->lchild ) ) &&
                             ( !other->rchild ||
                               rb_is_black( other->rchild ) ) ) {
                                // Case 2:
                                // x的兄弟w是黑色，且w的俩个孩子也都是黑色的
                                rb_set_red( other );
                                node = parent;
                                parent = rb_parent( node );
                        } else {
                                if ( !other->rchild ||
                                     rb_is_black( other->rchild ) ) {
                                        // Case 3:
                                        // x的兄弟w是黑色的，并且w的左孩子是红色，右孩子为黑色。
                                        rb_set_black( other->lchild );
                                        rb_set_red( other );
                                        rbtree_rchild_rotate( T, other );
                                        other = parent->rchild;
                                }
                                // Case 4:
                                // x的兄弟w是黑色的；并且w的右孩子是红色的，左孩子任意颜色。
                                rb_set_color( other, rb_color( parent ) );
                                rb_set_black( parent );
                                rb_set_black( other->rchild );
                                rbtree_lchild_rotate( T, parent );
                                node = T->root;
                                break;
                        }
                } else {
                        other = parent->lchild;
                        if ( rb_is_red( other ) ) {
                                // Case 1: x的兄弟w是红色的
                                rb_set_black( other );
                                rb_set_red( parent );
                                rbtree_rchild_rotate( T, parent );
                                other = parent->lchild;
                        }
                        if ( ( !other->lchild ||
                               rb_is_black( other->lchild ) ) &&
                             ( !other->rchild ||
                               rb_is_black( other->rchild ) ) ) {
                                // Case 2:
                                // x的兄弟w是黑色，且w的俩个孩子也都是黑色的
                                rb_set_red( other );
                                node = parent;
                                parent = rb_parent( node );
                        } else {
                                if ( !other->lchild ||
                                     rb_is_black( other->lchild ) ) {
                                        // Case 3:
                                        // x的兄弟w是黑色的，并且w的左孩子是红色，右孩子为黑色。
                                        rb_set_black( other->rchild );
                                        rb_set_red( other );
                                        rbtree_lchild_rotate( T, other );
                                        other = parent->lchild;
                                }
                                // Case 4:
                                // x的兄弟w是黑色的；并且w的右孩子是红色的，左孩子任意颜色。
                                rb_set_color( other, rb_color( parent ) );
                                rb_set_black( parent );
                                rb_set_black( other->lchild );
                                rbtree_rchild_rotate( T, parent );
                                node = T->root;
                                break;
                        }
                }
        }
        if ( node )
                rb_set_black( node );
}

/*
 * 删除结点
 *
 * 参数说明：
 *     tree 红黑树的根结点
 *     node 删除的结点
 */
void rbtree_delete( RBTree *T, RBTreeNode *node ) {
        RBTreeNode *child, *parent;
        int color;

        // 被删除节点的"左右孩子都不为空"的情况。
        if ( ( node->lchild != nullptr ) && ( node->rchild != nullptr ) ) {
                // 被删节点的后继节点。(称为"取代节点")
                // 用它来取代"被删节点"的位置，然后再将"被删节点"去掉。
                RBTreeNode *replace = node;

                // 获取后继节点
                replace = replace->rchild;
                while ( replace->lchild != nullptr )
                        replace = replace->lchild;

                // "node节点"不是根节点(只有根节点不存在父节点)
                if ( rb_parent( node ) ) {
                        if ( rb_parent( node )->lchild == node )
                                rb_parent( node )->lchild = replace;
                        else
                                rb_parent( node )->rchild = replace;
                } else
                        // "node节点"是根节点，更新根节点。
                        T->root = replace;

                // child是"取代节点"的右孩子，也是需要"调整的节点"。
                // "取代节点"肯定不存在左孩子！因为它是一个后继节点。
                child = replace->rchild;
                parent = rb_parent( replace );
                // 保存"取代节点"的颜色
                color = rb_color( replace );

                // "被删除节点"是"它的后继节点的父节点"
                if ( parent == node ) {
                        parent = replace;
                } else {
                        // child不为空
                        if ( child )
                                rb_set_parent( child, parent );
                        parent->lchild = child;

                        replace->rchild = node->rchild;
                        rb_set_parent( node->rchild, replace );
                }

                replace->parent = node->parent;
                replace->color = node->color;
                replace->lchild = node->lchild;
                node->lchild->parent = replace;

                if ( color == BLACK )
                        rbtree_delete_fixup( T, child, parent );
                free( node );

                return;
        }

        if ( node->lchild != nullptr )
                child = node->lchild;
        else
                child = node->rchild;

        parent = node->parent;
        // 保存"取代节点"的颜色
        color = node->color;

        if ( child )
                child->parent = parent;

        // "node节点"不是根节点
        if ( parent ) {
                if ( parent->lchild == node )
                        parent->lchild = child;
                else
                        parent->rchild = child;
        } else
                T->root = child;

        if ( color == BLACK )
                rbtree_delete_fixup( T, child, parent );
        free( node );
}

/*
 * 删除键值为key的结点
 *
 * 参数说明：
 *     tree 红黑树的根结点
 *     key 键值
 */
void DeleteRBTree( RBTree *T, TKeyType key ) {
        RBTreeNode *z, *node;

        if ( ( z = search( T->root, key ) ) != nullptr )
                rbtree_delete( T, z );
}

/*
 * 销毁红黑树
 */
static void rbtree_destroy( RBTreeNode *root ) {
        if ( root == nullptr )
                return;

        if ( root->lchild != nullptr )
                rbtree_destroy( root->lchild );
        if ( root->rchild != nullptr )
                rbtree_destroy( root->rchild );

        free( root );
}

void DestroyRBTree( RBTree *T ) {
        if ( T != nullptr )
                rbtree_destroy( T->root );

        free( T );
}

/*
 * 打印"红黑树"
 *
 * root       -- 红黑树的节点
 * key        -- 节点的键值
 * direction  --  0，表示该节点是根节点;
 *               -1，表示该节点是它的父结点的左孩子;
 *                1，表示该节点是它的父结点的右孩子。
 */
static void rbtree_print( RBTreeNode *root, TKeyType key, int direction ) {
        if ( root != nullptr ) {
                if ( direction == 0 ) // root是根节点
                        printf( "%2d(B) is rbtree\n", root->key );
                else // root是分支节点
                        printf( "%2d(%s) is %2d's %6s child\n", root->key,
                                rb_is_red( root ) ? "R" : "B", key,
                                direction == 1 ? "rchild" : "lchild" );

                rbtree_print( root->lchild, root->key, -1 );
                rbtree_print( root->rchild, root->key, 1 );
        }
}

void Print_RBTree( RBTree *T ) {
        if ( T != nullptr && T->root != nullptr )
                rbtree_print( T->root, T->root->key, 0 );
}
