/***************************
 *						   *
 * 文件夹: ▲09 查找       *
 * 						   *
 * 文件名: RBTree-main.c   *
 * 						   *
 * 内  容: RB树相关函数测试 *
 *                         *
 ***************************/

#include "RBTree.h"
#include <stdio.h>

#define Max 15

int main( ) {
        Table T;
        RBTree *rbtree;
        printf( "创建并输出一个查找表...\n" );
        {
                FILE *fp = fopen( "TestData_Table.txt", "r" );
                Create( fp, &T, Max );
	        Traverse( T, PrintKey );
		fclose(fp);
                printf( "\n" );
        }
        PressEnterToContinue( debug );

        printf(
            "▼1——8\n▲函数 CreateRBTree等 测试...\n" ); // 1——8.函数CreateBTree等测试
        {
                printf( "构造RB树...\n" );
                rbtree = CreateRBTree( T );
                printf( "\n" );
        }
        PressEnterToContinue( debug );

        printf(
            "▼15\n▲函数 PrintRBT_PreOreder 测试...\n" ); // 15.函数PrintRBT_PreOreder测试
        {
                printf( "★前序输出RB树关键字：" );
                PrintRBT_PreOreder( rbtree );
                printf( "\n\n" );
        }
        PressEnterToContinue( debug );

        printf(
            "▼16\n▲函数 PrintRBT_InOreder 测试...\n" ); // 16.函数PrintRBT_InOreder测试
        {
                printf( "★中序输出RB树关键字：" );
                PrintRBT_InOreder( rbtree );
                printf( "\n\n" );
        }
        PressEnterToContinue( debug );

        printf(
            "▼17\n▲函数 PrintRBT_PostOreder 测试...\n" ); // 17.函数PrintRBT_PostOreder测试
        {
                printf( "★后序输出RB树关键字：" );
                PrintRBT_PostOreder( rbtree );
                printf( "\n\n" );
        }
        PressEnterToContinue( debug );

        int i;
        if ( MinRBTree( rbtree, &i ) == 0 )
                printf( "== 最小值: %d\n", i );
        if ( MaxRBTree( rbtree, &i ) == 0 )
                printf( "== 最大值: %d\n", i );

        printf( "== 树的详细信息: \n" );
        Print_RBTree( rbtree );
        printf( "\n" );

        printf(
            "▼18\n▲函数 InsertRBTree 测试...\n" ); // 18.函数InsertRBTree/DeleteRBTree测试
        {
                printf( "★插入删除关键字：\n" );
                InsertRBTree( rbtree, 101 );
                PrintRBT_InOreder( rbtree );
                printf( "\n\n" );
                DeleteRBTree( rbtree, 37 );
                PrintRBT_InOreder( rbtree );
                printf( "\n\n" );
                Print_RBTree( rbtree );
                printf( "\n\n" );
        }
        PressEnterToContinue( debug );

        DestroyRBTree( rbtree );

        return 0;
}
