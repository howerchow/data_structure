/*==============
 * 进制转换
 *
 * 包含算法: 3.1
 ===============*/

#include "Conversion.h" //**▲03 栈和队列**//
#include <stdio.h>

/*
 * ████████ 算法3.1 ████████
 *
 * 进制转换：将指定的非负十进制整数，转换为八进制后输出。
 *
 *【注】
 * 教材使用的是控制台输入，这里为了便于测试，直接改为从形参接收参数
 */
void conversion( int i ,int base) {
        SElemType e;

        SqStack S;
        SqStack_InitStack( &S );

        // 八进制数前面加0
        printf( "十进制数 %d 转换为八进制数后为：0", i );

        while ( i != 0 ) {
                SqStack_Push( &S, i % base ); // 进栈时从低位到高位
                i = i / base;
        }

        while ( !SqStack_StackEmpty( S ) ) {
                SqStack_Pop( &S, &e ); // 出栈时从高位到低位
                printf( "%d", e );
        }

        printf( "\n" );

        SqStack_DestroyStack( &S );
}
