/*************************
 *                       *
 * 文件夹: ▲10 内部排序 *
 *                       *
 * 文件名: InsertSort.c  *
 *                       *
 * 算  法: 10.1          *
 *                       *
 *************************/

#include "InsertSort.h" //**▲10 内部排序**//

/*════╗
  ║算法10.1║
  ╚════*/
void InsertSort( SqList_sort *L ) {

        for ( int i = 1; i < L->length; ++i ) {

                // "<"，需将L.r[i]插入有序子表
                if ( LT( L->r[ i ].key, L->r[ i - 1 ].key ) ) {
                        RcdType temp = L->r[ i ]; // 复制为哨兵

                        int j = i - 1;
                        while ( LT( temp.key, L->r[ j ].key ) && j >= 0 ) {
                                L->r[ j + 1 ] = L->r[ j ]; // 记录后移 */
                                --j;
                        }
                        L->r[ ++j ] = temp; // 插入到正确位置
                }
        }
}
