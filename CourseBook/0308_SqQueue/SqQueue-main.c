#include "SqQueue.h" //**▲03 栈和队列**//
#include <stdio.h>

// 测试函数，打印整型
void PrintElem( QElemType e ) {
        printf( "%d ", e );
}

int main( int argc, char **argv ) {
        SqQueue Q;
        int i;
        QElemType e;

        printf( "████████ 函数 InitQueue \n" );
        {
                printf( "█ 初始化循环顺序队列 Q ...\n" );
                SqQueue_InitQueue( &Q );
        }
        PressEnterToContinue( debug );

        printf( "████████ 函数 QueueEmpty \n" );
        {
                SqQueue_QueueEmpty( Q ) ? printf( "█ Q 为空！！\n" )
                                : printf( "█ Q 不为空！\n" );
        }
        PressEnterToContinue( debug );

        printf( "████████ 函数 EnQueue \n" );
        {
                for ( i = 1; i <= 6; i++ ) {
                        SqQueue_EnQueue( &Q, 2 * i );
                        printf( "█ 元素 \"%2d\" 入队Q...\n", 2 * i );
                }
        }
        PressEnterToContinue( debug );

        printf( "████████ 函数 QueueTraverse \n" );
        {
                printf( "█ Q 中的元素为：Q = " );
                SqQueue_QueueTraverse( Q, PrintElem );
        }
        PressEnterToContinue( debug );

        printf( "████████ 函数 QueueLength \n" );
        {
                i = SqQueue_QueueLength( Q );
                printf( "█ Q 的长度为 %d \n", i );
        }
        PressEnterToContinue( debug );

        printf( "████████ 函数 DeQueue \n" );
        {
                SqQueue_DeQueue( &Q, &e );
                printf( "█ 队头元素 \"%d\" 出队...\n", e );
                printf( "█ Q 中的元素为：Q = " );
                SqQueue_QueueTraverse( Q, PrintElem );
        }
        PressEnterToContinue( debug );

        printf( "████████ 函数 GetHead \n" );
        {
                SqQueue_GetHead( Q, &e );
                printf( "█ 队头元素的值为 \"%d\" \n", e );
        }
        PressEnterToContinue( debug );

        printf( "████████ 函数 ClearQueue \n" );
        {
                printf( "█ 清空 Q 前：" );
                SqQueue_QueueEmpty( Q ) ? printf( " Q 为空！！\n" )
                                : printf( " Q 不为空！\n" );

                SqQueue_ClearQueue( &Q );

                printf( "█ 清空 Q 后：" );
                SqQueue_QueueEmpty( Q ) ? printf( " Q 为空！！\n" )
                                : printf( " Q 不为空！\n" );
        }
        PressEnterToContinue( debug );

        printf( "████████ 函数 DestroyQueue \n" );
        {
                printf( "█ 销毁 Q 前：" );
                Q.base != nullptr ? printf( " Q 存在！\n" )
                               : printf( " Q 不存在！！\n" );

                SqQueue_DestroyQueue( &Q );

                printf( "█ 销毁 Q 后：" );
                Q.base != nullptr ? printf( " Q 存在！\n" )
                               : printf( " Q 不存在！！\n" );
        }
        PressEnterToContinue( debug );

        return 0;
}
