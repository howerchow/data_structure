
add_executable(SqQueue SqQueue.h SqQueue.c SqQueue-main.c)
target_include_directories(SqQueue PUBLIC ${CMAKE_SOURCE_DIR}/Status)
# 链接公共库
target_link_libraries(SqQueue Scanf_lib)
