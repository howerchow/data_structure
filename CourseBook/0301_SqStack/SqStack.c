/*=========================
 * 栈的顺序存储结构（顺序栈）
 ==========================*/

#include "SqStack.h" //**▲03 栈和队列**//
#include <stdio.h>
#include <stdlib.h> // 提供malloc、realloc、free、exit原型
/*
 * 初始化
 *
 * 构造一个空栈。初始化成功则返回OK，否则返回ERROR。
 */
Status SqStack_InitStack( SqStack *S ) {
        if ( S == nullptr ) {
                return ERROR;
        }

        if ( STACK_INIT_SIZE > SIZE_MAX / sizeof( SElemType ) ) {
                return ERROR;
        }

        S->base = calloc( STACK_INIT_SIZE, sizeof( SElemType ) );
        if ( S->base == nullptr ) {
                exit( OVERFLOW );
        }

        S->top = S->base;
        S->stacksize = STACK_INIT_SIZE;

        return OK;
}

/* SqStack *CreateStack( ) { */
/*         SqStack *S = malloc( sizeof( SqStack ) ); */
/*         S->base = calloc( STACK_INIT_SIZE, sizeof( SElemType ) ); */
/*         if ( S->base == nullptr ) { */
/*                 exit( EXIT_FAILURE ); */
/*         } */

/*         S->top = S->base; */
/*         S->stacksize = STACK_INIT_SIZE; */

/*         return S; */
/* } */

/*
 * 销毁(结构)
 *
 * 释放顺序栈所占内存。
 */
Status SqStack_DestroyStack( SqStack *S ) {
        if ( S == nullptr ) {
                return ERROR;
        }

        free( S->base );

        S->base = nullptr;
        S->top = nullptr;
        S->stacksize = 0;

        return OK;
}

/*
 * 置空(内容)
 *
 * 只是清理顺序栈中存储的数据，不释放顺序栈所占内存。
 */
Status SqStack_ClearStack( SqStack *S ) {
        if ( S == nullptr || S->base == nullptr ) {
                return ERROR;
        }

        S->top = S->base;

        return OK;
}

/*
 * 判空
 *
 * 判断顺序栈中是否包含有效数据。
 *
 * 返回值：
 * true : 顺序栈为空
 * false: 顺序栈不为空
 */
bool SqStack_StackEmpty( SqStack S ) {
        if ( S.top == S.base ) {
                return true;
        }
        return false;
}

/*
 * 计数
 *
 * 返回顺序栈包含的有效元素的数量。
 */
int SqStack_StackLength( SqStack S ) {
        if ( S.base == nullptr ) {
                return 0;
        }

        return ( S.top - S.base );
}

/*
 * 取值
 *
 * 返回栈顶元素，并用e接收。
 */
Status SqStack_GetTop( SqStack S, SElemType *e ) {
        if ( S.base == nullptr || S.top == S.base ) {
                return 0;
        }

        // 不会改变栈中元素
        *e = *( S.top - 1 );

        return OK;
}

/*
 * 入栈
 *
 * 将元素e压入到栈顶。
 */
Status SqStack_Push( SqStack *S, SElemType e ) {
        if ( S == nullptr || S->base == nullptr ) {
                return ERROR;
        }

        // 栈满时，追加存储空间
        if ( S->top - S->base >= S->stacksize ) {
                S->base = realloc( S->base, ( S->stacksize + STACKINCREMENT ) *
                                                sizeof( SElemType ) );
                if ( S->base == nullptr ) {
                        exit( OVERFLOW ); // 存储分配失败
                }

                S->top = S->base + S->stacksize;
                S->stacksize += STACKINCREMENT;
        }

        // 进栈先赋值，栈顶指针再自增
        *( S->top++ ) = e;

        return OK;
}

/*
 * 出栈
 *
 * 将栈顶元素弹出，并用e接收。
 */
Status SqStack_Pop( SqStack *S, SElemType *e ) {
        if ( S == nullptr || S->base == nullptr ) {
                return ERROR;
        }

        if ( S->top == S->base ) {
                return ERROR;
        }

        // 出栈栈顶指针先递减，再赋值
        *e = *( --S->top );

        return OK;
}

/*
 * 遍历
 *
 * 用visit函数访问顺序栈S
 */
Status SqStack_StackTraverse( SqStack S, void ( *Visit )( SElemType ) ) {
        if ( S.base == nullptr ) {
                return ERROR;
        }

        SElemType *p = S.base;
        while ( p < S.top ) {
                Visit( *p++ );
        }

        printf( "\n" );

        return OK;
}
