#include "SqStack.h" //**▲03 栈和队列**//
#include <stdio.h>

// 测试函数，打印元素
void PrintElem( SElemType e ) {
        printf( "%d ", e );
}

int main( int argc, char **argv ) {
        SqStack S;
        int i;
        SElemType e;

        printf( "████████ 函数 SqStack_InitStack \n" );
        {
                printf( "█ 初始化顺序栈 S ...\n" );
                SqStack_InitStack( &S );
        }
        PressEnterToContinue( debug );

        printf( "████████ 函数 SqStack_StackEmpty \n" );
        {
                SqStack_StackEmpty( S ) ? printf( "█ S 为空！！\n" )
                                : printf( "█ S 不为空！\n" );
        }
        PressEnterToContinue( debug );

        printf( "████████ 函数 SqStack_Push \n" );
        {
                for ( i = 1; i <= 6; i++ ) {
                        SqStack_Push( &S, 2 * i );
                        printf( "█ 将 \"%2d\" 压入栈 S ...\n", 2 * i );
                }
        }
        PressEnterToContinue( debug );

        printf( "████████ 函数 SqStack_StackTraverse \n" );
        {
                printf( "█ S 中的元素为：S = " );
                SqStack_StackTraverse( S, PrintElem );
        }
        PressEnterToContinue( debug );

        printf( "████████ 函数 SqStack_StackLength \n" );
        {
                i = SqStack_StackLength( S );
                printf( "█ S 的长度为 %d \n", i );
        }
        PressEnterToContinue( debug );

        printf( "████████ 函数 SqStack_Pop \n" );
        {
                SqStack_Pop( &S, &e );
                printf( "█ 栈顶元素 \"%d\" 出栈...\n", e );
                printf( "█ S 中的元素为：S = " );
                SqStack_StackTraverse( S, PrintElem );
        }
        PressEnterToContinue( debug );

        printf( "████████ 函数 SqStack_GetTop \n" );
        {
                SqStack_GetTop( S, &e );
                printf( "█ 栈顶元素的值为 \"%d\" \n", e );
        }
        PressEnterToContinue( debug );

        printf( "████████ 函数 SqStack_ClearStack \n" );
        {
                printf( "█ 清空 S 前：" );
                SqStack_StackEmpty( S ) ? printf( " S 为空！！\n" )
                                : printf( " S 不为空！\n" );

                SqStack_ClearStack( &S );

                printf( "█ 清空 S 后：" );
                SqStack_StackEmpty( S ) ? printf( " S 为空！！\n" )
                                : printf( " S 不为空！\n" );
        }
        PressEnterToContinue( debug );

        printf( "████████ 函数 SqStack_DestroyStack \n" );
        {
                printf( "█ 销毁 S 前：" );
                S.base != nullptr &&S.top != nullptr ? printf( " S 存在！\n" )
                                               : printf( " S 不存在！！\n" );

                SqStack_DestroyStack( &S );

                printf( "█ 销毁 S 后：" );
                S.base != nullptr &&S.top != nullptr ? printf( " S 存在！\n" )
                                               : printf( " S 不存在！！\n" );
        }
        PressEnterToContinue( debug );

        return 0;
}
