/********************************
 *                              *
 * 文件夹: ▲10 内部排序        *
 *                              *
 * 文件名: QuickSort.h          *
 *                              *
 * 内  容: 快速排序相关操作列表 *
 *                              *
 ********************************/

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include "SequenceListType.h" //**▲10 内部排序**//

void QuickSort( SqList_sort *L );
void QuickSort_NonRecursive( SqList_sort *L );
/*━━━━━━━━━━━━━━━━━━┓
┃(04)算法10.7：对顺序表L作快速排序。 ┃
┗━━━━━━━━━━━━━━━━━━*/

#ifdef __cplusplus
}
#endif
