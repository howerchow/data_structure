/****************************
 *                          *
 * 文件夹: ▲10 内部排序    *
 *                          *
 * 文件名: QuickSort.c      *
 *                          *
 * 算  法: 10.6、10.7、10.8 *
 *                          *
 ****************************/

#include "QuickSort.h" //**▲10 内部排序**//
#include "SqStack.h"

static void Partition_Swap( SqList_sort *L, int i, int j ) {
        if ( L == nullptr || i < 0 || j < 0 || i >= L->length ||
             j >= L->length ) {
                return;
        }

        RcdType tmp = L->r[ i ];
        L->r[ i ] = L->r[ j ];
        L->r[ j ] = tmp;
}

/*══════╗
║ 算法10.6(a)║
╚══════*/
static int Partition_1( SqList_sort *L, int low, int high ) {

        KeyType pivotkey = L->r[ low ].key; // 用子表第一个记录作枢轴记录

        while ( low < high ) // 从表的两端交替地向中间扫描
        {
                while ( low < high && L->r[ high ].key >= pivotkey ) {
                        --high;
                }
                // 将比枢轴记录小的记录交换到低端
                Partition_Swap( L, low, high );

                while ( low < high && L->r[ low ].key <= pivotkey ) {
                        ++low;
                }
                // 将比枢轴记录大的记录交换到高端
                Partition_Swap( L, high, low );
        }

        return low; // 返回枢轴所在位置
}

/*══════╗
║ 算法10.6(b)║
╚══════*/
static int Partition_2( SqList_sort *L, int low, int high ) {

        RcdType tmp = L->r[ low ]; // 用子表第一个记录作枢轴记录

        int pivotkey = L->r[ low ].key; // 枢轴记录关键字

        while ( low < high ) // 从表的两端交替地向中间扫描
        {
                while ( low < high && L->r[ high ].key > pivotkey ) {
                        --high;
                }

                L->r[ low ] = L->r[ high ]; // 将比枢轴记录小的记录交换到低端

                while ( low < high && L->r[ low ].key <= pivotkey ) {
                        ++low;
                }

                L->r[ high ] = L->r[ low ]; // 将比枢轴记录大的记录交换到高端
        }

        L->r[ low ] = tmp; // 枢轴记录到位

        return low; // 返回枢轴所在位置
}

/*════╗
║算法10.7║
╚════*/
static void QSort( SqList_sort *L, int low, int high ) {

        if ( low < high ) // 长度大于1
        {
                //	pivotloc = Partition_1(L, low, high);
                ////将L->r[row..high]一分为二
                ///
                // 将L->r[row..high]一分为二
                int pivotloc = Partition_2( L, low, high );

                // 对低子表递归排序，pivotloc是枢轴位置
                QSort( L, low, pivotloc - 1 );

                // 对高子表递归排序
                QSort( L, pivotloc + 1, high );
        }
}

/*════╗
║算法10.8║
╚════*/
void QuickSort( SqList_sort *L ) {
        if ( L == nullptr ) {
                return;
        }

        QSort( L, 0, L->length - 1 );
}

void QuickSort_NonRecursive( SqList_sort *L ) {
        if ( L == nullptr ) {
                return;
        }

        SqStack S;
        SqStack_InitStack( &S );

        SqStack_Push( &S, 0 );
        SqStack_Push( &S, L->length - 1 );

        int low, high;

        while ( !SqStack_StackEmpty( S ) ) {
                SqStack_Pop( &S, &high );
                SqStack_Pop( &S, &low );
                int pivotloc = Partition_2( L, low, high );

                if ( low < pivotloc - 1 ) {
                        SqStack_Push( &S, low );
                        SqStack_Push( &S, pivotloc - 1 );
                }

                if ( pivotloc + 1 < high ) {
                        SqStack_Push( &S, pivotloc + 1 );
                        SqStack_Push( &S, high );
                }
        }

        SqStack_DestroyStack( &S );
}
