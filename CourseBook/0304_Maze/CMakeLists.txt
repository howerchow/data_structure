
# 生成可执行文件
add_executable(Maze SqStack.h SqStack.c Maze.h Maze.c Maze-main.c)
target_include_directories(Maze PUBLIC ${CMAKE_SOURCE_DIR}/Status)
# 链接公共库
target_link_libraries(Maze Scanf_lib)
