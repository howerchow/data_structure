/*=============================
 * 线性表的顺序存储结构（顺序表）
 *
 * 包含算法: 2.3、2.4、2.5、2.6
 =============================*/

#include "SqList.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h> // malloc、realloc、free、exit
#include <string.h>

/*
 * ████████ 算法2.3 ████████
 *
 * 初始化
 *
 * 初始化成功则返回OK，否则返回ERROR。
 */
Status SqList_InitList( SqList *L ) {
        // 分配指定容量的内存，如果分配失败，则返回nullptr
        L->elem = malloc( LIST_INIT_SIZE * sizeof( ElemType ) );
        if ( L->elem == nullptr ) {
                // 存储内存失败
                exit( EXIT_FAILURE );
        }

        L->length = 1;                // 初始化顺序表长度为0
        L->capacity = LIST_INIT_SIZE; // 顺序表初始内存分配量

        return OK; // 初始化成功
}

/*
 * 销毁(结构)
 *
 * 释放顺序表所占内存。
 */
Status SqList_DestroyList( SqList *L ) {
        // 确保顺序表结构存在
        if ( L == nullptr || L->elem == nullptr ) {
                return ERROR;
        }

        // 释放顺序表内存
        free( L->elem );

        // 释放内存后置空指针
        L->elem = nullptr;

        // 顺序表长度跟容量都归零
        L->length = 0;
        L->capacity = 0;

        return OK;
}

/*
 * 置空(内容)
 *
 * 只是清理顺序表中存储的数据，不释放顺序表所占内存。
 */
Status SqList_ClearList( SqList *L ) {
        if ( L == nullptr || L->elem == nullptr ) {
                return ERROR;
        }

        L->length = 0;

        return OK;
}

/*
 * 判空
 *
 * 判断顺序表中是否包含有效数据。
 *
 * 返回值：
 * true : 顺序表为空
 * false: 顺序表不为空
 */
bool SqList_ListEmpty( SqList L ) {
        return L.length == 0 ? true : false;
}

/*
 * 计数
 *
 * 返回顺序表包含的有效元素的数量。
 */
size_t SqList_ListLength( SqList L ) {
        return L.length;
}

/*
 * 取值
 *
 * 获取顺序表中第i个元素，将其存储到e中。
 * 如果可以找到，返回OK，否则，返回ERROR。
 *
 *【备注】
 * 通常，i的含义应该指索引，即从0开始计数。
 */
ElemType SqList_GetElem( SqList L, size_t i ) {
        // 因为i的含义是位置，所以其合法范围是：[0, length-1]
        if ( i > L.length - 1 ) {
                return ERROR; // i值不合法
        }

        return L.elem[ i ];
}

/*
 * ████████ 算法2.6 ████████
 *
 * 查找
 *
 * 返回顺序表中首个与e满足Compare关系的元素位序。
 * 如果不存在这样的元素，则返回0。
 *
 *【备注】
 * 元素e是Compare函数第二个形参
 */
size_t SqList_LocateElem( SqList L, ElemType e,
                          bool ( *Compare )( ElemType, ElemType ) ) {

        if ( L.elem == nullptr ) {
                return ERROR;
        }

        size_t i = 0;

        // 遍历顺序表
        while ( i < L.length && !Compare( L.elem[ i ], e ) ) {
                ++i;
        }

        if ( i < L.length ) {
                return i;
        }

        return 0;
}

/*
 * 前驱
 *
 * 获取元素cur_e的前驱，
 * 如果存在，将其存储到pre_e中，返回OK，
 * 如果不存在，则返回ERROR。
 */
Status SqList_PriorElem( SqList L, ElemType cur_e, ElemType *pre_e ) {

        // 确保顺序表结构存在，且最少包含两个元素
        if ( L.elem == nullptr || L.length < 2 ) {
                return ERROR;
        }

        // 这里的i初始化为第0个元素的【索引】
        size_t i = 0;

        // 从第0个元素开始，查找cur_e的位置
        while ( i < L.length && L.elem[ i ] != cur_e ) {
                ++i;
        }

        // 如果cur_e是首个元素(没有前驱)，或者没找到元素cur_e，返回ERROR
        if ( i == 0 || i > L.length ) {
                return ERROR;
        }

        // 存储cur_e的前驱
        *pre_e = L.elem[ i - 1 ];

        return OK;
}

/*
 * 后继
 *
 * 获取元素cur_e的后继，
 * 如果存在，将其存储到next_e中，返回OK，
 * 如果不存在，则返回ERROR。
 */
Status SqList_NextElem( SqList L, ElemType cur_e, ElemType *next_e ) {
        // 确保顺序表结构存在，且最少包含两个元素
        if ( L.elem == nullptr || L.length < 2 ) {
                return ERROR;
        }

        // 这里的i初始化为第0个元素的【索引】
        size_t i = 0;

        // 从第0个元素开始，查找cur_e的位置
        while ( i < L.length - 1 && L.elem[ i ] != cur_e ) {
                ++i;
        }

        // 如果cur_e是最后1个元素(没有前驱)，或者没找到元素cur_e，返回ERROR
        if ( i >= L.length - 1 ) {
                return ERROR;
        }

        // 存储cur_e的后继
        *next_e = L.elem[ i + 1 ];

        return OK;
}

/*
 * ████████ 算法2.4 ████████
 *
 * 插入
 *
 * 向顺序表第i个位置上插入e，插入成功则返回OK，否则返回ERROR。
 *
 *【备注】
 * i从0开始计数
 */
Status SqList_ListInsert( SqList *L, size_t i, ElemType e ) {
        // 确保顺序表结构存在
        if ( L == nullptr || L->elem == nullptr ) {
                return ERROR;
        }

        // i值越界
        if ( i > L->length ) {
                return ERROR;
        }

        // 若存储空间已满，则增加新空间
        if ( L->length >= L->capacity ) {
                // 基于现有空间扩容
                ElemType *newbase = ( ElemType * )realloc(
                    L->elem,
                    ( L->capacity + LISTINCREMENT ) * sizeof( ElemType ) );
                if ( newbase == nullptr ) {
                        // 存储内存失败
                        exit( EXIT_FAILURE );
                }

                // 新基址
                L->elem = newbase;
                // 存的存储空间
                L->capacity += LISTINCREMENT;
        }

        // q为插入位置
        ElemType *q = &L->elem[ i ];

        /* // 1.右移元素，腾出位置 */
        for ( ElemType *p = &L->elem[ L->length - 1 ]; p >= q; --p ) {
                //*( p + 1 ) = *p;
                memmove( p + 1, p, sizeof( *p ) );
        }

        // 2.插入e
        *q = e;

        // 3.表长增1
        ++L->length;

        return OK;
}

/*
 * ████████ 算法2.5 ████████
 *
 * 删除
 *
 * 删除顺序表第i个位置上的元素，并将被删除元素存储到e中。
 * 删除成功则返回OK，否则返回ERROR。
 *
 *【备注】
 * 教材中i的含义是元素位置，从1开始计数
 */
Status SqList_ListDelete( SqList *L, size_t i, ElemType *e ) {
        // 确保顺序表结构存在
        if ( L == nullptr || L->elem == nullptr || e == nullptr ) {
                return ERROR;
        }

        // i值越界
        if ( i > L->length ) {
                return ERROR;
        }

        // p为被删除元素的位置
        ElemType *p = &L->elem[ i ];

        // 1.获取被删除元素
        *e = *p;

        // 表尾元素位置
        ElemType *q = L->elem + ( L->length - 1 );

        // 2.左移元素，被删除元素的位置上会有新元素进来
        for ( ++p; p <= q; ++p ) {
                *( p - 1 ) = *p;
        }

        // 3.表长减1
        --L->length;

        return OK;
}

Status SqList_ListDeleteElement( SqList *L, ElemType e ) {
        // 确保顺序表结构存在
        if ( L == nullptr || L->elem == nullptr ) {
                return ERROR;
        }

        int j = 0;
        for ( size_t i = 0; i < L->length; ++i ) {
                if ( L->elem[ i ] != e ) {
                        L->elem[ j++ ] = L->elem[ i ];
                }
        }
        L->length = j; // 更新长度(j在末尾元素的下一个位置)

        return OK;
}

Status SqList_DeleteDuplicates( SqList *L ) {
        // 确保顺序表结构存在
        if ( L == nullptr || L->elem == nullptr ) {
                return ERROR;
        }

        ElemType e;
        // 遍历数组，找到重复的元素并移除
        for ( size_t i = 0; i < L->length; ++i ) {
                for ( size_t j = i + 1; j < L->length; ) {
                        if ( L->elem[ i ] == L->elem[ j ] ) {
                                // 如果找到重复元素，移除它
                                SqList_ListDelete( L, j, &e );
                        } else {
                                ++j; // 只有不重复时才移动指针
                        }
                }
        }

        return OK;
}

Status SqList_DeleteAdjacentDuplicates( SqList *L ) {
        // 确保顺序表结构存在
        if ( L == nullptr || L->elem == nullptr ) {
                return ERROR;
        }

        size_t i = 0;

        // 遍历数组，相邻重复元素，去重
        for ( size_t j = 1; j < L->length; ++j ) {
                if ( L->elem[ i ] != L->elem[ j ] &&
                     i != j - 1 ) { // 不相邻不重复 更新数组内容
                        L->elem[ ++i ] = L->elem[ j ];
                } else if ( L->elem[ i ] != L->elem[ j ] &&
                            i == j - 1 ) { // 相邻不重复 更新i, j
                        i = j;
                }
        }

        // 更新数组大小
        L->length = i + 1;

        return OK;
}

/*
 * 遍历
 *
 * 用visit函数访问顺序表L
 */
void SqList_ListTraverse( SqList L, void ( *Visit )( ElemType ) ) {
        for ( size_t i = 1; i < L.length; ++i ) {
                Visit( L.elem[ i ] );
        }
        printf( "\n" );
}
