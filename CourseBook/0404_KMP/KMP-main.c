#include "KMP.h" //**▲04 串**//
#include <stdlib.h>

// 测试函数，打印字符串
static void PrintElem( SString S ) {

        for ( size_t i = 1; i <= S[ 0 ]; ++i ) {
                printf( "%c", S[ i ] );
        }

        printf( "\n" );
}

static void PrintSqListElem( ElemType e ) {
        printf( "%d ", e );
}

int main( int argc, char **argv ) {
        char *s = "abaaabcaabaabcacabaabcaabaabcac";
        char *t = "abaabcac";

        SString S, T;
        StrAssign( S, s ); // 初始化主串 "abaaabcaabaabcacabaabcaabaabcac"
        printf( "\n" );
        printf( "S       = " );
        PrintElem( S );

        StrAssign( T, t ); // 初始化模式串 "abaabcac";
        printf( "T       = " );
        PrintElem( T );

        // 注：next数组和nextval数组的0号单元是弃用的，从1号单元开始存储有效数据
        SqList next;
        SqList nextval;
        SqList_InitList( &next );
        SqList_InitList( &nextval );

        /* int *next = */
        /*     malloc( ( T[ 0 ] + 1 ) * sizeof( int ) ); // 模式串的next函数值
         */
        /* int *nextval = */
        /*     malloc( ( T[ 0 ] + 1 ) * sizeof( int ) ); //
         * 模式串的nextval函数值 */

        KMP_GetNext( T, &next );       // 算法4.7
        KMP_GetNextVal( T, &nextval ); // 算法4.8，即算法4.7的改进版

        printf( "\n" );

        printf( "next    : " );
        KMP_TraverseNext( next, PrintSqListElem );

        printf( "nextval : " );
        KMP_TraverseNext( nextval, PrintSqListElem );

        printf( "\n" );

        int pos = 1; // 匹配起点

        int i = KMP_Index( S, T, pos, &next );
        int j = KMP_Index( S, T, pos, &nextval );

        printf( "从%d个字符起，T 在 S 中第一次匹配成功的位置为 %d\n", pos, i );
        printf( "从%d个字符起，T 在 S 中第一次匹配成功的位置为 %d\n", pos, j );

        SqList_DestroyList( &next );
        SqList_DestroyList( &nextval );

        return 0;
}
