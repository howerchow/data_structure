/*=======================
 * KMP算法 O(n + m)
 *
 * 包含算法: 4.6、4.7、4.8
 ========================*/

#pragma once

#include "SString.h" //**▲04 串**//
#include "SqList.h"

/*
 * ████████ 算法4.6 ████████
 *
 * 查找
 *
 * 从pos处开始搜索模式串T在主串S中首次出现的位置，如果不存在，则返回0。
 * 如果查找成功，返回匹配的位置。
 *
 *【注】
 * 1.该实现用到了KMP算法，是一种比较高效的字符串匹配方式
 * 2.教材中没有next参数
 */
int KMP_Index( const SString S, const SString T, int pos, SqList *next );

/*
 * ████████ 算法4.7 ████████
 *
 * 计算模式串的“失配数组”，用于KMP算法。
 */
void KMP_GetNext( const SString T, SqList *next );

/*
 * ████████ 算法4.8 ████████
 *
 * 计算模式串的“失配数组”，用于KMP算法。
 * 这是一个优化后的版本，效率较算法4.7有所提高。
 */
void KMP_GetNextVal( const SString T, SqList *nextval );

/*
 * "失配数组"遍历
 */
void KMP_TraverseNext( const SqList next, void ( *Visit )( ElemType ) );
