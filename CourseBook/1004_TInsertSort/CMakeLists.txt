add_executable(TInsertSort TInsertSort.h TInsertSort.c TInsertSort-main.c StaticLinkedListType.h StaticLinkedListType.c)
target_include_directories(TInsertSort PUBLIC ${CMAKE_SOURCE_DIR}/Status)
target_link_libraries(TInsertSort PUBLIC Scanf_lib)


file(GLOB TestData TestData*.txt)
file(COPY ${TestData} DESTINATION ${CMAKE_CURRENT_BINARY_DIR})
