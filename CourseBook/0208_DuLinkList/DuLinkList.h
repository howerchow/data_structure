/*=====================
 * 双向循环链表
 *
 * 包含算法: 2.18、2.19
 ======================*/

#pragma once

#include "Status.h" //**▲01 绪论**//


/* 双向循环链表元素类型定义 */
typedef int ElemType;

/*
 * 双向循环链表结构
 *
 * 注：这里的双向循环链表存在头结点
 */
typedef struct DuLNode {
        ElemType data;
        struct DuLNode *prior; // 前驱
        struct DuLNode *next;  // 后继
} DuLNode;

// 指向双向循环链表结点的指针
typedef DuLNode *DuLinkList;

/*
 * 初始化
 *
 * 初始化成功则返回OK，否则返回ERROR。
 */
Status DuLink_InitList( DuLinkList *L );

/*
 * 销毁(结构)
 *
 * 释放双向循环链表所占内存。
 */
Status DuLink_DestroyList( DuLinkList *L );

/*
 * 置空(内容)
 *
 * 这里需要释放双向循环链表中非头结点处的空间。
 */
Status DuLink_ClearList( DuLinkList L );

/*
 * 判空
 *
 * 判断双向循环链表中是否包含有效数据。
 *
 * 返回值：
 * true : 双向循环链表为空
 * false: 双向循环链表不为空
 */
bool DuLink_ListEmpty( const DuLinkList L );

/*
 * 计数
 *
 * 返回双向循环链表包含的有效元素的数量。
 */
int DuLink_ListLength( DuLinkList L );

/*
 * 取值(按位)
 *
 * 获取双向循环链表中第i个元素，将其存储到e中。
 * 如果可以找到，返回OK，否则，返回ERROR。
 *
 *【备注】
 * 教材中i的含义是元素位置，从1开始计数，但这不符合编码的通用约定。
 * 通常，i的含义应该指索引，即从0开始计数。
 */
Status DuLink_GetElem( DuLinkList L, int i, ElemType *e );

/*
 * 查找(按值)
 *
 * 返回双向循环链表中首个与e满足Compare关系的元素位序。
 * 如果不存在这样的元素，则返回0。
 *
 *【备注】
 * 元素e是Compare函数第二个形参
 */
int DuLink_LocateElem( const DuLinkList L, ElemType e,
                bool ( *Compare )( ElemType, ElemType ) );

/*
 * 前驱
 *
 * 获取元素cur_e的前驱，
 * 如果存在，将其存储到pre_e中，返回OK，
 * 如果不存在，则返回ERROR。
 */
Status DuLink_PriorElem( const DuLinkList L, ElemType cur_e, ElemType *pre_e );

/*
 * 后继
 *
 * 获取元素cur_e的后继，
 * 如果存在，将其存储到next_e中，返回OK，
 * 如果不存在，则返回ERROR。
 */
Status DuLink_NextElem( const DuLinkList L, ElemType cur_e, ElemType *next_e );

/*
 * ████████ 算法2.18 ████████
 *
 * 插入
 *
 * 向双向循环链表第i个位置上插入e，插入成功则返回OK，否则返回ERROR。
 *
 *【备注】
 * 教材中i的含义是元素位置，从1开始计数
 */
Status DuLink_ListInsert( DuLinkList L, int i, ElemType e );

/*
 * ████████ 算法2.19 ████████
 *
 * 删除
 *
 * 删除双向循环链表第i个位置上的元素，并将被删除元素存储到e中。
 * 删除成功则返回OK，否则返回ERROR。
 *
 *【备注】
 * 教材中i的含义是元素位置，从1开始计数
 */
Status DuLink_ListDelete( DuLinkList L, int i, ElemType *e );

/*
 * 遍历
 *
 * 用visit函数访问双向循环链表L
 */
void DuLink_ListTraverse( const DuLinkList L, void ( *Visit )( ElemType ) );
