# 生成可执行文件
add_executable(OLGraph LinkQueue.h LinkQueue.c OLGraph.h OLGraph.c OLGraph-main.c)
target_include_directories(OLGraph PUBLIC ${CMAKE_SOURCE_DIR}/Status)
# 链接公共库
target_link_libraries(OLGraph Scanf_lib)

# 记录要拷贝到*.exe目录下的资源文件
file(GLOB TestData TestData*.txt)
# 将资源文件拷贝到*.exe目录下，不然无法加载
file(COPY ${TestData} DESTINATION ${CMAKE_CURRENT_BINARY_DIR})
