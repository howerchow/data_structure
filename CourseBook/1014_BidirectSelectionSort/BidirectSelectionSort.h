/********************************
 *                              *
 * 文件夹: ▲10 鸡尾酒排序        *
 *                              *
 * 文件名: CocktailShakerSort.h         *
 *                              *
 * 内  容: 鸡尾酒排序相关操作列表 *
 *                              *
 ********************************/

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include "SequenceListType.h" //**▲10 内部排序**//

/* 双向选择排序函数列表 */
void BidirectSelectionSort( SqList_sort *L );
/*━━━━━━━━━━━━━┓
┃(01)对顺序表L作双向选择排序。 ┃
┗━━━━━━━━━━━━━*/

#ifdef __cplusplus
}
#endif
