/********************************
 *                               *
 * 文件夹: ▲10 鸡尾酒排序        *
 *                               *
 * 文件名: CocktailShakerSort.c  *
 *                               *
 ********************************/

#include "BidirectSelectionSort.h" //**▲10 内部排序**//
#include <stddef.h>

static void BidirectSelectionSwap( SqList_sort *L, int i, int j ) {
        if ( L == nullptr || i == j || i < 0 || j < 0 ) {
                return;
        }

        RcdType temp = L->r[ i ];
        L->r[ i ] = L->r[ j ];
        L->r[ j ] = temp;
}

void BidirectSelectionSort( SqList_sort *L ) {
        if ( L == nullptr ) {
                return;
        }

        int left = 0;
        int right = L->length - 1;

        while ( left < right ) {
                // 每一轮找出最小值和最大值
                int minIndex = left;
                int maxIndex = left;

                for ( int i = left; i <= right; ++i ) {
                        if ( L->r[ i ].key < L->r[ minIndex ].key ) {
                                minIndex = i;
                        }
                        if ( L->r[ i ].key > L->r[ maxIndex ].key ) {
                                maxIndex = i;
                        }
                }

                // 把最小值放到左边
                if ( minIndex != left ) {
                        BidirectSelectionSwap( L, left, minIndex );
                }

                // 更新最大值的位置（如果最大值原本在左边，被交换后就指向
                // minIndex）
                if ( maxIndex == left ) {
                        maxIndex = minIndex;
                }

                // 把最大值放到右边
                if ( maxIndex != right ) {
                        BidirectSelectionSwap( L, right, maxIndex );
                }

                // 缩小未排序部分
                ++left;
                --right;
        }
}
