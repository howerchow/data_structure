/*************************
 *                       *
 * 文件夹: ▲10 内部排序 *
 *                       *
 * 文件名: BubbleSort.c  *
 *                       *
 *************************/

#include "BubbleSort.h" //**▲10 内部排序**//
#include <stddef.h>

static void BubbleSortSwap( SqList_sort *L, int i, int j ) {
        if ( L == nullptr || i == j || i < 0 || j < 0 ) {
                return;
        }

        RcdType tmp = L->r[ i ];
        L->r[ i ] = L->r[ j ];
        L->r[ j ] = tmp;
}

void BubbleSort( SqList_sort *L ) {

        for ( int i = L->length - 1; i > 0; --i ) {
                bool swapped = false; // swapped标记遍历过程是否发生的交换

                for ( int j = 0; j < i; ++j ) {
                        if ( GT( L->r[ j ].key, L->r[ j + 1 ].key ) ) {
                                BubbleSortSwap( L, j, j + 1 );
                                // 若遍历不发生交换，说明序列已经有序
                                swapped = true;
                        }
                }

                if ( !swapped ) {
                        break;
                }
        }
}

void BubbleSort_1( SqList_sort *L ) {

        int i = L->length - 1;
        int k = 0;

        while ( i > 0 ) {
                bool swapped = false; // swapped标记遍历过程是否发生的交换
                for ( int j = 0; j < i; ++j ) {
                        if ( GT( L->r[ j ].key, L->r[ j + 1 ].key ) ) {
                                BubbleSortSwap( L, j, j + 1 );
                                // 若遍历不发生交换，说明序列已经有序
                                swapped = true;
                                k = j; // 记录最后一次交换位置
                        }
                }
                if ( !swapped ) {
                        break;
                }
                i = k;
        }
}
