/******************************
 *                            *
 * 文件夹: ▲10 内部排序      *
 *                            *
 * 文件名: SequenceListType.c *
 *                            *
 ******************************/

#include "SequenceListType.h" //**▲10 内部排序**//
#include <string.h>

Status CreateSortList( FILE *fp, SqList_sort *L ) {

        ReadData( fp, "%d", &( L->length ) );


        if ( L->length > MAXSIZE ) {
                return ERROR;
        }
	memset(L->r,'\0',MAXSIZE);

        for ( int i = 0; i < L->length; ++i ) {
                ReadData( fp, "%d", &( L->r[ i ].key ) );
        }

        return OK;
}

void Traverse( SqList_sort L, void ( *Visit )( KeyType ) ) {
        for ( int i = 0; i < L.length; ++i ) {
                Visit( L.r[ i ].key );
        }

        printf( "\n" );
}
