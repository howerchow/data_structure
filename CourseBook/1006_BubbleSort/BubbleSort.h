/********************************
 *                              *
 * 文件夹: ▲10 内部排序        *
 *                              *
 * 文件名: BubbleSort.h         *
 *                              *
 * 内  容: 起泡排序相关操作列表 *
 *                              *
 ********************************/

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include "SequenceListType.h" //**▲10 内部排序**//

/* 起泡排序函数列表 */
void BubbleSort( SqList_sort *L );
void BubbleSort_1( SqList_sort *L );
/*━━━━━━━━━━━━━┓
┃(01)对顺序表L作起泡排序。 ┃
┗━━━━━━━━━━━━━*/

#ifdef __cplusplus
}
#endif
