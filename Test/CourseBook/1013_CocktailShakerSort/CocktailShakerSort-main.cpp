#include "CocktailShakerSort.h" //**02 线性表**//
#include <catch2/catch_all.hpp>
#include <chrono>
#include <iostream>
#include <stdio.h>

TEST_CASE( "CocktailShakerSort", "[CocktailShakerSort]" ) {
        SqList_sort L;
        FILE *fp = fopen( "TestData.txt", "r" );
        CreateSortList( fp, &L );

        SqList_sort L1;
        CreateSortList( fp, &L1 );
        fclose( fp );

        SECTION( "CocktailShakerSort: " ) {
                auto start = std::chrono::high_resolution_clock::now( );
                CocktailShakerSort( &L );
                auto end = std::chrono::high_resolution_clock::now( );
                auto duration =
                    std::chrono::duration_cast<std::chrono::nanoseconds>(
                        end - start );
                std::cout << "CocktailShakerSort Execution time: "
                          << duration.count( ) << " nanoseconds" << std::endl;

                // KeyType expec2[] = { 13, 27, 38, 49, 49, 65, 76, 97 };
                KeyType expec2[] = { 6,  7,  8,  11, 12, 13, 14, 16, 19, 20,
                                     22, 23, 24, 26, 29, 30, 31, 33, 34, 35,
                                     36, 37, 38, 42, 44, 45, 48, 50, 53, 54,
                                     60, 62, 64, 66, 72, 73, 74, 75, 80, 81,
                                     84, 85, 87, 88, 90, 91, 92, 93, 94, 97 };

                for ( int i = 0; i < L.length; ++i ) {
                        REQUIRE( L.r[ i ].key == expec2[ i ] );
                }
        }

        SECTION( "CocktailShakerSort_1: " ) {
                auto start = std::chrono::high_resolution_clock::now( );
                CocktailShakerSort_1( &L1 );
                auto end = std::chrono::high_resolution_clock::now( );
                auto duration =
                    std::chrono::duration_cast<std::chrono::nanoseconds>(
                        end - start );
                std::cout << "CocktailShakerSort_1 Execution time: "
                          << duration.count( ) << " nanoseconds" << std::endl;

                // KeyType expec2[] = { 13, 27, 38, 49, 49, 65, 76, 97 };
                KeyType expec2[] = { 6,  7,  8,  11, 12, 13, 14, 16, 19, 20,
                                     22, 23, 24, 26, 29, 30, 31, 33, 34, 35,
                                     36, 37, 38, 42, 44, 45, 48, 50, 53, 54,
                                     60, 62, 64, 66, 72, 73, 74, 75, 80, 81,
                                     84, 85, 87, 88, 90, 91, 92, 93, 94, 97 };

                for ( int i = 0; i < L1.length; ++i ) {
                        REQUIRE( L1.r[ i ].key == expec2[ i ] );
                }
        }
}
