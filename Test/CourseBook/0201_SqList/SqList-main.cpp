#include "SqList.h" //**02 线性表**//
#include <catch2/catch_all.hpp>
#include <stdio.h>

// 判断data>e是否成立
bool CmpGreater( ElemType data, ElemType e ) {
        return data > e ? true : false;
}

// 测试函数，打印元素
void PrintElem( ElemType e ) {
        printf( "%d ", e );
}

TEST_CASE( "SqList", "[SqList]" ) {
        SqList L;
        SqList_InitList( &L );

        ElemType e;

        ElemType expected[] = { 0, 2, 4, 6, 8, 10, 12, 14, 2, 2, 3, 3, 3 };

        for ( int i = 0; i < sizeof( expected ) / sizeof( *expected ); ++i ) {
                SqList_ListInsert( &L, i, expected[ i ] );
                REQUIRE( SqList_GetElem( L, i ) == expected[ i ] );
        }

        SECTION( "SqList_ListLength:" ) {
                REQUIRE( SqList_ListLength( L ) == 13 );
        }

        SECTION( "SqList_ListDelete:" ) {
                SqList_ListDelete( &L, 6, &e );
                REQUIRE( e == 12 );
                REQUIRE( SqList_ListLength( L ) == 12 );
        }

        SECTION( "SqList_GetElem:" ) {
                REQUIRE( SqList_GetElem( L, 4 ) == 8 );
        }

        SECTION( "SqList_LocateElem:" ) {
                REQUIRE( SqList_LocateElem( L, 7, CmpGreater ) == 4 );
        }

        SECTION( "SqList_PriorElem:" ) {
                SqList_PriorElem( L, 6, &e );
                REQUIRE( e == 4 );
        }

        SECTION( "SqList_PriorElem:" ) {
                SqList_NextElem( L, 6, &e );
                REQUIRE( e == 8 );
        }

        SECTION( "SqList_ListDeleteElement:" ) {
                ElemType expecDelet[] = { 0,  2, 6, 8, 10, 12,
                                          14, 2, 2, 3, 3,  3 };
                SqList_ListDeleteElement( &L, 4 );
                for ( int i = 0; i < SqList_ListLength( L ); ++i ) {
                        REQUIRE( SqList_GetElem( L, i ) == expecDelet[ i ] );
                }
        }

        SECTION( "SqList_DestroyList:" ) {
                SqList_DestroyList( &L );
                REQUIRE( SqList_ListEmpty( L ) == true );
        }

        SECTION( "SqList_DeleteAdjacentDuplicates:" ) {
                SqList_DeleteAdjacentDuplicates( &L );

                ElemType expec2[] = { 0, 2, 4, 6, 8, 10, 12, 14, 2, 3 };
                for ( int i = 0; i < SqList_ListLength( L ); ++i ) {
                        REQUIRE( SqList_GetElem( L, i ) == expec2[ i ] );
                }
        }

        SqList_DestroyList( &L );
}
