
set(test_name BubbleSort)

add_executable(${test_name}_Tests BubbleSort-main.cpp
  ${CMAKE_SOURCE_DIR}/CourseBook/1006_BubbleSort/BubbleSort.c
  ${CMAKE_SOURCE_DIR}/CourseBook/1006_BubbleSort/BubbleSort.h
  ${CMAKE_SOURCE_DIR}/CourseBook/1006_BubbleSort/SequenceListType.h
  ${CMAKE_SOURCE_DIR}/CourseBook/1006_BubbleSort/SequenceListType.c
)

target_include_directories(${test_name}_Tests
  PUBLIC ${CMAKE_SOURCE_DIR}/Status ${CMAKE_SOURCE_DIR}/CourseBook/1006_BubbleSort
)

file(GLOB TestData   ${CMAKE_SOURCE_DIR}/CourseBook/1006_BubbleSort/TestData*.txt)
file(COPY ${TestData} DESTINATION ${CMAKE_CURRENT_BINARY_DIR})

find_package(Catch2 3 REQUIRED)

target_link_libraries(${test_name}_Tests
  PUBLIC Scanf_lib
  PRIVATE Catch2::Catch2WithMain
)

include(CTest)
include(Catch)
catch_discover_tests(${test_name}_Tests)
